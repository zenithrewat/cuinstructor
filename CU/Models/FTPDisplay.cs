﻿using System;
using System.ComponentModel.DataAnnotations;
using CU.Constants;

namespace CU.Models
{
    public class FTPDisplay
    {
        public int id { get; set; }
        public string serverip { get; set; }
        public int port { get; set; }
        public DateTime deleted { get; set; }
        public bool isdeleted { get; set; }
        public bool isenabled { get; set; }
        public string security_protocol { get; set; }
        public bool isdefault { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        [Compare("password", ErrorMessage = AppConstant.PassNotMacth)]
        public string confirmpassword { get; set; }
        public int creator { get; set; }
        public string creatorname { get; set; }
        public int editor { get; set; }
        public string editorname { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public int type { get; set; }

        public string DrivePath { get; set; }
        public bool isauthenticated { get; set; }
        public string PathUpload { get; set; }
    
        public FTPDisplay(int type)
        {
            this.type = type;
        }

        public FTPDisplay()
        {

        }
    }
}
