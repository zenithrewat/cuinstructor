﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using CU.Models;
using CU.Services;
using NLog;
using Renci.SshNet;

namespace CU.Job
{
    public class SyncDataRegister
    {
        protected ILogger log = NLog.LogManager.GetCurrentClassLogger();

        public void Execute()
        {
            log.Info("Sync CU Data Job is executing.");
            log.Info(SyncMasterSubject());
            log.Info(SyncMasterDG207());
            log.Info("Sync CU Data Job is finishing.");
        }

        public string SyncMasterSubject()
        {
            log.Debug("Start Sync Master User from {0}", Environment.GetEnvironmentVariable("ftpMasterUserFileName"));
            DataTable dt = new DataTable();
            DataTable dtForSubject = new DataTable();            
            string fullText;

            var startJob = new Stopwatch();
            startJob.Start();

            try
            {
                SftpClient sFtpclient = new FTPService().ftpConnect((int)Constants.AppConstant.FtpServerType.Gateway);

                if (sFtpclient.IsConnected)
                {
                    //DG0207
                    //MemoryStream ms = new MemoryStream();
                    //sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterCourseTermFileName"), ms);
                    //using (ms)
                    //{
                    //    fullText = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                    //    string[] rows = fullText.Split('\n');
                    //    for (int i = 0; i < rows.Count() - 1; i++)
                    //    {
                    //        string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    //        //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                    //        if (i == 0)
                    //        {
                    //            for (int j = 0; j < rowValues.Count(); j++)
                    //            {
                    //                dt.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                    //            }
                    //        }
                    //        else
                    //        {
                    //            DataRow dr = dt.NewRow();
                    //            for (int k = 0; k < rowValues.Count(); k++)
                    //            {
                    //                dr[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                    //            }
                    //            dt.Rows.Add(dr); //add other rows  
                    //        }

                    //    }
                    //}
                    //ms.Close();

                    //DG0206
                    MemoryStream msSubject = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterSubjectTermFileName"), msSubject);
                    using (msSubject)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(msSubject.ToArray());
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtForSubject.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow drSubject = dtForSubject.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    drSubject[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dtForSubject.Rows.Add(drSubject); //add other rows  
                            }

                        }
                    }
                    msSubject.Close();

                    sFtpclient.Disconnect();
                    log.Debug("Successfully Disconnected to SFTP :", sFtpclient.ConnectionInfo.Host);
                }
                else
                {
                    return ("Ftp Connection Failed");
                }

                //copy datatable
                //filter Current Year      
                //DataTable resultCourseName = dt.AsEnumerable()
                //.Where(row => Int32.Parse(row.Field<string>("YEAR")) >= DateTime.Now.Year)
                //.CopyToDataTable();
                //dt = resultCourseName;

                //DataTable resultSubjectName = dtForSubject.AsEnumerable()
                //.Where(row => Int32.Parse(row.Field<string>("YEAR")) >= DateTime.Now.Year)
                //.CopyToDataTable();
                //dtForSubject = resultSubjectName;


                //foreach (DataRow dr in dt.Rows)
                //{

                //}
            }
            catch (Exception e)
            {
                log.Error(e, "FTP and Read File  Sync Master User Exception");
                return ("Ftp Connection Failed");
            }


            var masterSubjectService = new MasterSubjectService();

            var masterSubjectList = masterSubjectService.GetAllDG0206();

            int countAll = 0;
            int countSuccess = 0;
            int countFailed = 0;

            if (dtForSubject != null && dtForSubject.Rows.Count > 0)
            {
                countAll = dtForSubject.Rows.Count;
                var list = new List<MasterDg0206>();

                foreach (DataRow item in dtForSubject.Rows)
                {
                    try
                    {
                        var newItem = item.ItemArray.Select(o => o == null ? String.Empty : o.ToString()).ToList();
                        var masterSubject = new MasterDg0206();
                        if(BuildModel(masterSubject, newItem))
                        {
                            //check duplicated coursecode
                            var duplicated = list.Where(x => x.CourseCode == masterSubject.CourseCode).FirstOrDefault();

                            if (duplicated == null)
                            {
                                list.Add(masterSubject);
                                countSuccess++;
                            }
                            else
                            {
                                countFailed++;
                            }
                        }
                        else
                        {
                            countFailed++;
                        }

                    }
                    catch (Exception)
                    {
                        //countFailed++;
                    }
                }

                var markDeleteResult = masterSubjectService.MarkAsDeleteAllDG206();

                masterSubjectService = new MasterSubjectService();
                var result = masterSubjectService.AddBulkDGResister(list);

                if (result)
                {
                    if (markDeleteResult > 0) masterSubjectService.DeleteAllMarkAsDeleteItemDG206();
                    log.Debug("Successfully Save MasterSubject to Database");
                    
                }
                else
                {
                    masterSubjectService.UnMarkAsDeleteAllDg206();
                    log.Debug("RollBack Sync Master Subject");
                }

            }

            startJob.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = startJob.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            var returnString = string.Format("Total records is {0:N0} (success: {1:N0}, failed: {2:N0}) Job Duration : {3}", countAll, countSuccess, countFailed, elapsedTime);
            log.Debug("Successful Sync Master Job Assignment Detial Sub Type 1-1: {0}", returnString);
            log.Debug("Job Duration : {0}", elapsedTime);

            return returnString;

        }


        public string SyncMasterDG207()
        {
            log.Debug("Start Sync Master User from {0}", Environment.GetEnvironmentVariable("ftpMasterUserFileName"));
            DataTable dt = new DataTable();
            string fullText;

            var startJob = new Stopwatch();
            startJob.Start();

            try
            {
                SftpClient sFtpclient = new FTPService().ftpConnect((int)Constants.AppConstant.FtpServerType.Gateway);

                if (sFtpclient.IsConnected)
                {
                    //DG0207
                    MemoryStream ms = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterCourseTermFileName"), ms);
                    using (ms)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dt.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dt.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dt.Rows.Add(dr); //add other rows  
                            }

                        }
                    }
                    ms.Close();

                    sFtpclient.Disconnect();
                    log.Debug("Successfully Disconnected to SFTP :", sFtpclient.ConnectionInfo.Host);
                }
                else
                {
                    return ("Ftp Connection Failed");
                }

                //copy datatable
                //filter Current Year      
                //DataTable resultCourseName = dt.AsEnumerable()
                //.Where(row => Int32.Parse(row.Field<string>("YEAR")) >= DateTime.Now.Year)
                //.CopyToDataTable();
                //dt = resultCourseName;

                //DataTable resultSubjectName = dtForSubject.AsEnumerable()
                //.Where(row => Int32.Parse(row.Field<string>("YEAR")) >= DateTime.Now.Year)
                //.CopyToDataTable();
                //dtForSubject = resultSubjectName;


                //foreach (DataRow dr in dt.Rows)
                //{

                //}
            }
            catch (Exception e)
            {
                log.Error(e, "FTP and Read File  Sync Master User Exception");
                return ("Ftp Connection Failed");
            }


            var masterSubjectService = new MasterSubjectService();

            var masterSubjectList = masterSubjectService.GetAllDG0207();

            int countAll = 0;
            int countSuccess = 0;
            int countFailed = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                countAll = dt.Rows.Count;
                var list = new List<MasterDg0207>();

                foreach (DataRow item in dt.Rows)
                {
                    try
                    {
                        var newItem = item.ItemArray.Select(o => o == null ? String.Empty : o.ToString()).ToList();
                        var masterSubject = new MasterDg0207();
                        if (BuildModelForDG0207(masterSubject, newItem))
                        {
                            //check duplicated coursecode
                            var duplicated = list.Where(x => x.CourseNo == masterSubject.CourseNo).FirstOrDefault();

                            if (duplicated == null)
                            {
                                list.Add(masterSubject);
                                countSuccess++;
                            }
                            else
                            {
                                countFailed++;
                            }
                        }
                        else
                        {
                            countFailed++;
                        }

                    }
                    catch (Exception)
                    {
                        //countFailed++;
                    }
                }

                var markDeleteResult = masterSubjectService.MarkAsDeleteAllDG207();

                masterSubjectService = new MasterSubjectService();
                var result = masterSubjectService.AddBulkDG0207(list);
                
                if (result)
                {
                    if (markDeleteResult > 0) masterSubjectService.DeleteAllMarkAsDeleteItemDG0207();
                    log.Debug("Successfully Save MasterSubject to Database");

                }
                else
                {
                    masterSubjectService.UnMarkAsDeleteAllDg0207();
                    log.Debug("RollBack Sync Master Subject");
                }

            }

            startJob.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = startJob.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            var returnString = string.Format("Total records is {0:N0} (success: {1:N0}, failed: {2:N0}) Job Duration : {3}", countAll, countSuccess, countFailed, elapsedTime);
            log.Debug("Successful Sync Master Job Assignment Detial Sub Type 1-1: {0}", returnString);
            log.Debug("Job Duration : {0}", elapsedTime);

            return returnString;

        }

        public bool BuildModel(MasterDg0206 masterSubject, List<string> newItem)
        {
            try
            {
                masterSubject.YearCode = newItem[0];
                masterSubject.Semester = newItem[1];
                masterSubject.CourseCode = newItem[2];
                masterSubject.StudyPrpgramSystem = newItem[18];
                

                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }

        }


        public bool BuildModelForDG0207(MasterDg0207 masterSubject, List<string> newItem)
        {
            try
            {
                masterSubject.CourseNo = newItem[0];
                masterSubject.Degree = newItem[2];
                masterSubject.Major = newItem[3];
                            
                masterSubject.ProgramSystem = newItem[7];
                masterSubject.Calendar = newItem[8];

                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }

        }
    }
}
