﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CU.Models
{
    public class PersonalModel
    {
    }

    public class TitleName
    {
        public string ANRED { get; set; }
        public string ATEXT { get; set; }
        public string ANRLT { get; set; }
        public string TitleEng { get; set; }
    }

    public class JobTitle
    {
        public string TITLE { get; set; }
        public string TTOUT { get; set; }
        public string ZTOUT { get; set; }
    }

    public class Country
    {
        public string Land1 { get; set; }
        public string Landx { get; set; }
        public string Natio { get; set; }
        public string Natio50 { get; set; }
    }

    public class City
    {
        public string Bezei { get; set; }
        public string Bland { get; set; }
    }

    public class District
    {
        public string Bezei { get; set; }
        public string Bland { get; set; }
        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
    }

    public class SubDistrict
    {
        public string Bezei { get; set; }
        public string Bland { get; set; }
        public string District { get; set; }
        public string Subdistr { get; set; }
        public string SubdistrName { get; set; }
    }

    public class BankInfo
    {
        public string Bankl { get; set; }
        public string Banka { get; set; }
        public string Branch { get; set; }
        public string BankName { get; set; }
    }
}
