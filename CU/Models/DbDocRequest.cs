﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbDocRequest
    {
        public int Id { get; set; }
        public int InstructorId { get; set; }
        public int InstitutionCode { get; set; }
        public int Programid { get; set; }
        public int Courseid { get; set; }
        public string DocStatusCode { get; set; }
        public string ReqNo { get; set; }
        public bool? IsFromExcel { get; set; }
    }
}
