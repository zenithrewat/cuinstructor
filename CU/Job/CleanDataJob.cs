﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using CUEnterprisePortal.Models;
using CUEnterprisePortal.Services;
using NLog;

namespace CUEnterprisePortal.Job
{
    public class CleanDataJob
    {
        private ILogger log = NLog.LogManager.GetCurrentClassLogger();
        public void Execute()
        {
            log.Info("Clean Data Job is executing.");
            CleanData();
            log.Info("Clean Data Job is finishing.");
        }

        public void CleanData()
        {
            try
            {
                OtpService otpService = new OtpService();
                otpService.CleanExpiredOtp();

                //ShortUrlService shortUrlService = new ShortUrlService();
                //shortUrlService.CleanExpiredUrl();
            }
            catch (Exception err)
            {
                log.Error(err);
            }
        }
    }
}
