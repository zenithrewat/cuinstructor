﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int? GroupId { get; set; }
        public string PersonnelId { get; set; }
        public string Email { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TitleTh { get; set; }
        public string NameTh { get; set; }
        public string SurnameTh { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Nation { get; set; }
        public string NameEn { get; set; }
        public string SurnameEn { get; set; }
        public string CitizenId { get; set; }
        public string PassportNumber { get; set; }
        public string PersonnelGroupId { get; set; }
        public string PersonnelGroupName { get; set; }
        public string PersonnelSupGroupId { get; set; }
        public string PersonnelSupGroupName { get; set; }
        public string Werks { get; set; }
        public string WerksText { get; set; }
        public string Btrtl { get; set; }
        public string BtrtlText { get; set; }
        public string StructureLevel1Id { get; set; }
        public string StructureLevel1Name { get; set; }
        public string StructureLevel2Id { get; set; }
        public string StructureLevel2Name { get; set; }
        public string StructureLevel3Id { get; set; }
        public string StructureLevel3Name { get; set; }
        public string StructureLevel4Id { get; set; }
        public string StructureLevel4Name { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public string ContractTypeId { get; set; }
        public string ContractTypeName { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string StatusId { get; set; }
        public string StatusName { get; set; }
        public string EmployeeName { get; set; }
        public int? Creator { get; set; }
        public string Creatorname { get; set; }
        public int? Editor { get; set; }
        public string Editorname { get; set; }
        public bool? IsFixed { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
