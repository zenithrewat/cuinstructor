﻿using System;
using System.ComponentModel.DataAnnotations;
using CU.Constants;

namespace CU.Models
{
    public class SmtpDisplay
    {
        public int id { get; set; }
        public string host_name { get; set; }
        public int port { get; set; }
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "ไม่สามารถดำเนินการได้เนื่องจากอีเมลไม่ถูกต้อง")]
        public string sender_email { get; set; }
        public string sender_name { get; set; }
        public bool enable_ssl { get; set; }
        public string security_protocol { get; set; }
        public bool is_authenticated { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        [Compare("password", ErrorMessage = AppConstant.PassNotMacth)]
        public string confirmpassword { get; set; }
        public bool is_default { get; set; }
        public int creator { get; set; }
        public string creator_name { get; set; }
        public int editor { get; set; }
        public string editor_name { get; set; }
        public DateTime created_date { get; set; }
    }
}