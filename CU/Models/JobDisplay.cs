﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CU.Models
{
    public class JobDisplay
    {
        public int Id { get; set; }
        [Required]
        public string JobName { get; set; }
        [Required]
        public string Minutes { get; set; }
        [Required]
        public string Hours { get; set; }
        [Required]
        public string DayofMonth { get; set; }
        [Required]
        public string Month { get; set; }
        [Required]
        public string DayofWeek { get; set; }
        public DateTime NextRun { get; set; }
        public string Status { get; set; }
        [Required]
        public bool JobEnable { get; set; }
        public string GetCronEx()
        {
            return Minutes + " " + Hours + " " + DayofMonth + " " + Month + " " + DayofWeek;
        }
    }
}