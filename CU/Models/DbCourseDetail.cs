﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbCourseDetail
    {
        public int Id { get; set; }
        public int InstructorId { get; set; }
        public int CourseId { get; set; }
        public string DocStatusCode { get; set; }
        public bool? CriterionStatus { get; set; }
    }
}
