﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CU.Models
{
    public partial class CUSpecialInstructorContext : DbContext
    {
        public CUSpecialInstructorContext()
        {
        }

        public CUSpecialInstructorContext(DbContextOptions<CUSpecialInstructorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DbActivityLogging> DbActivityLoggings { get; set; }
        public virtual DbSet<DbAddress> DbAddresses { get; set; }
        public virtual DbSet<DbCommand> DbCommands { get; set; }
        public virtual DbSet<DbCommander> DbCommanders { get; set; }
        public virtual DbSet<DbCourse> DbCourses { get; set; }
        public virtual DbSet<DbCourseDetail> DbCourseDetails { get; set; }
        public virtual DbSet<DbDocRequest> DbDocRequests { get; set; }
        public virtual DbSet<DbEducation> DbEducations { get; set; }
        public virtual DbSet<DbEducationCode> DbEducationCodes { get; set; }
        public virtual DbSet<DbExperience> DbExperiences { get; set; }
        public virtual DbSet<DbGroupDoc> DbGroupDocs { get; set; }
        public virtual DbSet<DbGroupPosition> DbGroupPositions { get; set; }
        public virtual DbSet<DbInstructor> DbInstructors { get; set; }
        public virtual DbSet<DbJobcronSchedule> DbJobcronSchedules { get; set; }
        public virtual DbSet<DbResearch> DbResearches { get; set; }
        public virtual DbSet<DbRoleAuthority> DbRoleAuthorities { get; set; }
        public virtual DbSet<DbSecurityRole> DbSecurityRoles { get; set; }
        public virtual DbSet<DbSecurityUser> DbSecurityUsers { get; set; }
        public virtual DbSet<DbSecurityUserRole> DbSecurityUserRoles { get; set; }
        public virtual DbSet<DbStatus> DbStatuses { get; set; }
        public virtual DbSet<DbStudyLevel> DbStudyLevels { get; set; }
        public virtual DbSet<DbSubject> DbSubjects { get; set; }
        public virtual DbSet<DbSubjectSemester> DbSubjectSemesters { get; set; }
        public virtual DbSet<DbTitleRank> DbTitleRanks { get; set; }
        public virtual DbSet<DbWorkExperience> DbWorkExperiences { get; set; }
        public virtual DbSet<MasterBank> MasterBanks { get; set; }
        public virtual DbSet<MasterCountry> MasterCountries { get; set; }
        public virtual DbSet<MasterCourse> MasterCourses { get; set; }
        public virtual DbSet<MasterDepartment> MasterDepartments { get; set; }
        public virtual DbSet<MasterDg0206> MasterDg0206s { get; set; }
        public virtual DbSet<MasterDg0207> MasterDg0207s { get; set; }
        public virtual DbSet<MasterDistrict> MasterDistricts { get; set; }
        public virtual DbSet<MasterDivision> MasterDivisions { get; set; }
        public virtual DbSet<MasterDocStatus> MasterDocStatuses { get; set; }
        public virtual DbSet<MasterExpert> MasterExperts { get; set; }
        public virtual DbSet<MasterGroupStudy> MasterGroupStudies { get; set; }
        public virtual DbSet<MasterInstitution> MasterInstitutions { get; set; }
        public virtual DbSet<MasterLdap> MasterLdaps { get; set; }
        public virtual DbSet<MasterNotification> MasterNotifications { get; set; }
        public virtual DbSet<MasterPosition> MasterPositions { get; set; }
        public virtual DbSet<MasterPostCode> MasterPostCodes { get; set; }
        public virtual DbSet<MasterProvince> MasterProvinces { get; set; }
        public virtual DbSet<MasterSapdegree> MasterSapdegrees { get; set; }
        public virtual DbSet<MasterSapinstitution> MasterSapinstitutions { get; set; }
        public virtual DbSet<MasterSapmajor> MasterSapmajors { get; set; }
        public virtual DbSet<MasterSftp> MasterSftps { get; set; }
        public virtual DbSet<MasterSmtp> MasterSmtps { get; set; }
        public virtual DbSet<MasterSubDistrict> MasterSubDistricts { get; set; }
        public virtual DbSet<MasterSubject> MasterSubjects { get; set; }
        public virtual DbSet<MasterSubject50> MasterSubject50s { get; set; }
        public virtual DbSet<MasterTitleName> MasterTitleNames { get; set; }
        public virtual DbSet<MasterTypeOfWork> MasterTypeOfWorks { get; set; }
        public virtual DbSet<MasterTypeResearch> MasterTypeResearches { get; set; }
        public virtual DbSet<MasterUser> MasterUsers { get; set; }
        public virtual DbSet<UploadDocument> UploadDocuments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Environment.GetEnvironmentVariable("CONNECTION_STRING"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Thai_100_CI_AS");

            modelBuilder.Entity<DbActivityLogging>(entity =>
            {
                entity.ToTable("DbActivityLogging");

                entity.Property(e => e.Action).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Category).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Exception).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Hostname).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.IpAddress).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Level).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Message).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Originator).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Source).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Thread).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Url).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UserAgent).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Username).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<DbAddress>(entity =>
            {
                entity.ToTable("DbAddress");

                entity.Property(e => e.Alley)
                    .HasMaxLength(60)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.BuildingNo)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Country)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.District)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Floor)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.HouseNo)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Moo)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Province)
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Road)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.RoomNo)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubDistrict)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Village)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbCommand>(entity =>
            {
                entity.ToTable("DbCommand");

                entity.Property(e => e.Cmd)
                    .HasMaxLength(50)
                    .HasColumnName("CMD")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Cmdc)
                    .HasMaxLength(4)
                    .HasColumnName("CMDC")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Subty)
                    .HasMaxLength(4)
                    .HasColumnName("SUBTY")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbCommander>(entity =>
            {
                entity.ToTable("DbCommander");

                entity.Property(e => e.Reqt)
                    .HasMaxLength(50)
                    .HasColumnName("REQT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Reqtc)
                    .HasMaxLength(50)
                    .HasColumnName("REQTC")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbCourse>(entity =>
            {
                entity.ToTable("DbCourse");

                entity.Property(e => e.CoueseNo)
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CourseName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbCourseDetail>(entity =>
            {
                entity.Property(e => e.DocStatusCode)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbDocRequest>(entity =>
            {
                entity.ToTable("DbDocRequest");

                entity.Property(e => e.Courseid).HasColumnName("courseid");

                entity.Property(e => e.DocStatusCode)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Programid).HasColumnName("programid");

                entity.Property(e => e.ReqNo)
                    .IsRequired()
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbEducation>(entity =>
            {
                entity.ToTable("DbEducation");

                entity.Property(e => e.Acadamy)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Degree)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EducationLevel)
                    .IsRequired()
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Major)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbEducationCode>(entity =>
            {
                entity.ToTable("DbEducationCode");

                entity.Property(e => e.Faart)
                    .HasMaxLength(50)
                    .HasColumnName("FAART")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Slart)
                    .HasMaxLength(5)
                    .HasColumnName("SLART")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbExperience>(entity =>
            {
                entity.ToTable("DbExperience");

                entity.Property(e => e.ClassLevel)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Institution)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbGroupDoc>(entity =>
            {
                entity.ToTable("DbGroupDoc");

                entity.Property(e => e.Ntype)
                    .HasMaxLength(4)
                    .HasColumnName("NTYPE")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Text)
                    .HasMaxLength(50)
                    .HasColumnName("TEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbGroupPosition>(entity =>
            {
                entity.ToTable("DbGroupPosition");

                entity.Property(e => e.Infty)
                    .HasMaxLength(4)
                    .HasColumnName("INFTY")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Stext)
                    .HasMaxLength(50)
                    .HasColumnName("STEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Subty)
                    .HasMaxLength(4)
                    .HasColumnName("SUBTY")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbInstructor>(entity =>
            {
                entity.ToTable("DbInstructor");

                entity.Property(e => e.AccountNo)
                    .HasMaxLength(18)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.BankCode)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.BirthDate)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Branch)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DocStatusCode)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DosStatusCode)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.IdCard)
                    .HasMaxLength(30)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.IdCardEndDate)
                    .HasMaxLength(8)
                    .HasColumnName("IdCard_EndDate")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.IdCardStartDate)
                    .HasMaxLength(8)
                    .HasColumnName("IdCard_StartDate")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MilitaryTitleCode)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.NameEng)
                    .HasMaxLength(60)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.NameTh)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.NationCode)
                    .HasMaxLength(3)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Passport)
                    .HasMaxLength(30)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PassportEndDate)
                    .HasMaxLength(8)
                    .HasColumnName("Passport_EndDate")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PassportStartDate)
                    .HasMaxLength(8)
                    .HasColumnName("Passport_StartDate")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PerId)
                    .IsRequired()
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Sex)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SurNameEng)
                    .HasMaxLength(60)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SurNameTh)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TaxId)
                    .HasMaxLength(13)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleCode1)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleCode2)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleCode3)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleEngCode)
                    .HasMaxLength(2)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleOther)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleThCode)
                    .HasMaxLength(2)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbJobcronSchedule>(entity =>
            {
                entity.ToTable("DbJobcronSchedule");

                entity.Property(e => e.DayofMonth)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DayofWeek)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Hours)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.JobName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Minutes)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Month)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbResearch>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("DbResearch");

                entity.Property(e => e.AuthorName)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Journal)
                    .HasMaxLength(250)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.JournalDb)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.JournalNo)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.JournalPage)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.JournalYear)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ResearchType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Subject)
                    .HasMaxLength(500)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TypeOfWork)
                    .IsRequired()
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbRoleAuthority>(entity =>
            {
                entity.Property(e => e.AuthorityCode)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.DbRoleAuthorities)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_RoleAuthorities_SecurityRole");
            });

            modelBuilder.Entity<DbSecurityRole>(entity =>
            {
                entity.ToTable("DbSecurityRole");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Description).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Rolename)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbSecurityUser>(entity =>
            {
                entity.ToTable("DbSecurityUser");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Description).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EmailSecondary)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasMaxLength(512)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<DbSecurityUserRole>(entity =>
            {
                entity.Property(e => e.CreatorName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.DbSecurityUserRoles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_SecurityRole_SecurityUserRoles");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DbSecurityUserRoles)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_SecurityUser_SecurityUserRoles");
            });

            modelBuilder.Entity<DbStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("DbStatus");

                entity.Property(e => e.CommiteeDateExpert).HasColumnName("CommiteeDate_Expert");

                entity.Property(e => e.CommiteeNo)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CommiteeNoExpert)
                    .HasMaxLength(50)
                    .HasColumnName("CommiteeNo_Expert")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.IsFormerCu).HasColumnName("IsFormerCU");
            });

            modelBuilder.Entity<DbStudyLevel>(entity =>
            {
                entity.ToTable("DbStudyLevel");

                entity.Property(e => e.Slart)
                    .HasMaxLength(5)
                    .HasColumnName("SLART")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Stext)
                    .HasMaxLength(50)
                    .HasColumnName("STEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbSubject>(entity =>
            {
                entity.ToTable("DbSubject");

                entity.Property(e => e.ComitteeDate)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ComitteeNo)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EndDate)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.InstructorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StandardCriterion)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StartDate)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubjectName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubjectNo)
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbSubjectSemester>(entity =>
            {
                entity.ToTable("DbSubjectSemester");
            });

            modelBuilder.Entity<DbTitleRank>(entity =>
            {
                entity.ToTable("DbTitleRank");

                entity.Property(e => e.Art)
                    .HasMaxLength(2)
                    .HasColumnName("ART")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Titel)
                    .HasMaxLength(50)
                    .HasColumnName("TITEL")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Ttout)
                    .HasMaxLength(50)
                    .HasColumnName("TTOUT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Ztout)
                    .HasMaxLength(50)
                    .HasColumnName("ZTOUT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<DbWorkExperience>(entity =>
            {
                entity.ToTable("DbWorkExperience");

                entity.Property(e => e.Company)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Position)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterBank>(entity =>
            {
                entity.ToTable("MasterBank");

                entity.Property(e => e.BankName)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Banka)
                    .HasMaxLength(50)
                    .HasColumnName("BANKA")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Bankl)
                    .HasMaxLength(50)
                    .HasColumnName("BANKL")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Brnch)
                    .HasMaxLength(50)
                    .HasColumnName("BRNCH")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterCountry>(entity =>
            {
                entity.ToTable("MasterCountry");

                entity.Property(e => e.Land1)
                    .HasMaxLength(5)
                    .HasColumnName("LAND1")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Landx)
                    .HasMaxLength(50)
                    .HasColumnName("LANDX")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Natio)
                    .HasMaxLength(50)
                    .HasColumnName("NATIO")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Natio50)
                    .HasMaxLength(50)
                    .HasColumnName("NATIO50")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterCourse>(entity =>
            {
                entity.ToTable("MasterCourse");

                entity.Property(e => e.Calendar)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Degree)
                    .HasMaxLength(128)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Departmentcode)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Facultycode)
                    .HasMaxLength(2)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Levelcode)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Major)
                    .HasMaxLength(128)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Muano)
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProgramnameEn)
                    .HasMaxLength(100)
                    .HasColumnName("Programname_en")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProgramnameTh)
                    .HasMaxLength(100)
                    .HasColumnName("Programname_th")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Programsystem)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitledegreeEn)
                    .HasMaxLength(100)
                    .HasColumnName("Titledegree_en")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitledegreeTh)
                    .HasMaxLength(100)
                    .HasColumnName("Titledegree_th")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterDepartment>(entity =>
            {
                entity.ToTable("MasterDepartment");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DepartmentCode)
                    .IsRequired()
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DepartmentNameEn)
                    .HasColumnName("DepartmentNameEN")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DepartmentNameTh)
                    .IsRequired()
                    .HasColumnName("DepartmentNameTH")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterDg0206>(entity =>
            {
                entity.ToTable("MasterDG0206");

                entity.Property(e => e.CourseCode)
                    .HasMaxLength(7)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Semester)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StudyPrpgramSystem)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.YearCode)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterDg0207>(entity =>
            {
                entity.ToTable("MasterDG0207");

                entity.Property(e => e.Calendar)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CourseNo)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Degree)
                    .HasMaxLength(128)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Major)
                    .HasMaxLength(128)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProgramSystem)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterDistrict>(entity =>
            {
                entity.ToTable("MasterDistrict");

                entity.Property(e => e.Bland)
                    .HasMaxLength(5)
                    .HasColumnName("BLAND")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.District)
                    .HasMaxLength(5)
                    .HasColumnName("DISTRICT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DistrictText)
                    .HasMaxLength(50)
                    .HasColumnName("DISTRICT_TEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Land1)
                    .HasMaxLength(2)
                    .HasColumnName("LAND1")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterDivision>(entity =>
            {
                entity.ToTable("MasterDivision");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Description).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Divisionid)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Name)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.NameEn)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterDocStatus>(entity =>
            {
                entity.ToTable("MasterDocStatus");

                entity.Property(e => e.DocStatus)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DocStatusCode)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterExpert>(entity =>
            {
                entity.Property(e => e.CommiteeDate)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CommiteeNo)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.InstitutionId)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterGroupStudy>(entity =>
            {
                entity.ToTable("MasterGroupStudy");

                entity.Property(e => e.Creatorname).HasMaxLength(50);

                entity.Property(e => e.EditorName).HasMaxLength(50);

                entity.Property(e => e.GroupStudyName).IsRequired();

                entity.Property(e => e.InstitutionCode).IsRequired();
            });

            modelBuilder.Entity<MasterInstitution>(entity =>
            {
                entity.ToTable("MasterInstitution");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.InstitutionCode)
                    .IsRequired()
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.InstitutionNameEn)
                    .HasColumnName("InstitutionNameEN")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.InstitutionNameTh)
                    .IsRequired()
                    .HasColumnName("InstitutionNameTH")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterLdap>(entity =>
            {
                entity.ToTable("MasterLdap");

                entity.Property(e => e.BasePath).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Description).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DirectoryPath).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DomainName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Hostname)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ProtocolVersion)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Username)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterNotification>(entity =>
            {
                entity.ToTable("MasterNotification");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MailBody).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MailCc)
                    .HasMaxLength(100)
                    .HasColumnName("MailCC")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MailFrom)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MailKey)
                    .HasMaxLength(10)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MailSubject)
                    .HasMaxLength(500)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.MailTo)
                    .HasMaxLength(100)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterPosition>(entity =>
            {
                entity.ToTable("MasterPosition");

                entity.Property(e => e.Plans)
                    .HasMaxLength(50)
                    .HasColumnName("PLANS")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Stext)
                    .HasMaxLength(50)
                    .HasColumnName("STEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Subty)
                    .HasMaxLength(4)
                    .HasColumnName("SUBTY")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterPostCode>(entity =>
            {
                entity.ToTable("MasterPostCode");

                entity.Property(e => e.Bland)
                    .HasMaxLength(5)
                    .HasColumnName("BLAND")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.District)
                    .HasMaxLength(5)
                    .HasColumnName("DISTRICT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Land1)
                    .HasMaxLength(5)
                    .HasColumnName("LAND1")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Pstlz)
                    .HasMaxLength(5)
                    .HasColumnName("PSTLZ")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Subdistr)
                    .HasMaxLength(5)
                    .HasColumnName("SUBDISTR")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterProvince>(entity =>
            {
                entity.Property(e => e.Bezei)
                    .HasMaxLength(50)
                    .HasColumnName("BEZEI")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Bland)
                    .HasMaxLength(50)
                    .HasColumnName("BLAND")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Land1)
                    .HasMaxLength(2)
                    .HasColumnName("LAND1")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSapdegree>(entity =>
            {
                entity.ToTable("MasterSAPDegree");

                entity.Property(e => e.Faart)
                    .HasMaxLength(50)
                    .HasColumnName("FAART")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Ftext)
                    .HasMaxLength(50)
                    .HasColumnName("FTEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Langu)
                    .HasMaxLength(5)
                    .HasColumnName("LANGU")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSapinstitution>(entity =>
            {
                entity.ToTable("MasterSAPInstitution");

                entity.Property(e => e.Insti)
                    .HasMaxLength(250)
                    .HasColumnName("INSTI")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Schcd)
                    .HasMaxLength(50)
                    .HasColumnName("SCHCD")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Slart)
                    .HasMaxLength(50)
                    .HasColumnName("SLART")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSapmajor>(entity =>
            {
                entity.ToTable("MasterSAPMajor");

                entity.Property(e => e.Atext)
                    .HasMaxLength(50)
                    .HasColumnName("ATEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Ausbi)
                    .HasMaxLength(50)
                    .HasColumnName("AUSBI")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Langu)
                    .HasMaxLength(50)
                    .HasColumnName("LANGU")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSftp>(entity =>
            {
                entity.ToTable("MasterSftp");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.DrivePath)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PathUpload)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ServerIp)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.Username)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSmtp>(entity =>
            {
                entity.ToTable("MasterSmtp");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Description).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Hostname)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SecurityProtocol)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SenderEmail)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SenderName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Username)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSubDistrict>(entity =>
            {
                entity.ToTable("MasterSubDistrict");

                entity.Property(e => e.Bland)
                    .HasMaxLength(5)
                    .HasColumnName("BLAND")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.District)
                    .HasMaxLength(5)
                    .HasColumnName("DISTRICT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Land1)
                    .HasMaxLength(2)
                    .HasColumnName("LAND1")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Subdistr)
                    .HasMaxLength(5)
                    .HasColumnName("SUBDISTR")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubdistrText)
                    .HasMaxLength(50)
                    .HasColumnName("SUBDISTR_TEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSubject>(entity =>
            {
                entity.ToTable("MasterSubject");

                entity.Property(e => e.CoursenameEn)
                    .HasMaxLength(100)
                    .HasColumnName("Coursename_en")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CoursenameTh)
                    .HasMaxLength(100)
                    .HasColumnName("Coursename_th")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Courseno)
                    .HasMaxLength(7)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StudyProgramSystem)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterSubject50>(entity =>
            {
                entity.ToTable("MasterSubject50");

                entity.Property(e => e.CommitteYear)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CourseName).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CourseNo)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.InstitutionCode).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SubjectName)
                    .HasMaxLength(128)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterTitleName>(entity =>
            {
                entity.ToTable("MasterTitleName");

                entity.Property(e => e.Anred)
                    .HasMaxLength(2)
                    .HasColumnName("ANRED")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Anrlt)
                    .HasMaxLength(50)
                    .HasColumnName("ANRLT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Atext)
                    .HasMaxLength(50)
                    .HasColumnName("ATEXT")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleEng)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterTypeOfWork>(entity =>
            {
                entity.ToTable("MasterTypeOfWork");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TypeOfWork)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterTypeResearch>(entity =>
            {
                entity.ToTable("MasterTypeResearch");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TypeOfResearch)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<MasterUser>(entity =>
            {
                entity.ToTable("MasterUser");

                entity.Property(e => e.Btrtl)
                    .HasMaxLength(4)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.BtrtlText)
                    .HasMaxLength(15)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CitizenId)
                    .HasMaxLength(30)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ContractEndDate).HasColumnName("CONTRACT_END_DATE");

                entity.Property(e => e.ContractTypeId)
                    .HasMaxLength(2)
                    .HasColumnName("CONTRACT_TYPE_ID")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.ContractTypeName)
                    .HasMaxLength(40)
                    .HasColumnName("CONTRACT_TYPE_NAME")
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Creatorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Editorname)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EmployeeName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.NameEn)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.NameTh)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Nation)
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PassportNumber)
                    .HasMaxLength(30)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PersonnelGroupId)
                    .HasMaxLength(1)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PersonnelGroupName)
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PersonnelId)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PersonnelSupGroupId)
                    .HasMaxLength(2)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PersonnelSupGroupName)
                    .HasMaxLength(20)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PositionId)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.PositionName)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StatusId)
                    .HasMaxLength(3)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StatusName)
                    .HasMaxLength(40)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel1Id)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel1Name)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel2Id)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel2Name)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel3Id)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel3Name)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel4Id)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.StructureLevel4Name)
                    .HasMaxLength(80)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SurnameEn)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.SurnameTh)
                    .HasMaxLength(50)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.TitleTh)
                    .HasMaxLength(5)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Username)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Werks)
                    .HasMaxLength(8)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.WerksText)
                    .HasMaxLength(30)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");
            });

            modelBuilder.Entity<UploadDocument>(entity =>
            {
                entity.ToTable("UploadDocument");

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CategoryCode)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.CreatorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Description).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.EditorName)
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.FileType)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.OriginalFileName).UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.UploadPath)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("SQL_Latin1_General_CP1_CI_AS");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
