﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterSapmajor
    {
        public int Id { get; set; }
        public string Langu { get; set; }
        public string Ausbi { get; set; }
        public string Atext { get; set; }
    }
}
