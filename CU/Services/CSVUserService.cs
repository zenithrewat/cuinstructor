﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using CUEnterprisePortal.Models;
using CUEnterprisePortal.Utilities;
using CUEnterprisePortal.Constants;

namespace CUEnterprisePortal.Services
{
    public class CSVUserService : BaseService
    {
        private static CUEnterprisePortalContext _dbContext = new CUEnterprisePortalContext();
        public bool DeleteUser(DbLoadcsv json)
        {
            int result = 0;
            try
            {
                //DbLoadcsv model = new DbLoadcsv();
                //model = Mapper.ToLoadCSV(json);

                var target = _dbContext.DbLoadcsvs.Where(x => x.Id == json.Id).FirstOrDefault();
                if(target != null)
                {
                    _dbContext.DbLoadcsvs.Remove(target);
                    result = _dbContext.SaveChanges();
                }
                return result > 0;
            }catch(Exception err)
            {
                log.Error("Error CSVUserService ", err.Message);
                return false;
            }
        }

        public List<DbLoadUserError> GetForPage(int pageIndex, String keyword, DateTime from, DateTime to, out int totalItem, out int actualIndex)
        {
            var List = new List<DbLoadUserError>();
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                IQueryable<DbLoadUserError> query = _dbcontext.DbLoadUserErrors.Where(log => log.SyncDate >= from && log.SyncDate < to && log.IsClosed == false);

                if (!String.IsNullOrEmpty(keyword))
                {
                    query = query.Where(log => log.UserName.Contains(keyword.Trim()));
                }

                totalItem = query.Count();
                if (pageIndex > 0)
                {
                    var lastPageIndex = totalItem % AppConstant.logPageSize != 0 ? (int)Math.Floor((double)totalItem / AppConstant.logPageSize) : (int)Math.Floor((double)totalItem / AppConstant.pageSize) - 1;
                    if (pageIndex > lastPageIndex) pageIndex = lastPageIndex;
                }
                else pageIndex = 0;
                actualIndex = pageIndex;

                List = query.OrderByDescending(log => log.SyncDate).Skip(AppConstant.logPageSize * pageIndex).Take(AppConstant.logPageSize).ToList();
            }

            return List;
        }

        public int GetErrorLoadCSV(DateTime loadDt)
        {
            int totaluser = 0;
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var query = _dbcontext.DbLoadUserErrors.Where(r => r.SyncDate >= loadDt && r.SyncDate <= loadDt && r.IsClosed == false).AsQueryable();
                if(query != null)
                {
                    totaluser = query.Count();
                }
            }
            return totaluser;
        }
    }
}
