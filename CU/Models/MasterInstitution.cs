﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterInstitution
    {
        public int Id { get; set; }
        public string InstitutionCode { get; set; }
        public string InstitutionNameTh { get; set; }
        public string InstitutionNameEn { get; set; }
        public string Creatorname { get; set; }
        public int? Editor { get; set; }
        public string EditorName { get; set; }
        public bool? Isfixed { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public byte? Status { get; set; }
        public int? Creator { get; set; }
    }
}
