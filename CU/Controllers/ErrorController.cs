﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;


namespace CU.Controllers
{
    public class ErrorController : BaseController
    {
        public IActionResult Index([Bind(Prefix = "id")] int statusCode = 0)
        {

            var path = this.HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (path != null)
                log.Error(path.Error.Message);

            string action = "";

            switch (statusCode)
            {
                case 401:
                    action = "Unauthorized";
                    break;
                case 403:
                    action = "Forbidden";
                    break;
                case 404:
                    // page not found
                    action = "NotFound";
                    break;
                case 500:
                    // server error
                    action = "ServerError";
                    break;
                default:
                    action = "Default";
                    break;
            }

            return RedirectToAction(action);
        }

        public IActionResult Default()
        {
            return View();
        }

        public IActionResult Unauthorized()
        {
            return View();
        }

        public IActionResult Forbidden()
        {
            return View();
        }

        public IActionResult NotFound()
        {
            return View();
        }

        public IActionResult ServerError()
        {
            return View();
        }
    }
}
