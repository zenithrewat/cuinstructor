﻿$(document).ready(function () {

    //Level Education
    $(function () {
        $.ajax({
            url: "/Education/GetLevelEducate"
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].slart + "'>" + obj.data[i].stext + "</option>";

                }
                $('#selLevelEdu').html(text);

                //if ($('#txtLevelEdu').val() !== '-1' && $('#txtLevelEdu').val() !== '') { //

                //    $('select#selLevelEdu').val($('#txtLevelEdu').val());
                //}
            }
        });
    });

    //Institution
    $('#selLevelEdu').on('change', function () {

        var code = $(this).val();
        $.ajax({
            url: "/Education/GetInstitution",
            data: { 'code': code}
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].slart + "'>" + obj.data[i].insti + "</option>";

                }
                $('#selInstitution').html(text);

                //if ($('#txtLevelEdu').val() !== '-1' && $('#txtLevelEdu').val() !== '') { //

                //    $('select#selLevelEdu').val($('#txtLevelEdu').val());
                //}
            }
        });

        //Degree
        $.ajax({
            url: "/Education/GetDegreeInfo"
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].faart + "'>" + obj.data[i].ftext + "</option>";

                }
                console.log(text);
                $('#selDegree').html(text);

                //if ($('#txtLevelEdu').val() !== '-1' && $('#txtLevelEdu').val() !== '') { //

                //    $('select#selLevelEdu').val($('#txtLevelEdu').val());
                //}
            }
        });


        //Major
        $.ajax({
            url: "/Education/GetMajorInfo"
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].ausbi + "'>" + obj.data[i].atext + "</option>";

                }
                $('#selMajor').html(text);

                //if ($('#txtLevelEdu').val() !== '-1' && $('#txtLevelEdu').val() !== '') { //

                //    $('select#selLevelEdu').val($('#txtLevelEdu').val());
                //}
            }
        });

    });

    //select country
    $(function () {
        $.ajax({
            url: "/CreateForm/GetCountry"
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].land1 + "'>" + obj.data[i].landx + "</option>";
                }
                $('#selCountry').html(text);

                //if ($('#txtCountry').val() !== '-1' && $('#txtCountry').val() !== '') { //

                //    $('select#selCountry').val($('#txtCountry').val());
                //}

                

            }

        });
    });

});