﻿using System;
using System.Collections.Generic;
using System.Linq;
using CUEnterprisePortal.Constants;
using CUEnterprisePortal.Models;
using CUEnterprisePortal.Utilities;
using Microsoft.EntityFrameworkCore;

namespace CUEnterprisePortal.Services
{
    public class RoleService : BaseService
    {
        public RoleService()
        {

        }

        public RoleDisplay GetById(int id)
        {
            var output = new RoleDisplay();
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var target = _dbcontext.DbSecurityroles.Include(r => r.DbRoleauthorities).Include(r => r.DbSecurityuserroles).Where(x => x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    output = Mapper.ToRoleDisplay(target);
                }
                else
                {
                    throw new Exception($"Role (id:{id}) not found");
                }
            }

            return output;
        }

        public List<RoleDisplay> GetAllExist()
        {
            var output = new List<RoleDisplay>();
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var targetlist = _dbcontext.DbSecurityroles.Include(r => r.DbRoleauthorities).Include(r => r.DbSecurityuserroles).Where(x => x.Isdeleted == false).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(Mapper.ToRoleDisplay(target));
                    }
                }
            }

            return output;
        }

        public List<string> GetAllExistName()
        {
            var output = new List<string>();
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var targetlist = _dbcontext.DbSecurityroles.Where(x => x.Isdeleted == false).Select(x => x.Rolename).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(target);
                    }
                }
            }

            return output;
        }

        public List<RoleDisplay> GetExistForPage(int pageIndex, String rolename, out int totalItem, out int actualIndex)
        {
            var output = new List<RoleDisplay>();
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                IQueryable<DbSecurityrole> query = _dbcontext.DbSecurityroles.Include(r => r.DbRoleauthorities).Include(r => r.DbSecurityuserroles).Where(x => x.Isdeleted == false);

                if (!String.IsNullOrEmpty(rolename))
                {
                    query = query.Where(r => r.Rolename.Contains(rolename.Trim()));
                }

                totalItem = query.Count();
                if (pageIndex > 0)
                {
                    var lastPageIndex = totalItem % AppConstant.pageSize != 0 ? (int)Math.Floor((double)totalItem / AppConstant.pageSize) : (int)Math.Floor((double)totalItem / AppConstant.pageSize) - 1;
                    if (pageIndex > lastPageIndex) pageIndex = lastPageIndex;
                }
                else pageIndex = 0;
                actualIndex = pageIndex;

                var targetlist = query.OrderBy(r => r.Created).Skip(AppConstant.pageSize * pageIndex).Take(AppConstant.pageSize).ToList();
                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(Mapper.ToRoleDisplay(target));
                    }
                }
            }

            return output;
        }

        public bool Save(RoleDisplay roleDisplay, out string Msg, out bool isNew)
        {
            Msg = "";
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var roleid = Convert.ToInt32(roleDisplay.Id);
                if (_dbcontext.DbSecurityroles.Where(r => r.Id != roleid && r.Rolename == roleDisplay.Name && r.Isdeleted == false).FirstOrDefault() == null)
                {
                    var Role = _dbcontext.DbSecurityroles.Where(u => u.Id == roleid && u.Isdeleted == false).FirstOrDefault();
                    if (Role == null)
                    {
                        // Create
                        isNew = true;
                        return CreateRole(_dbcontext, roleDisplay, out Msg);
                    }
                    else
                    {
                        // Update
                        isNew = false;
                        return UpdateRole(_dbcontext, roleDisplay, Role, out Msg);
                    }
                }
                else
                {
                    isNew = false;
                    Msg = "ไม่สามารถใช้ชื่อหน้าที่นี้ได้";
                }
            }

            return false;
        }

        public bool CreateRole(CUEnterprisePortalContext _dbcontext, RoleDisplay roleDisplay, out string Msg)
        {
            Msg = "";
            var newRole = new DbSecurityrole
            {
                Created = DateTime.UtcNow,
                Creator = AppContextHelper.User.Id,
                Creatorname = AppContextHelper.User.Fullname,

                Modified = DateTime.UtcNow,
                Editor = AppContextHelper.User.Id,
                Editorname = AppContextHelper.User.Fullname,

                Isenabled = true,
                Isdeleted = false,
                Isfixed = false,
                Status = (byte)AppConstant.Status.Normal,

                Rolename = roleDisplay.Name,
            };
            _dbcontext.DbSecurityroles.Add(newRole);

            if (_dbcontext.SaveChanges() > 0)
            {
                log.Info($"Create new role {newRole.Rolename} (id:{newRole.Id})");
                var authlist = Mapper.ToListRoleAuthority(roleDisplay, newRole.Id);
                if (authlist.Count == 0)
                {
                    Msg = "ทำการเพิ่มหน้าที่ใหม่สำเร็จ";
                    return true;
                }
                else
                {
                    _dbcontext.DbRoleauthorities.AddRange(authlist);
                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Create authories for new role {newRole.Rolename} (id:{newRole.Id})");
                        Msg = "ทำการเพิ่มหน้าที่ใหม่สำเร็จ";
                        return true;
                    }
                }
            }
            Msg = "ทำการเพิ่มหน้าที่ใหม่ไม่สำเร็จ";
            return false;
        }

        public bool UpdateRole(CUEnterprisePortalContext _dbcontext, RoleDisplay roleDisplay, DbSecurityrole Role, out string Msg)
        {
            Msg = "";

            Role.Modified = DateTime.UtcNow;
            Role.Editor = AppContextHelper.User.Id;
            Role.Editorname = AppContextHelper.User.Fullname;

            Role.Rolename = roleDisplay.Name;

            _dbcontext.DbRoleauthorities.Where(a => a.Roleid == Role.Id).ToList().ForEach(a => a.Isdeleted = true);

            if (_dbcontext.SaveChanges() > 0)
            {
                log.Info($"Update role {Role.Rolename} (id:{Role.Id})");
                var authlist = Mapper.ToListRoleAuthority(roleDisplay, Role.Id);
                if (authlist.Count == 0) return true;
                else
                {
                    _dbcontext.DbRoleauthorities.AddRange(authlist);
                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Update authories for role {Role.Rolename} (id:{Role.Id})");
                        Msg = "ทำการแก้ไขหน้าที่สำเร็จ";
                        return true;
                    }
                }
            }
            Msg = "ทำการแก้ไขหน้าที่ไม่สำเร็จ";
            return false;
        }

        public bool Delete(int id)
        {
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var target = _dbcontext.DbSecurityroles.Where(x => x.Id == id && x.Isfixed == false).FirstOrDefault();

                target.Deleted = DateTime.UtcNow;
                target.Editor = AppContextHelper.User.Id;
                target.Editorname = AppContextHelper.User.Fullname;

                target.Isdeleted = true;

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete role {target.Rolename} (id:{target.Id})");
                    return true;
                }
            }
            return false;
        }
    }
}