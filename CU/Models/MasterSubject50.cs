﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterSubject50
    {
        public int Id { get; set; }
        public string InstitutionCode { get; set; }
        public string CourseNo { get; set; }
        public string CourseName { get; set; }
        public string SubjectName { get; set; }
        public int? CommitteNo { get; set; }
        public string CommitteYear { get; set; }
        public DateTime? CommitteDate { get; set; }
        public int? Creator { get; set; }
        public string Creatorname { get; set; }
        public int? Editor { get; set; }
        public string Editorname { get; set; }
        public bool? IsFixed { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
