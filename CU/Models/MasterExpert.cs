﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterExpert
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CommiteeNo { get; set; }
        public string CommiteeDate { get; set; }
        public string InstitutionId { get; set; }
    }
}
