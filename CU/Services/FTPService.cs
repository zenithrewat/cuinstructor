﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CU.Models;
using CU.Utilities;
using Renci.SshNet;

namespace CU.Services
{
    public class FTPService : BaseService
    {
        public FTPDisplay GetById(int id)
        {
            var json = new FTPDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSftps.Where(x => x.IsDeleted == false && x.IsDefault == true && x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToFTPDisplay(target);
                }
            }

            return json;
        }

        public FTPDisplay GetDefault(int type)
        {
            var json = new FTPDisplay(type);
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSftps.Where(x => x.IsDeleted == false && x.IsDefault == true && x.Type == type).OrderByDescending(x => x.Created).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToFTPDisplay(target);
                }
            }
            return json;
        }

        public List<FTPDisplay> GetAllDefault()
        {
            var json = new List<FTPDisplay>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterSftps.Where(x => x.IsDeleted == false && x.IsDefault == true).OrderByDescending(x => x.Created).ToList();

                if (targetlist != null)
                {
                    foreach(var target in targetlist)
                    json.Add(Mapper.ToFTPDisplay(target));
                }
            }
            return json;
        }

        public bool Save(FTPDisplay json)
        {
            var model = new MasterSftp();
            json.username = (json.username ?? "");
            json.password = (json.password ?? "");
            model = Mapper.ToMasterFTP(json);

            //Insert new
            if (model.Id <= 0)
            {
                return Insert(model, json);
            }

            //Update
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSftps.Where(x => x.Id == model.Id).FirstOrDefault();

                if (target == null)
                {
                    throw new ArgumentOutOfRangeException("FTP", "Item not found.");
                }

                target.Modified = DateTime.UtcNow;
                target.Editor = AppContextHelper.User.Id;
                target.EditorName = AppContextHelper.User.Fullname;
                target.ServerIp = model.ServerIp;
                target.Port = model.Port;
                target.DrivePath = model.DrivePath;
                target.PathUpload = model.PathUpload;
                target.IsAuthenticated = model.IsAuthenticated;
                if (model.IsAuthenticated)
                {
                    target.Username = model.Username;
                    target.Password = CryptoHelpers.Encrypt(json.password);
                }

                return _dbcontext.SaveChanges() > 0;
            }
        }

        public bool Insert(MasterSftp model, FTPDisplay json)
        {
            model.IsEnabled = true;
            model.IsDeleted = false;
            model.Created = DateTime.UtcNow;
            model.Modified = DateTime.UtcNow;
            model.Deleted = new DateTime();
            model.Type = json.type;
            model.DrivePath = json.DrivePath;
            model.PathUpload = json.PathUpload;

            if (string.IsNullOrEmpty(model.CreatorName))
            {
                model.Creator = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
                model.CreatorName = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            }

            if (string.IsNullOrEmpty(model.EditorName))
            {
                model.Editor = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
                model.EditorName = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            }

            model.Password = CryptoHelpers.Encrypt(json.password);

            if (GetDefault(json.type).id == 0) model.IsDefault = true;

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterSftps.Add(model);
                _dbcontext.SaveChanges();
            }

            return model.Id > 0;
        }

        public MemoryStream GetFileStream(string filePath)
        {
            SftpClient sFtpclient = ftpConnect((int) Constants.AppConstant.FtpServerType.Gateway);
            MemoryStream ms = new MemoryStream();
            sFtpclient.DownloadFile(filePath, ms);
            sFtpclient.Disconnect();
            log.Debug("Successfully Disconnected to SFTP :", sFtpclient.ConnectionInfo.Host);
            return ms;
        }

        public SftpClient ftpConnect(int type)
        {
            MasterSftp masterSftp = new MasterSftp();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSftps.Where(f => f.IsDefault == true && f.IsDeleted == false && f.Type == type).FirstOrDefault();

                if (target != null)
                {
                    masterSftp = target;
                }
            }
            var host = masterSftp.ServerIp;
            var username = masterSftp.Username;
            var password = CryptoHelpers.Decrypt(masterSftp.Password);
            var port = masterSftp.Port;

            var client = new SftpClient(host, port, username, password);
            try
            {
                client.Connect();
                log.Debug($"Successfully Connected to SFTP : {masterSftp.ServerIp}");
            }
            catch (Exception e)
            {
                log.Error("ftp conntect error : ", e.Message);
            }
            return client;
        }
    }
}
