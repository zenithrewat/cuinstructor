﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using CU.Models;
using CU.Constants;

namespace CU.Services
{
    public class PersonalService : BaseService
    {
        public List<TitleName> GetTitle(string type)
        {
            try
            {
                var output = new List<TitleName>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {

                    var tmp = db.MasterTitleNames.Select(p => new { p.Anred, p.Atext, p.Anrlt, p.TitleEng }).OrderBy(p => p.Anred).ToList();
                    if (type == AppConstant.Local)
                    {                        
                        foreach(var t in tmp)
                        {
                            output.Add(new TitleName() {
                                ANRED = t.Anred, 
                                ATEXT =  t.Atext, 
                                ANRLT = t.Anrlt
                            });
                        }
                    }
                    else
                    {
                        foreach (var t in tmp)
                        {
                            output.Add(new TitleName()
                            {
                                ANRED = t.Anred,                                
                                TitleEng = t.TitleEng
                            });
                        }
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<JobTitle> GetJobTitle(string type)
        {
            try
            {
                var output = new List<JobTitle>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {

                    var tmp = db.DbTitleRanks.Where(p => p.Art.Contains(type)).Select(p => new { p.Titel, p.Ttout, p.Ztout }).OrderBy(p => p.Titel).ToList();
                    foreach (var t in tmp)
                    {
                        output.Add(new JobTitle()
                        {
                            TITLE = t.Titel,
                            TTOUT = t.Ttout,
                            ZTOUT = t.Ztout
                        });
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<Country> GetCountry()
        {
            try
            {
                var output = new List<Country>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {

                    var tmp = db.MasterCountries.Select(p => new { p.Natio, p.Natio50, p.Land1, p.Landx }).OrderBy(p => p.Land1).ToList();
                    foreach (var t in tmp)
                    {
                        output.Add(new Country()
                        {
                            Land1 = t.Land1,
                            Natio = t.Natio,
                            Natio50 = t.Natio50,
                            Landx = t.Landx
                        });
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<BankInfo> GetBank()
        {
            try
            {
                var output = new List<BankInfo>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {


                    var tmp = db.MasterBanks.Select(p => new { Bankl = p.Bankl.Substring(0, 3), p.BankName }).Distinct();
                    foreach (var t in tmp)
                    {
                        output.Add(new BankInfo()
                        {
                            Bankl = t.Bankl,
                            BankName = t.BankName
                        });
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<BankInfo> GetBankBranch(string bankcode)
        {
            try
            {
                var output = new List<BankInfo>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {


                    //var tmp = db.MasterBanks.Select(p => new { Bankl = p.Bankl.Substring(0, 3), p.BankName }).Distinct();
                    var tmp = db.MasterBanks.Where(p => p.Bankl.Substring(0, 3).Contains(bankcode)).ToList();
                    foreach (var t in tmp)
                    {
                        output.Add(new BankInfo()
                        {
                            Banka = t.Banka,
                            Branch = t.Brnch,
                            Bankl = t.Bankl
                        });
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }


        public List<City> GetCity(string code)
        {
            try
            {
                var output = new List<City>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {


                    var tmp = db.MasterProvinces.Where(p => p.Land1.Contains(code)).ToList();
                    foreach (var t in tmp)
                    {
                        output.Add(new City()
                        {
                            Bezei = t.Bezei,
                            Bland = t.Bland,
                        });
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<District> GetDistict(string code)
        {
            try
            {
                var output = new List<District>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {


                    var tmp = db.MasterDistricts.Where(p => p.Bland.Contains(code)).ToList();
                    foreach (var t in tmp)
                    {
                        output.Add(new District()
                        {
                            DistrictName = t.DistrictText,
                            DistrictId = t.District,
                        });
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }


        public List<SubDistrict> GetSubDistict(string citycode, string distrcode)
        {
            try
            {
                var output = new List<SubDistrict>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {


                    var tmp = db.MasterSubDistricts.Where(p => p.Bland.Contains(citycode) && p.District.Contains(distrcode)).ToList();
                    foreach (var t in tmp)
                    {
                        output.Add(new SubDistrict()
                        {
                            SubdistrName = t.SubdistrText,
                            Subdistr = t.Subdistr,
                            District = t.District
                        });
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public string GetPostCode(string citycode, string distrcode, string subdistrcode)
        {
            try
            {
                string output = null;

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {


                    var tmp = db.MasterPostCodes.Where(p => p.Bland.Contains(citycode) && p.District.Contains(distrcode) && p.Subdistr.Contains(subdistrcode)).FirstOrDefault();
                    if(tmp != null)
                    {
                        output = tmp.Pstlz;
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
