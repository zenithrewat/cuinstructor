﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CU.Constants;
using CU.Models;
using CU.Utilities;

namespace CU.Services
{
    public class MasterDepartmentService : BaseService
    {
        public MasterDepartmentService()
        {

        }

        public MasterDepartment GetById(int id)
        {
            MasterDepartment output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterDepartments.Where(x => x.Id == id).FirstOrDefault();

                if (output == null)
                {
                    throw new Exception($"Master Division (id:{id}) not found");
                }
            }

            return output;
        }

        public List<MasterDepartment> GetAll()
        {
            List<MasterDepartment> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterDepartments.ToList();
            }

            return output;
        }

        public List<KeyValuePair<string, string>> GetAllExistWitKeyword(string keyword, string unit)
        {
            var output = new List<KeyValuePair<string, string>>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var query = _dbcontext.MasterDepartments.Where(x => x.IsDeleted == false);
                if (!string.IsNullOrEmpty(unit)) query = query.Where(x => x.DepartmentCode == unit);
                if (!string.IsNullOrEmpty(keyword)) query = query.Where(x => x.DepartmentNameTh.Contains(keyword));
                var targetlist = query.OrderBy(x => x.DepartmentNameTh).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(new KeyValuePair<string, string>(target.DepartmentCode, target.DepartmentNameTh));
                    }
                }
            }

            return output;
        }

        public List<KeyValuePair<string, string>> GetAllExistWitKeywordAndLimit(string keyword, string unit, int limit)
        {
            var output = new List<KeyValuePair<string, string>>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var query = _dbcontext.MasterDepartments.Where(x => x.IsDeleted == false);
                if (!string.IsNullOrEmpty(unit)) query = query.Where(x => x.DepartmentCode == unit);
                if (!string.IsNullOrEmpty(keyword)) query = query.Where(x => x.DepartmentNameTh.Contains(keyword));
                var targetlist = query.OrderBy(x => x.DepartmentNameTh).Take(limit).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(new KeyValuePair<string, string>(target.DepartmentCode, target.DepartmentNameTh));
                    }
                }
            }

            return output;
        }

        public bool AddBulk(IEnumerable<MasterDepartment> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    model.Status = (byte)EnumHelpers.Status.Normal;
                    model.IsFixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.Editorname))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.Editorname = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterDepartments.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master Division successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master Division");
            return false;
        }

        public bool Delete(int id)
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterDepartments.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();

                if (target != null && target.IsFixed == false)
                {
                    target.Modified = DateTime.UtcNow;
                    target.Editor = ConfigHelpers.GetDefaultCreator();
                    target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                    target.IsDeleted = true;

                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Delete Master Division {target.DepartmentNameTh} (id:{target.Id})");
                        return true;
                    }
                }
            }
            return false;
        }

        public int MarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDepartments.ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.IsFixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master Division as delete");
                    }
                }
                return count;
            }
        }

        public bool UnMarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDepartments.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.IsFixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master Division");
                    return true;
                }
            }
            return false;
        }

        public bool DeleteAllMarkAsDeleteItem()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDepartments.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterDepartments.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master Division which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master Division which marked as delete");
            return false;
        }

    }
}