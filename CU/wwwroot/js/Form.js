﻿(function ($) {
    $.fn.enterNext = function () {
        var _i = 0;
        $('input[type=text], input[type=password], input[class=tagator_input], select, textarea', this)
            .each(function (index) {
                $(this)
                    .addClass('tab' + index)
                    .keydown(function (event) {
                        if (event.keyCode == 13 && !$(this).is('textarea')) {
                            $('.tab' + (index + 1)).focus();
                            event.preventDefault();
                        }
                    });
                _i = _i + 1;
            })
        $("button[type=submit]", this).addClass('tab' + (_i));
        textareaAutoSize();
    }

    $.fn.removeNext = function () {
        $('input[type=text], input[type=password], input[class=tagator_input], button[type=submit], select, textarea', this).each(function (i, el) {
            var classes = el.className.split(" ").filter(function (c) {
                return c.lastIndexOf('tab', 0) !== 0;
            });
            el.className = $.trim(classes.join(" "));
        });
    }

    $.fn.focusTextToEnd = function () {
        this.focus();
        var $thisVal = this.val();
        this.val('').val($thisVal);
        return this;
    }
})(jQuery);

$("form").enterNext();

var keepSessionAlive = false;
var keepSessionAliveUrl = null;

function SetupSessionUpdater(actionUrl) {
    keepSessionAliveUrl = actionUrl;
    var container = $("body");
    container.on("mousemove", function () { keepSessionAlive = true; });
    container.on("keydown", function () { keepSessionAlive = true; });
    CheckToKeepSessionAlive();
}

function CheckToKeepSessionAlive() {
    setTimeout("KeepSessionAlive()", 5 * 60 * 1000);
}

function KeepSessionAlive() {
    if (keepSessionAlive && keepSessionAliveUrl != null) {
        $.ajax({
            type: "POST",
            url: keepSessionAliveUrl,
        }).done(function (data) {
            console.log(data);
            if (data.Status == 'SUCCESS') {
                keepSessionAlive = false;
            }
        });
    }
    CheckToKeepSessionAlive();
}

SetupSessionUpdater('/Account/KeepSessionAlive');

function StartLoading() {
    $(".loading").fadeIn();
}

function StopLoading() {
    $(".loading").fadeOut();
}

function textareaAutoSize() {
    $('textarea.autoExpand').on('change keyup input', function () {
        $(this).css('height', 'auto').css('height', this.scrollHeight + (this.offsetHeight - this.clientHeight));
    });
}

function textareaResize() {
    $('textarea.autoExpand').each(function () {
        $(this).css('height', 'auto').css('height', this.scrollHeight + (this.offsetHeight - this.clientHeight));
    });
}
