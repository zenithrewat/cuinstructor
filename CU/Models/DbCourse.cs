﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbCourse
    {
        public int Id { get; set; }
        public string CoueseNo { get; set; }
        public string CourseName { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Deleted { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }
        public string EditorName { get; set; }
        public string CreatorName { get; set; }
        public int Creator { get; set; }
        public int Editor { get; set; }
    }
}
