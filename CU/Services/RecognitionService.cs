﻿using CUEnterprisePortal.Models;
using DalSoft.RestClient;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using TencentCloud.Common;
using TencentCloud.Common.Profile;
using TencentCloud.Faceid.V20180301;
using TencentCloud.Iai.V20200303;
using TencentCloud.Iai.V20200303.Models;

namespace CUEnterprisePortal.Services
{
    public class RecognitionService : BaseService
    {
        double faceidcriteria;

        public RecognitionService()
        {
            faceidcriteria = Convert.ToDouble(Environment.GetEnvironmentVariable("FACEID_SIM_CRITERIA"));
        }

        public bool RecognizeVideo(Microsoft.AspNetCore.Http.IFormFile file, string imgid)
        {
            FileService fileService = new FileService();

            log.Debug(fileService.WebmToMp4Based64(file));
            //TODO send to api for checking it
            return true;
        }

        public bool RecognizeImage(Microsoft.AspNetCore.Http.IFormFile file, string original, bool IsOutsource, out FaceApiResult faceApiResult)
        {
            FileService fileService = new FileService();
            string inputFile = fileService.WebmToJpegBased64(file);
            string originalFile;

            try
            {
                if (IsOutsource) originalFile = fileService.FileToJpegBased64(Convert.ToInt32(original));
                else
                {
                    var stream = new FTPService().GetFileStream(original);
                    stream = fileService.ResizeImageOnStream(stream);
                    originalFile = fileService.StreamToJpegBased64(stream);
                    stream.Close();
                }

                log.Debug(inputFile);
                log.Debug(originalFile);

                //send to api for checking it
                //** Tencent faceid api accept only 3:2 image ratio                
                faceApiResult = CompareImage(inputFile, originalFile).Result;
                if (faceApiResult.error != null)
                {
                    log.Error($"{faceApiResult.error.statusCode} {faceApiResult.error.code} {faceApiResult.error.message}");
                    return false;
                }
                log.Info($"similarity: {faceApiResult.similarity} (criteria > {faceidcriteria}%), liveness:{faceApiResult.liveness}");
                return (faceApiResult.liveness && faceApiResult.similarity > faceidcriteria);

            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                faceApiResult = new FaceApiResult();
                return false;
            }
        }

        public async Task<dynamic> CompareImage(string input, string original)
        {
            dynamic client = new RestClient(Environment.GetEnvironmentVariable("FACEIDAPI_URL"));

            dynamic result = await client.liveness.Post(new { reference = original, live = input });

            return result;
        }

        public bool ValidateFaceImage(Microsoft.AspNetCore.Http.IFormFile file, out string Msg)
        {
            Msg = "";
            try
            {
                int _maxFileSize = 1024 * 1024 * Constants.AppConstant.uploadImgLimit;
                if (file.Length > _maxFileSize)
                {
                    Msg = "Image size exceeds limit.";
                    return false;
                }

                Credential cred = new Credential
                {
                    SecretId = Environment.GetEnvironmentVariable("TENCENTCLOUD_SECRET_ID"),
                    SecretKey = Environment.GetEnvironmentVariable("TENCENTCLOUD_SECRET_KEY")
                };

                ClientProfile clientProfile = new ClientProfile();
                clientProfile.SignMethod = ClientProfile.SIGN_SHA1;

                HttpProfile httpProfile = new HttpProfile
                {
                    ReqMethod = "POST",
                    Timeout = 10,
                    Endpoint = ("iai.tencentcloudapi.com"),
                    WebProxy = Environment.GetEnvironmentVariable("HTTPS_PROXY")
                };

                clientProfile.HttpProfile = httpProfile;

                IaiClient client = new IaiClient(cred, "ap-singapore", clientProfile);

                DetectFaceRequest detectFaceRequest = new DetectFaceRequest
                {
                    MaxFaceNum = 1,
                    MinFaceSize = 40,
                    Image = new FileService().FileToJpegBased64(file),
                    NeedFaceAttributes = 0,
                    NeedQualityDetection = 1
                };

                DetectFaceResponse detectFaceResponse = client.DetectFace(detectFaceRequest).Result;

                if(detectFaceResponse != null && detectFaceResponse.FaceInfos != null)
                {
                    var completeness_req = Convert.ToInt32(Environment.GetEnvironmentVariable("FACE_COMPLETENESS"));
                    var completeness = detectFaceResponse.FaceInfos[0].FaceQualityInfo.Completeness;
                    if (detectFaceResponse.FaceInfos[0].FaceQualityInfo.Score >= completeness_req && completeness.Cheek >= completeness_req &&
                        completeness.Chin >= completeness_req && completeness.Eye >= completeness_req && completeness.Eyebrow >= completeness_req &&
                        completeness.Mouth >= completeness_req && completeness.Nose >= completeness_req) return true;
                }
                log.Info($"Image Face Quality Info : {JsonConvert.SerializeObject(detectFaceResponse.FaceInfos[0].FaceQualityInfo)}");
                return false;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                Msg = "Invalid image. Please try another image.";
                return false;
            }
            
        }
    }
}