﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterSftp
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Deleted { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public int Creator { get; set; }
        public string CreatorName { get; set; }
        public int Editor { get; set; }
        public string EditorName { get; set; }
        public string ServerIp { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsDefault { get; set; }
        public string DrivePath { get; set; }
        public string PathUpload { get; set; }
        public int? Type { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
