﻿using CU.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CU.Services;

namespace CU.Controllers
{
    public class CreateFormController : BaseController
    {
        private readonly ILogger<CreateFormController> _logger;

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SearchResult()
        {
            return View();
        }

        public IActionResult PersonalInfo()
        {
            return View();
        }

        public IActionResult WorkandCourse()
        {
            return View();
        }

       

        public IActionResult Experience()
        {
            return View();
        }

        public IActionResult Article()
        {
            return View();
        }

        public IActionResult Etc()
        {
            return View();
        }
        public IActionResult Preview()
        {
            return View();
        }
        public IActionResult Validation()
        {
            return View();
        }

        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public JsonResult GetTitle(string type)
        {
            PersonalService serv = new PersonalService();
            List<TitleName> titleNames = serv.GetTitle(type);
            return Json(GetOk(titleNames));
        }

        [HttpGet]
        public JsonResult GetJobTitle(string type)
        {
            PersonalService serv = new PersonalService();
            List<JobTitle> jobTitles = serv.GetJobTitle(type);
            return Json(GetOk(jobTitles));
        }

        [HttpGet]
        public JsonResult GetCountry()
        {
            PersonalService serv = new PersonalService();
            List<Country> countries = serv.GetCountry();
            return Json(GetOk(countries));
        }

        [HttpGet]
        public JsonResult GetBank()
        {
            PersonalService serv = new PersonalService();
            List<BankInfo> bankinfo = serv.GetBank();
            return Json(GetOk(bankinfo));
        }

        [HttpGet]
        public JsonResult GetBankBranch(string bankcode)
        {
            PersonalService serv = new PersonalService();
            List<BankInfo> bankinfo = serv.GetBankBranch(bankcode);
            return Json(GetOk(bankinfo));
        }

        [HttpGet]
        public JsonResult GetCity(string contrycode)
        {
            PersonalService serv = new PersonalService();
            List<City> cities = serv.GetCity(contrycode);
            return Json(GetOk(cities));
        }

        [HttpGet]
        public JsonResult GetDistict(string citycode)
        {
            PersonalService serv = new PersonalService();
            List<District> district = serv.GetDistict(citycode);
            return Json(GetOk(district));
        }

        [HttpGet]
        public JsonResult GetSubDistict(string citycode, string districtcode)
        {
            PersonalService serv = new PersonalService();
            List<SubDistrict> subdistrict = serv.GetSubDistict(citycode, districtcode);
            return Json(GetOk(subdistrict));
        }
        
        [HttpGet]
        public JsonResult GetPostCode(string citycode, string districtcode, string subdistrictcode)
        {
            PersonalService serv = new PersonalService();
            string subdistrict = serv.GetPostCode(citycode, districtcode, subdistrictcode);
            return Json(GetOk(subdistrict));
        }
    }
}
