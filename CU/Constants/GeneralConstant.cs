﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CU.Constants
{
    public static class GeneralConstant
    {
        public static readonly string[] ThaiDays = { "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์", "อาทิตย์" };
        public static readonly string[] FullThaiMonths = { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
        public static readonly string[] ShortThaiMonths = { "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค." };

        public const string DefaultErrorMsg = "An error occurred. Please try again later or contact our support.";
    }
}