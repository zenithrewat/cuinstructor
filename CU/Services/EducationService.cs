﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CU.Models;

namespace CU.Services
{
    public class EducationService : BaseService
    {

        public List<EducationLevel> GetLevelEducate()
        {
            try
            {
                var output = new List<EducationLevel>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {

                    var tmp = db.DbStudyLevels.Where(p => p.IsEnable == true).Select(p => new { p.Slart, p.Stext}).OrderBy(p => p.Slart).ToList();
                    if (tmp != null)
                    {
                        foreach (var t in tmp)
                        {
                            output.Add(new EducationLevel()
                            {
                                SLART = t.Slart,
                                STEXT = t.Stext,
                            });
                        }
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<EducationDegree> GetDegreeInfo()
        {
            try
            {
                var output = new List<EducationDegree>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {

                    var tmp = db.MasterSapdegrees.Where(p => p.Langu.Contains("2")).Select(p => new { p.Faart, p.Ftext }).OrderBy(p => p.Faart).ToList();
                    if (tmp != null)
                    {
                        foreach (var t in tmp)
                        {
                            output.Add(new EducationDegree()
                            {
                                FTEXT = t.Ftext                                ,
                                FAART = t.Faart,
                            });
                        }
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<EducationMajor> GetMajorInfo()
        {
            try
            {
                var output = new List<EducationMajor>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {

                    var tmp = db.MasterSapmajors.Where(p => p.Langu.Contains("2")).Select(p => new { p.Atext, p.Ausbi }).OrderBy(p => p.Ausbi).ToList();
                    if (tmp != null)
                    {
                        foreach (var t in tmp)
                        {
                            output.Add(new EducationMajor()
                            {
                                ATEXT = t.Atext,
                                AUSBI = t.Ausbi,
                            });
                        }
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public List<Institution> GetInstitution(string code)
        {
            try
            {
                var output = new List<Institution>();

                using (CUSpecialInstructorContext db = new CUSpecialInstructorContext())
                {

                    var tmp = db.MasterSapinstitutions.Where(x => x.Slart.Contains(code))
                        .Select(p => new { p.Slart, p.Insti }).OrderBy(p => p.Slart).ToList();
                    if (tmp != null)
                    {
                        foreach (var t in tmp)
                        {
                            output.Add(new Institution()
                            {
                                SLART = t.Slart,
                                INSTI = t.Insti,
                            });
                        }
                    }

                    return output;
                }
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
