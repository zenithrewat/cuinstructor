﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using CU.Models;
using CU.Constants;

namespace CU.Job
{
    public class QuartzServer
    {
        private static IScheduler _scheduler;
        public static void Start()
        {
            _scheduler = StdSchedulerFactory.GetDefaultScheduler().GetAwaiter().GetResult();
            Load();
            _scheduler.Start();
        }

        private static void Load()
        {
            JobCreator.CreateJob<MasterJob>(new JobInfo
            {
                JobName = "MasterJob",
                TriggerName = "MasterJobTrg",
                GroupName = "MasterJobGroup",
                DataParameters = null,
                CronExpression = "0 0/1 * 1/1 * ? *" // http://www.cronmaker.com/
            }, ref _scheduler) ;
        }

        public sealed class JobCreator
        {
            public static void CreateJob<T>(JobInfo jInfo, ref IScheduler scheduler) where T : IJob
            {
                JobBuilder jbuilder = JobBuilder.Create<T>();
                jbuilder.WithIdentity(jInfo.JobName, jInfo.GroupName);
                if (jInfo.DataParameters != null && jInfo.DataParameters.Any())
                {
                    foreach (var item in jInfo.DataParameters)
                    {
                        jbuilder.UsingJobData(GetDataMap(item));
                    }
                }

                IJobDetail jobDetail = jbuilder.Build();
                TriggerBuilder tBuilder = TriggerBuilder.Create();
                tBuilder.WithIdentity(jInfo.TriggerName, jInfo.GroupName)
                    .StartNow()
                    .WithCronSchedule(jInfo.CronExpression);

                ITrigger trigger = tBuilder.Build();

                scheduler.ScheduleJob(jobDetail, trigger);
                //.Create(jobType)
                //.WithIdentity(jobType.FullName)
                //.WithDescription(jobType.Name)
                //.Build();
            }

            private static JobDataMap GetDataMap(DataParameter dataParameter)
            {
                JobDataMap jDataMap = new JobDataMap();

                switch (dataParameter.Value.GetType().Name)
                {
                    case "Int32":
                        jDataMap.Add(dataParameter.Key, (int)dataParameter.Value);
                        break;
                    case "String":
                        jDataMap.Add(dataParameter.Key, dataParameter.Value);
                        break;
                }
                return jDataMap;
            }
        }
    }
}
