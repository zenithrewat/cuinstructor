﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterPosition
    {
        public int Id { get; set; }
        public string Subty { get; set; }
        public string Plans { get; set; }
        public string Stext { get; set; }
    }
}
