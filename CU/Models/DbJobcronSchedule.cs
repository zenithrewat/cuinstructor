﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbJobcronSchedule
    {
        public int Id { get; set; }
        public string JobName { get; set; }
        public string Minutes { get; set; }
        public string Hours { get; set; }
        public string DayofMonth { get; set; }
        public string Month { get; set; }
        public string DayofWeek { get; set; }
        public DateTime NextRun { get; set; }
        public bool IsExcuting { get; set; }
        public bool JobEnable { get; set; }
    }
}
