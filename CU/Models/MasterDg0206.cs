﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterDg0206
    {
        public int Id { get; set; }
        public string CourseCode { get; set; }
        public string StudyPrpgramSystem { get; set; }
        public int? Creator { get; set; }
        public string Creatorname { get; set; }
        public int? Editor { get; set; }
        public string Editorname { get; set; }
        public bool? IsFixed { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public string YearCode { get; set; }
        public string Semester { get; set; }
    }
}
