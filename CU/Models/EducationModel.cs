﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CU.Models
{
    public class EducationModel
    {
    }

    public class EducationLevel
    {
        public string SLART { get; set; }
        public string STEXT { get; set; }
    }

    public class EducationDegree
    {
        public string FAART { get; set; }
        public string FTEXT { get; set; }
    }

    public class EducationMajor
    {
        public string AUSBI { get; set; }
        public string ATEXT { get; set; }
    }

    public class Institution
    {
        public string SLART {get;set;}
        public string SCHCD { get; set; }
        
        public string INSTI { get; set; }
    }
}
