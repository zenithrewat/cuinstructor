﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CU.Models
{
    public class AuthenUser
    {
        [Required(ErrorMessage = "Username is required")]
        public string username { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string password { get; set; }

        public bool IsSystemAdmin()
        {
            return username == Environment.GetEnvironmentVariable("ADMINUSER") ? true : false;
        }

        public bool IsCorrectSystemAdmin()
        {
            return username == Environment.GetEnvironmentVariable("ADMINUSER") &&
             password == Environment.GetEnvironmentVariable("ADMINPASSWORD") ? true : false;
        }
    }
}