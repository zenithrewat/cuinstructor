﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbSubjectSemester
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public bool? IsTerm1 { get; set; }
        public bool? IsTerm2 { get; set; }
        public bool? IsSummer { get; set; }
    }
}
