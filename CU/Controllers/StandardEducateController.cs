﻿using Microsoft.AspNetCore.Mvc;
using System;
using CU.Constants;
using Microsoft.AspNetCore.Authorization;

namespace CU.Controllers
{
    public class StandardEducateController : BaseController
    {
        [Authorize(Policy = PolicyName.AdminPage)]
        public IActionResult Index()
        {
            return View();
        }
    }
}
