﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbStatus
    {
        public int Id { get; set; }
        public bool? IsFormerCu { get; set; }
        public bool? IsResearcher { get; set; }
        public string CommiteeNo { get; set; }
        public DateTime? CommiteeDate { get; set; }
        public DateTime? ResearcherStartDate { get; set; }
        public DateTime? ResearcherEndDate { get; set; }
        public bool? IsExpert { get; set; }
        public string CommiteeNoExpert { get; set; }
        public DateTime? CommiteeDateExpert { get; set; }
        public DateTime? ExpertStartDate { get; set; }
        public DateTime? ExpertEndDate { get; set; }
        public int InstructorId { get; set; }
    }
}
