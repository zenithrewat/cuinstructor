﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbSecurityUser
    {
        public DbSecurityUser()
        {
            DbSecurityUserRoles = new HashSet<DbSecurityUserRole>();
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public byte Status { get; set; }
        public int Creator { get; set; }
        public string CreatorName { get; set; }
        public int Editor { get; set; }
        public string EditorName { get; set; }
        public string Description { get; set; }
        public byte[] Version { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string EmailSecondary { get; set; }

        public virtual ICollection<DbSecurityUserRole> DbSecurityUserRoles { get; set; }
    }
}
