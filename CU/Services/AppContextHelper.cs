﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CU.Constants;
using CU.Models;
using CU.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using NLog;

namespace CU.Services
{
    public static class AppContextHelper
    {
        private static IHttpContextAccessor _contextAccessor;
        private static ISession _session => _contextAccessor.HttpContext.Session;
        public static HttpContext _httpContext => _contextAccessor.HttpContext;

        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _contextAccessor = httpContextAccessor;
        }

        public static DbSecurityUser User
        {
            get
            {
                return _session.Get<DbSecurityUser>("user");
            }
            set
            {
                _session.Set("user", value);
            }
        }

        public static List<string> Role
        {
            get
            {
                return _session.Get<List<string>>("role");
            }
            set
            {
                _session.Set("role", value);
            }
        }

        public static List<string> AuthorityCode
        {
            get
            {
                return _session.Get<List<string>>("authoritycode");
            }
            set
            {
                _session.Set("authoritycode", value);
            }
        }

        public static bool IsSystemAdmin()
        {
            if (User != null && User.Id == 0) return true;
            return false;
        }

        //public static string GetDefaultName()
        //{
        //    var realid = CryptoHelpers.Decrypt(Digitalid.Identitynumber);
        //    if (realid.Length == 13 && System.Text.RegularExpressions.Regex.IsMatch(realid, "^[0-9]{13}$"))
        //    {
        //        return Digitalid.Firstnameth + " " + Digitalid.Lastnameth;
        //    }
        //    else
        //    {
        //        return Digitalid.Firstnameen + " " + Digitalid.Lastnameen;
        //    }
        //}

        //public static string GetDefaultName(DbDigitalid dbDigitalid)
        //{
        //    if (dbDigitalid == null) return "";
        //    var realid = CryptoHelpers.Decrypt(dbDigitalid.Identitynumber);
        //    if (realid.Length == 13 && System.Text.RegularExpressions.Regex.IsMatch(realid, "^[0-9]{13}$"))
        //    {
        //        return dbDigitalid.Firstnameth + " " + dbDigitalid.Lastnameth;
        //    }
        //    else
        //    {
        //        return dbDigitalid.Firstnameen + " " + dbDigitalid.Lastnameen;
        //    }
        //}

        //public static string GetDefaultFirstName()
        //{
        //    var realid = CryptoHelpers.Decrypt(Digitalid.Identitynumber);
        //    if (realid.Length == 13 && System.Text.RegularExpressions.Regex.IsMatch(realid, "^[0-9]{13}$"))
        //    {
        //        return Digitalid.Firstnameth;
        //    }
        //    else
        //    {
        //        return Digitalid.Firstnameen;
        //    }
        //}

        //public static string GetDefaultFirstName(DbDigitalid dbDigitalid)
        //{
        //    if (dbDigitalid == null) return "";
        //    var realid = CryptoHelpers.Decrypt(dbDigitalid.Identitynumber);
        //    if (realid.Length == 13 && System.Text.RegularExpressions.Regex.IsMatch(realid, "^[0-9]{13}$"))
        //    {
        //        return dbDigitalid.Firstnameth;
        //    }
        //    else
        //    {
        //        return dbDigitalid.Firstnameen;
        //    }
        //}

        //public static string GetDefaultLastName()
        //{
        //    var realid = CryptoHelpers.Decrypt(Digitalid.Identitynumber);
        //    if (realid.Length == 13 && System.Text.RegularExpressions.Regex.IsMatch(realid, "^[0-9]{13}$"))
        //    {
        //        return Digitalid.Lastnameth;
        //    }
        //    else
        //    {
        //        return Digitalid.Lastnameen;
        //    }
        //}

        //public static string GetDefaultLastName(DbDigitalid dbDigitalid)
        //{
        //    if (dbDigitalid == null) return "";
        //    var realid = CryptoHelpers.Decrypt(dbDigitalid.Identitynumber);
        //    if (realid.Length == 13 && System.Text.RegularExpressions.Regex.IsMatch(realid, "^[0-9]{13}$"))
        //    {
        //        return dbDigitalid.Lastnameth;
        //    }
        //    else
        //    {
        //        return dbDigitalid.Lastnameen;
        //    }
        //}

        //Use for first time digitalid register only
        //public static AuthenUser Authuser
        //{
        //    get
        //    {
        //        return _session.Get<AuthenUser>("authuser");
        //    }
        //    set
        //    {
        //        _session.Set("authuser", value);
        //    }
        //}

        public static bool HasAuthority(string Code)
        {
            if (AuthorityCode != null
                && !string.IsNullOrEmpty(Code)
                && AuthorityCode.Contains(Code)) return true;
            return false;
        }

        public static bool HasGroupAuthority(string GroupCode)
        {
            if (AuthorityCode != null
                && !string.IsNullOrEmpty(GroupCode))
            {
                foreach (var Code in AuthorityCode)
                {
                    if (Code.StartsWith(GroupCode)) return true;
                }
            }
            return false;
        }

        public static bool Login(AuthenUser authuser)
        {
            if (authuser.IsSystemAdmin())
            {
                if (SystemAdminLogin(authuser).Result)
                {
                    //passlogin = true;
                    return true;
                }
                //passlogin = false;
                return false;
            }
            else
            {
                LogInResult logInResult = LdapLogin(authuser).Result;
               // passlogin = logInResult.PassLogin;
                return logInResult.Result;
            }
        }

        //Login
        public static async Task<bool> SystemAdminLogin(AuthenUser authuser)
        {
            if (authuser.IsCorrectSystemAdmin())
            {
                User = new DbSecurityUser
                {
                    Id = 0,
                    Username = Environment.GetEnvironmentVariable("ADMINUSER"),
                    Fullname = Environment.GetEnvironmentVariable("ADMINUSER"),
                    Email = "",
                };
                using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
                {
                    Role = new List<string>() { AppConstant.SystemAdmin };
                    AuthorityCode = _dbcontext.DbRoleAuthorities.Where(r => r.Role.Rolename == AppConstant.SystemAdmin && r.IsDeleted == false).Select(r => r.AuthorityCode).ToList();
                    //Digitalid = new DbDigitalid()
                    //{
                    //    Username = Environment.GetEnvironmentVariable("ADMINUSER"),
                    //    Firstnameth = Environment.GetEnvironmentVariable("ADMINUSER"),
                    //    Lastnameth = "",
                    //    Firstnameen = Environment.GetEnvironmentVariable("ADMINUSER"),
                    //    Lastnameen = "",
                    //    Identitynumber = "",
                    //    Usertype = (int)AppConstant.UserType.Outsource
                    //};
                }
                var claims = new[] {
                    new Claim(ClaimTypes.NameIdentifier, authuser.username),
                    new Claim(ClaimTypes.Name, authuser.username),
                };

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await _contextAccessor.HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(identity));
                return true;
            }
            ILogger log = NLog.LogManager.GetCurrentClassLogger();
            log.Warn($"Someone tried to access as {Environment.GetEnvironmentVariable("ADMINUSER")} with wrong password");
            return false;
        }

        public static async Task<LogInResult> LdapLogin(AuthenUser authuser)
        {
            bool passlogin = false;
            LdapAuthenticationService ldapAuthenticationService = new LdapAuthenticationService("");
            if (ldapAuthenticationService.Authenticated("", authuser))
            {
                passlogin = true;
                using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
                {
                    var currentUTC = DateTime.UtcNow;
                        var user = _dbcontext.DbSecurityUsers.Include(r => r.DbSecurityUserRoles).ThenInclude(x => x.Role).Where(u => u.Username == authuser.username && u.IsDeleted == false).FirstOrDefault();
                        if (user != null)
                        {
                            //just log in
                            var SecurityRoleList = user.DbSecurityUserRoles.Where(r => r.IsDeleted == false && r.Role.IsDeleted == false).Select(r => r.Role).ToList();
                            var SecurityRoleIdList = SecurityRoleList.Select(r => r.Id).ToList();
                            Role = SecurityRoleList.Select(r => r.Rolename).ToList();
                            AuthorityCode = _dbcontext.DbRoleAuthorities.Where(r => SecurityRoleIdList.Contains(r.RoleId) && r.IsDeleted == false).Select(r => r.AuthorityCode).ToList();
                            user.DbSecurityUserRoles = null;
                            User = user;
                            var claims = new[] {
                            new Claim(ClaimTypes.NameIdentifier, authuser.username),
                            new Claim(ClaimTypes.Name, authuser.username),
                        };

                            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                            await _contextAccessor.HttpContext.SignInAsync(
                                CookieAuthenticationDefaults.AuthenticationScheme,
                                new ClaimsPrincipal(identity));
                            return new LogInResult() { Result = true, PassLogin = passlogin };
                        }
                        else
                        {
                            //register
                            user = new DbSecurityUser
                            {
                                Created = currentUTC,
                                Creator = ConfigHelpers.GetDefaultCreator(),
                                CreatorName = ConfigHelpers.GetDefaultCreatorName(),

                                Modified = currentUTC,
                                Editor = ConfigHelpers.GetDefaultCreator(),
                                EditorName = ConfigHelpers.GetDefaultCreatorName(),

                                IsEnabled = true,
                                IsDeleted = false,
                                Status = (byte)AppConstant.Status.Normal,

                                Username = authuser.username,
                                Fullname = ldapAuthenticationService.Fullname,
                                Email = ldapAuthenticationService.Email,
                            };

                            _dbcontext.DbSecurityUsers.Add(user);
                            if (_dbcontext.SaveChanges() > 0)
                            {
                                var General = _dbcontext.DbSecurityRoles.Where(r => r.Rolename == AppConstant.General && r.IsDeleted == false).FirstOrDefault();
                                user.DbSecurityUserRoles.Add(
                                    new DbSecurityUserRole
                                    {
                                        Created = DateTime.UtcNow,
                                        Creator = ConfigHelpers.GetDefaultCreator(), //user.Id,
                                        CreatorName = ConfigHelpers.GetDefaultCreatorName(),//user.Fullname,

                                        Editor = user.Id,
                                        EditorName = user.Fullname,

                                        IsDeleted = false,

                                        UserId = user.Id,
                                        RoleId = General.Id
                                    }
                                );
                                if (_dbcontext.SaveChanges() > 0)
                                {
                                    Role = new List<string>() { AppConstant.General };
                                    AuthorityCode = General.DbRoleAuthorities.Where(r => r.IsDeleted == false).Select(r => r.AuthorityCode).ToList(); ;
                                    user.DbSecurityUserRoles = null;
                                    User = user;
                                    var claims = new[] {
                                    new Claim(ClaimTypes.NameIdentifier, authuser.username),
                                    new Claim(ClaimTypes.Name, authuser.username),
                                };

                                    var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                                    await _contextAccessor.HttpContext.SignInAsync(
                                        CookieAuthenticationDefaults.AuthenticationScheme,
                                        new ClaimsPrincipal(identity));
                                    return new LogInResult() { Result = true, PassLogin = passlogin };
                                }
                            }
                        }
                    
                }
            }
            return new LogInResult() { Result = false, PassLogin = passlogin };
        }

        public static void Logout()
        {
            _contextAccessor.HttpContext.SignOutAsync();
            _session.Clear();
        }
    }
}