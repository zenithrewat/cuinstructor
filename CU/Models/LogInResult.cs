﻿using System;
using System.ComponentModel.DataAnnotations;
using CU.Constants;
using CU.Utilities;

namespace CU.Models
{
    public class LogInResult
    {
        public bool Result { get; set; }
        public bool PassLogin { get; set; }
    }
}