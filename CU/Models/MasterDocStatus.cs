﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterDocStatus
    {
        public int Id { get; set; }
        public string DocStatusCode { get; set; }
        public string DocStatus { get; set; }
    }
}
