﻿using System;
using System.ComponentModel.DataAnnotations;
using CU.Constants;
using CU.Utilities;

namespace CU.Models
{
    public class AuthenticationDisplay
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string CitizenIdNo { get; set; }
        [Required]
        public string Birthdate { get; set; }
    }
}