﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cronos;
using CU.Models;
using CU.Utilities;

namespace CU.Services
{
    public class CronJobService : BaseService
    {
        public JobDisplay GetById(int id)
        {
            var json = new JobDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.DbJobcronSchedules.Where(x => x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.TouJobDisplay(target);
                }
            }

            return json;
        }

        public List<JobDisplay> GetAll()
        {
            var output = new List<JobDisplay>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.DbJobcronSchedules.ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(Mapper.TouJobDisplay(target));
                    }
                }
            }
            return output;
        }

        public void CalNextRunAll()
        {
            var output = new List<DbJobcronSchedule>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var joblist = _dbcontext.DbJobcronSchedules.Where(x => x.JobEnable == true).ToList();

                if (joblist != null)
                {
                    var currentUTC = DateTime.UtcNow;
                    foreach (var job in joblist)
                    {
                        if (job.NextRun == null) job.NextRun = CalNextRun(job);
                        else if(job.IsExcuting == true && job.NextRun.AddHours(2) < currentUTC)
                        {
                            // Self healing
                            job.NextRun = CalNextRun(job);
                            job.IsExcuting = false;
                            log.Error($"Job : {job.JobName} is time out");
                        }
                    }

                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Debug($"Successfully calculated next run for all jobs!");
                    }
                }
            }
        }

        public DateTime CalNextRun(DbJobcronSchedule job)
        {
            var expression = CronExpression.Parse(job.Minutes + " " + job.Hours + " " + job.DayofMonth + " " + job.Month + " " + job.DayofWeek);
            return expression.GetNextOccurrence(DateTimeOffset.Now, TimeZoneInfo.Local).Value.DateTime.ToUniversalTime();
        }

        public List<DbJobcronSchedule> GetJobsForExecuting(DateTime currentUTC)
        {
            var output = new List<DbJobcronSchedule>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.DbJobcronSchedules.Where(x => x.JobEnable == true && x.IsExcuting == false && x.NextRun < currentUTC).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        target.IsExcuting = true;
                        output.Add(target);
                    }
                    _dbcontext.SaveChanges();
                }
            }
            return output;
        }

        public bool Update(JobDisplay jobDisplay, out string Msg)
        {

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.DbJobcronSchedules.Where(x => x.Id == jobDisplay.Id).FirstOrDefault();

                if (target != null)
                {
                    target.Minutes = jobDisplay.Minutes;
                    target.Hours = jobDisplay.Hours;
                    target.DayofMonth = jobDisplay.DayofMonth;
                    target.Month = jobDisplay.Month;
                    target.DayofWeek = jobDisplay.DayofWeek;
                    target.NextRun = CalNextRun(target);
                    target.JobEnable = jobDisplay.JobEnable;

                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Update Job (id:{jobDisplay.Id})");
                        Msg = "ทำการแก้ไข Job สำเร็จ";
                        return true;
                    }
                }
            }

            Msg = "ทำการแก้ไข Job ไม่สำเร็จ";
            return false;
        }
        public bool ToggleStatus(int id)
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.DbJobcronSchedules.Where(x => x.JobEnable == true && x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    target.IsExcuting = !target.IsExcuting;
                    if (!target.IsExcuting) target.NextRun = CalNextRun(target);
                    _dbcontext.SaveChanges();
                }
            }
            return true;
        }
    }
}
