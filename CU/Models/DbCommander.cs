﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbCommander
    {
        public int Id { get; set; }
        public string Reqtc { get; set; }
        public string Reqt { get; set; }
    }
}
