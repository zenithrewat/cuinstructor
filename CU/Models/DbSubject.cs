﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbSubject
    {
        public int Id { get; set; }
        public string SubjectNo { get; set; }
        public string SubjectName { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime Created { get; set; }
        public string EditorName { get; set; }
        public int? Editor { get; set; }
        public string CreatorName { get; set; }
        public int Creator { get; set; }
        public bool? IsHour50 { get; set; }
        public string InstructorName { get; set; }
        public string ComitteeNo { get; set; }
        public string ComitteeDate { get; set; }
        public bool? IsTeachPeriod { get; set; }
        public string StandardCriterion { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int CourseId { get; set; }
        public bool IsEdit { get; set; }
    }
}
