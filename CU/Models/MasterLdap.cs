﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterLdap
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Deleted { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public byte Status { get; set; }
        public int Creator { get; set; }
        public string CreatorName { get; set; }
        public int Editor { get; set; }
        public string EditorName { get; set; }
        public string Description { get; set; }
        public string Hostname { get; set; }
        public int Port { get; set; }
        public string BasePath { get; set; }
        public string DirectoryPath { get; set; }
        public string DomainName { get; set; }
        public bool EnableSsl { get; set; }
        public string ProtocolVersion { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsDefault { get; set; }
    }
}
