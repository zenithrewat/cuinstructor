﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterNotification
    {
        public int Id { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public bool IsDeleted { get; set; }
        public int? Creator { get; set; }
        public string CreatorName { get; set; }
        public int? Editor { get; set; }
        public string EditorName { get; set; }
        public string Email { get; set; }
        public string MailKey { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public string MailCc { get; set; }
        public string MailBody { get; set; }
        public string MailSubject { get; set; }
        public bool IsEnable { get; set; }
    }
}
