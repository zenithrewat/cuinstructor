﻿using Microsoft.AspNetCore.Mvc;
using CU.Models;
using Microsoft.AspNetCore.Authorization;
using CU.Services;
using System.Linq;

namespace CU.Controllers
{
    [Authorize(Policy = Constants.PolicyName.FTPPage)]
    public class FTPController : BaseController
    {
        public IActionResult Index()
        {

            FTPService ftpServ = new FTPService();
            var ftpList = ftpServ.GetAllDefault();
            FTPDisplay gateway = ftpList.Where(f => f.type == (int)Constants.AppConstant.FtpServerType.Gateway).FirstOrDefault();
            if (gateway == null) gateway = new FTPDisplay((int)Constants.AppConstant.FtpServerType.Gateway);

            //FTPDisplay idm = ftpList.Where(f => f.type == (int)Constants.AppConstant.FtpServerType.IDM).FirstOrDefault();
            //if (idm == null) gateway = new FTPDisplay((int)Constants.AppConstant.FtpServerType.IDM);

            ViewBag.gateway = gateway;
           // ViewBag.idm = idm;

            ViewBag.showPopUp = "false";

            return View(new FTPDisplay());
        }

        public IActionResult GetById(string Id)
        {
            FTPService ftpServ = new FTPService();

            return PartialView("Detail", ftpServ.GetById(System.Convert.ToInt32(Id)));
        }

        [HttpPost]
        public ActionResult UpdateFTP(FTPDisplay model)
        {
            FTPService ftpServ = new FTPService();
            if(ModelState.IsValid)
            {
                if (ftpServ.Save(model)) return RedirectToAction("Index");
            }

            var ftpList = ftpServ.GetAllDefault();
            FTPDisplay gateway = ftpList.Where(f => f.type == (int)Constants.AppConstant.FtpServerType.Gateway).FirstOrDefault();
            if (gateway == null) gateway = new FTPDisplay((int)Constants.AppConstant.FtpServerType.Gateway);

            FTPDisplay idm = ftpList.Where(f => f.type == (int)Constants.AppConstant.FtpServerType.IDM).FirstOrDefault();
            if (idm == null) gateway = new FTPDisplay((int)Constants.AppConstant.FtpServerType.IDM);

            ViewBag.gateway = gateway;
            ViewBag.idm = idm;

            ViewBag.showPopUp = "true";
            return View("Index", model);
        }
    }
}
