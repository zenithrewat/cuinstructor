﻿using CU.Constants;
using CU.Models;
using CU.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CU.Controllers
{
    [Authorize(Policy = PolicyName.SmtpPage)]
    public class SmtpController : BaseController
    {
        public IActionResult Index()
        {
            SmtpService smtpSetting = new SmtpService();
            ViewBag.showPopUp = "false";
            return View(smtpSetting.GetDefault());
        }

        [HttpPost]
        public IActionResult Update(SmtpDisplay input)
        {
            SmtpService smtpSetting = new SmtpService();
            if (ModelState.IsValid)
            {
                if (smtpSetting.Save(input)) return RedirectToAction("Index");
            }
            ViewBag.showPopUp = "true";
            return View("Index", input);
        }

        [HttpPost]
        public JsonResult TestSmtp(string ToEmail)
        {
            log.Info("testing smtp server to {0}", ToEmail);

            SmtpService smtpService = new SmtpService();
            if (smtpService.TestAsync(ToEmail) == "")
            {
                return Json(GetSuccess());
            }

            return Json(GetError());
        }

        public IActionResult MailTemplate()
        {
            SmtpService smtpSetting = new SmtpService();
            MailTemplateDisplay model = new MailTemplateDisplay();
            try
            {
                ViewBag.MailList = smtpSetting.GetMailTemplate();
                ViewBag.showPopUp = true;
                return View(model);
            }
            catch(Exception err)
            {
                log.Error(err.Message);
                return null;
            }
            
        }

        public ActionResult Create(MailTemplateDisplay model)
        {
            //MailTemplateDisplay model = new MailTemplateDisplay();
            try
            {
                SmtpService smtpSetting = new SmtpService();
                ViewBag.showPopUp = true;
                model.Id = Convert.ToInt32(UrlDecrypt(model.TemplateId));
                if (smtpSetting.SaveMailTemplate(model)) return RedirectToAction("MailTemplate");
            }
            catch(Exception err)
            {
                log.Error(err.Message, "Error Create MailTemplate");
                return null;
            }
            
            return RedirectToAction("MailTemplate");
        }
        
        public ActionResult ResetMail()
        {
            MailTemplateDisplay model = new MailTemplateDisplay();
            return PartialView("MailDetail", model);
        }

        public IActionResult GetMailById(string Id)
        {
            //CronJobService cronJobService = new CronJobService();
            SmtpService smtpSetting = new SmtpService();
            return PartialView("MailDetail", smtpSetting.GetMailTemplateById(Convert.ToInt32(UrlDecrypt(Id))));
        }
    }
}
