﻿using Microsoft.AspNetCore.Mvc;
using CU.Constants;
using Microsoft.AspNetCore.Authorization;

namespace CU.Controllers
{
    public class WorkflowController : BaseController
    {
        [Authorize(Policy = PolicyName.AdminPage)]
        public IActionResult Index()
        {
            return View();
        }
    }
}
