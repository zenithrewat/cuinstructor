﻿using System.Threading.Tasks;
using CU.Models;
using CU.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace CU.Utilities
{
    public class CheckAuthorizationHandler : AuthorizationHandler<CheckAuthorizeRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CheckAuthorizeRequirement requirement)
        {

            var filterContext = context.Resource as AuthorizationFilterContext;
            var routeInfo = context.Resource as RouteEndpoint;
            var response = filterContext?.HttpContext.Response;

            if (AppContextHelper.User == null)
            {
                AppContextHelper.Logout();
                response?.OnStarting(async () =>
                {
                    filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                });

                context.Fail();
            }
            else
            {
                if (string.IsNullOrEmpty(requirement.AuthorityCode) || 
                    AppContextHelper.HasAuthority(requirement.AuthorityCode) ||
                    AppContextHelper.HasGroupAuthority(requirement.AuthorityCode))
                {
                    context.Succeed(requirement);
                }
                else
                {
                    response?.OnStarting(async () =>
                    {
                        filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                    });

                    context.Fail();
                }
            }

            return Task.CompletedTask;
        }
    }
}
