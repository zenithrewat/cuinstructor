﻿using CU.Job;
using CU.Models;
using CU.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CU.Controllers
{
    public class SchedulerController : BaseController
    {
        [Authorize(Policy = Constants.PolicyName.JobPage)]
        [Route("[controller]")]
        public IActionResult Index()
        {
            CronJobService cronJobService = new CronJobService();

            ViewBag.Jobs = cronJobService.GetAll();
            ViewBag.showPopUp = "false";
            return View(new JobDisplay());
        }

        [Authorize(Policy = Constants.PolicyName.JobPage)]
        [Route("[controller]/GetById/{Id}")]
        public IActionResult GetById(string Id)
        {
            CronJobService cronJobService = new CronJobService();

            return PartialView("Detail", cronJobService.GetById(Convert.ToInt32(UrlDecrypt(Id))));
        }

        [Authorize(Policy = Constants.PolicyName.JobPage)]
        [Route("[controller]/UpdateSchedule")]
        [HttpPost]
        public IActionResult UpdateSchedule(string JobID ,JobDisplay jobDisplay)
        {
            jobDisplay.Id = Convert.ToInt32(UrlDecrypt(JobID));
            CronJobService cronJobService = new CronJobService();
            if(cronJobService.Update(jobDisplay, out string Msg))
            {
                ShowSuccessModal(Msg);
                return RedirectToAction("Index");
            }
            else
            {
                ShowFailureModal(Msg);
            }

            ViewBag.Jobs = cronJobService.GetAll();
            ViewBag.showPopUp = "true";
            return View("Index", jobDisplay);
        }

        [Route("[controller]/Jobs/Run")]
        [HttpPost]
        public IActionResult RunScheduler()
        {
            if (Environment.GetEnvironmentVariable("JOBS_ENABLE") == "0")
            {
                System.Threading.Tasks.Task.Run(() =>
                {
                    MasterJob masterJob = new MasterJob();
                    masterJob.ExceuteJob();
                });
                return GetOk();
            }

            return GetError();
        }
    }
}
