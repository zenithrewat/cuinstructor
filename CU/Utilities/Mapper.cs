﻿using CU.Constants;
using CU.Models;
using CU.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CU.Utilities
{
    public static class Mapper
    {
        public static UserDisplay ToUserDisplay(DbSecurityUser input)
        {
            UserDisplay output = new UserDisplay
            {
                Id = input.Id,
                Username = input.Username,
                Fullname = input.Fullname,
                Email = input.Email,
                //EmailSecondary = input.Emailsecondary,
                Roles = input.DbSecurityUserRoles.Where(r => r.IsDeleted == false).ToList().Select(r => new Role { Id = r.Role.Id, Name = r.Role.Rolename }).ToList()
            };

            return output;
        }

        public static RoleDisplay ToRoleDisplay(DbSecurityRole input)
        {
            var roleAuthorities = input.DbRoleAuthorities.Where(x => x.IsDeleted == false).ToList();
            RoleDisplay output = new RoleDisplay
            {
                Id = input.Id.ToString(),
                Name = input.Rolename,
                UserCount = input.DbSecurityUserRoles.Where(x => x.IsDeleted == false).ToList().Count,
                AuthCount = roleAuthorities.Count,
                Created = input.Created,
                CreatorName = input.CreatorName,
                isFixed = input.IsFixed,
                _HrDigitalID = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.HrDigitalID)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _HrLesspaper = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.HrLesspaper)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _Ldap = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.Ldap)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _Log = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.Log)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _User = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.User)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _Role = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.Role)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _SmsGateway = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.SmsGateway)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _Job = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.Job)).Select(a => a.AuthorityCode).FirstOrDefault(),
                _ImportCsv = roleAuthorities.Where(r => r.AuthorityCode.StartsWith(PageAuthorityCode.ImportCsv)).Select(a => a.AuthorityCode).FirstOrDefault(),
            };

            return output;
        }

        public static List<DbRoleAuthority> ToListRoleAuthority(RoleDisplay input, int id)
        {
            List<DbRoleAuthority> roleAuthorities = new List<DbRoleAuthority>();

            if (!String.IsNullOrEmpty(input._HrDigitalID)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._HrDigitalID, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._HrLesspaper)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._HrLesspaper, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._Ldap)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._Ldap, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._Smtp)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._Smtp, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._User)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._User, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._Role)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._Role, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._Log)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._Log, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._SmsGateway)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._SmsGateway, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._Job)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._Job, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });
            if (!String.IsNullOrEmpty(input._ImportCsv)) roleAuthorities.Add(new DbRoleAuthority { RoleId = id, AuthorityCode = input._ImportCsv, Created = DateTime.UtcNow, Creator = AppContextHelper.User.Id, CreatorName = AppContextHelper.User.Fullname });

            return roleAuthorities;
        }

        public static LdapDisplay ToLdapDisplay(MasterLdap input)
        {
            LdapDisplay output = new LdapDisplay
            {
                id = input.Id,
                host_name = input.Hostname,
                port = input.Port,
                base_path = input.BasePath,
                directory_path = input.DirectoryPath,
                domain_name = input.DomainName,
                enable_ssl = input.EnableSsl,
                protocol_version = input.ProtocolVersion,
                is_authenticated = input.IsAuthenticated,
                username = input.Username,
                password = input.Password,
                is_default = input.IsDefault,
                creator = input.Creator,
                creator_name = input.CreatorName,
                editor = input.Editor,
                editor_name = input.EditorName,
                created_date = input.Created.ToLocalTime()
            };

            return output;
        }

        public static MasterLdap ToMasterLdap(LdapDisplay input)
        {
            MasterLdap output = new MasterLdap
            {
                Id = input.id,
                Hostname = input.host_name,
                Port = input.port,
                BasePath = input.base_path,
                DirectoryPath = input.directory_path,
                DomainName = input.domain_name,
                EnableSsl = input.enable_ssl,
                ProtocolVersion = input.protocol_version,
                IsAuthenticated = input.is_authenticated,
                Username = input.username,
                Password = input.password,
                IsDefault = input.is_default,
                Creator = input.creator,
                CreatorName = input.creator_name,
                Editor = input.editor,
                EditorName = input.editor_name,
                Created = input.created_date
            };

            return output;
        }

        public static SmtpDisplay ToSmtpDisplay(MasterSmtp input)
        {
            SmtpDisplay output = new SmtpDisplay
            {
                id = input.Id,
                host_name = input.Hostname,
                port = input.Port,
                sender_email = input.SenderEmail,
                sender_name = input.SenderName,
                enable_ssl = input.EnableSsl,
                security_protocol = input.SecurityProtocol,
                is_authenticated = input.IsAuthenticated,
                username = input.Username,
                password = input.Password,
                is_default = input.IsDefault,
                creator = input.Creator,
                creator_name = input.CreatorName,
                editor = input.Editor,
                editor_name = input.EditorName,
                created_date = input.Created.ToLocalTime()
            };

            return output;
        }

        public static MasterSmtp ToMasterSmtp(SmtpDisplay input)
        {
            MasterSmtp output = new MasterSmtp
            {
                Id = input.id,
                Hostname = input.host_name,
                Port = input.port,
                SenderEmail = input.sender_email,
                SenderName = input.sender_name,
                EnableSsl = input.enable_ssl,
                SecurityProtocol = input.security_protocol,
                IsAuthenticated = input.is_authenticated,
                Username = input.username,
                Password = input.password,
                IsDefault = input.is_default,
                Creator = input.creator,
                CreatorName = input.creator_name,
                Editor = input.editor,
                EditorName = input.editor_name,
                Created = input.created_date
            };

            return output;
        }

        public static FTPDisplay ToFTPDisplay(MasterSftp input)
        {
            FTPDisplay output = new FTPDisplay
            {
                id = input.Id,
                serverip = input.ServerIp,
                port = input.Port,
                isauthenticated = input.IsAuthenticated,
                username = input.Username,
                password = input.Password,
                isdefault = input.IsDefault,
                type = Convert.ToInt32(input.Type),
                creator = input.Creator,
                creatorname = input.CreatorName,
                editor = input.Editor,
                editorname = input.EditorName,
                created = input.Created.ToLocalTime(),
                deleted = input.Deleted.ToLocalTime(),
                DrivePath = input.DrivePath,
                PathUpload = input.PathUpload
            };

            return output;
        }

        public static MasterSftp ToMasterFTP(FTPDisplay input)
        {
            MasterSftp output = new MasterSftp
            {
                Id = input.id,
                ServerIp = input.serverip,
                Port = input.port,
                Deleted = input.deleted,
                IsAuthenticated = input.isauthenticated,
                Username = input.username,
                Password = input.password,
                IsDefault = input.isdefault,
                Type = input.type,
                Creator = input.creator,
                CreatorName = input.creatorname,
                Editor = input.editor,
                EditorName = input.editorname,
                Created = input.created,
                DrivePath = input.DrivePath,
                PathUpload = input.PathUpload
            };

            return output;
        }

        //public static DbLoadcsv ToLoadCSV(UserCSV input)
        //{
        //    DbLoadcsv output = new DbLoadcsv
        //    {
        //        Id = input.Id,
        //        MobileNo = input.MobileNo,
        //        UserName = input.UserName,
        //        CreatedDate = input.CreatedDate,
        //        IsOtp = input.IsOtp,
        //    };
        //    return output;

        //}

        //public static UserCSV TouserCSV(DbLoadcsv input)
        //{
        //    UserCSV output = new UserCSV
        //    {
        //        Id = input.Id,
        //        MobileNo = input.MobileNo,
        //        UserName = input.UserName,
        //        CreatedDate = Convert.ToDateTime(input.CreatedDate),
        //        IsOtp = Convert.ToBoolean(input.IsOtp),
        //    };
        //    return output;
        //}

        //public static DbLoadUserError ToLoadUserError(UserCSVError input)
        //{
        //    DbLoadUserError output = new DbLoadUserError
        //    {
        //        Id = input.Id,
        //        MobileNo = input.MobileNo,
        //        UserName = input.UserName,
        //        SyncDate = input.SyncDate
        //    };
        //    return output;
        //}

        //public static UserCSVError TouserCSVError(DbLoadUserError input)
        //{
        //    UserCSVError output = new UserCSVError
        //    {
        //        Id = input.Id,
        //        MobileNo = input.MobileNo,
        //        UserName = input.UserName,
        //        SyncDate = Convert.ToDateTime(input.SyncDate)
        //    };
        //    return output;
        //}
        //public sealed class UserCSVMap : CsvHelper.Configuration.ClassMap<UserCSV>
        //{
        //    public UserCSVMap()
        //    {
        //        Map(m => m.MobileNo).Validate(args =>
        //        {
        //            if (string.IsNullOrEmpty(args.Field))
        //            {
        //                return false;
        //            }

        //            if (args.Field.Length != 10)
        //                return false;

        //            return true;
        //        });


        //        Map(m => m.UserName).Name("UserName").Validate(args =>
        //        {

        //            if (String.IsNullOrEmpty(args.Field))
        //                return false;
        //            return true;
        //        });
                
        //    }
        //}

        public static JobDisplay TouJobDisplay(DbJobcronSchedule input)
        {
            JobDisplay output = new JobDisplay
            {
                Id = input.Id,
                JobName = input.JobName,
                Minutes = input.Minutes,
                Hours = input.Hours,
                DayofMonth = input.DayofMonth,
                Month = input.Month,
                DayofWeek = input.DayofWeek,
                NextRun = input.NextRun,
                Status = input.IsExcuting ? "Processing" : "Idle",
                JobEnable = input.JobEnable
            };
            return output;
        }

        public static CUUserModel ToCUUserModel(MasterUser input)
        {
            CUUserModel output = new CUUserModel
            {
                
                //Firstnameth = input.Firstnameth,
                //Lastnameth = input.Lastnameth,
                //Firstnameen = input.Firstnameen,
                //Lastnameen = input.Lastnameen,
                //Division = input.Structurelevel2id,
                //Unit = input.Structurelevel1id,
                //citizenid = input.Citizenid,
                //Passport = input.Passport,
                //Mobile = null,
                //Email = input.Email,
                //Image = input.Imagepath
            };

            //if (input.Birthdate.HasValue) output.Birthdate = input.Birthdate.Value;

            return output;
        }

        //public static RegisterCSVDisplay ToRegisterDisplayCSV(DbRegistercsv input)
        //{
        //    RegisterCSVDisplay output = new RegisterCSVDisplay
        //    {
        //        PerNo = "",
        //        FirstNameTh = input.Firstnameth,
        //        LastNameTh = input.Lastnameth,
        //        FirstNameEn = input.Firstnameen,
        //        LastNameEn = input.Lastnameen,
        //        Division = input.Division,
        //        Unit = input.Unit,
        //        StatusEmploy = "",
        //        GroupEmploy = "",
        //        SubGroupEmploy = "",
        //        PersonalNo = CryptoHelpers.Decrypt(input.Identitynumber),
        //        Mobile = input.Mobile
        //    };
        //    return output;
        //}

    //    public sealed class HeaderCSVMap : CsvHelper.Configuration.ClassMap<HeaderCSV>
    //    {

    //        public HeaderCSVMap()
    //        {
    //            Map(m => m.PerNo).Name("Pers.No.");
    //            Map(m => m.NameTh).Name("ชื่อ");
    //            Map(m => m.SurnameTh).Name("นามสกุล");
    //            Map(m => m.NameEn).Name("Name");
    //            Map(m => m.SurnameEn).Name("Surname");
    //            Map(m => m.Division).Name("ชื่อโครงสร้างระดับ 1");
    //            Map(m => m.Unit).Name("ชื่อโครงสร้างระดับ 2");
    //            Map(m => m.StatusEmploy).Name("สถานะการว่าจ้าง");
    //            Map(m => m.GroupEmploy).Name("กลุ่มพนักงาน");
    //            Map(m => m.SubGroupEmploy).Name("กลุ่มย่อยพนักงาน");
    //            Map(m => m.IdNumber).Name("ID Number");
    //            Map(m => m.MobileNo).Name("MobileNo");
    //        }
    //}

        //public static DbRegistercsv ToRegisterCSV(RegisterCSVDisplay input)
        //{
        //    DbRegistercsv output = new DbRegistercsv
        //    {
        //        Firstnameth = input.FirstNameTh,
        //        Lastnameth = input.LastNameTh,
        //        Firstnameen = input.FirstNameEn,
        //        Lastnameen = input.LastNameEn,
        //        Division = input.Division,
        //        Unit = input.Unit,
        //        Mobile = input.Mobile
        //    };
        //    return output;
        //}

        public static MailTemplateDisplay ToMailTemplate(MasterNotification input)
        {
            MailTemplateDisplay output = new MailTemplateDisplay
            {
                Id = input.Id,
                MailKey = input.MailKey,
                MailFrom = input.MailFrom,
                MailTo = input.MailTo,
                MailCC = input.MailCc,
                MailBody = input.MailBody,
                MailSubject = input.MailSubject,
                IsEnable = input.IsEnable
            };
            return output;
        }

        public static MasterNotification ToDbMailTemplate(MailTemplateDisplay input)
        {
            MasterNotification output = new MasterNotification
            {
                Id = input.Id,
                MailKey = input.MailKey,
                MailFrom = input.MailFrom,
                MailTo = input.MailTo,
                MailCc = input.MailCC,
                MailSubject = input.MailSubject,
                MailBody = input.MailBody,
                IsEnable = Convert.ToBoolean(input.IsEnable)
            };
            return output;
        }
    }
}