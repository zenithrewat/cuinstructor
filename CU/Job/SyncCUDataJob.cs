﻿using CU.Models;
using CU.Services;
using NLog;
using Quartz;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CU.Job
{
    public class SyncCUDataJob
    {
        protected ILogger log = NLog.LogManager.GetCurrentClassLogger();

        public void Execute()
        {
            log.Info("Sync CU Data Job is executing.");
            log.Info(SyncMasterUser());
            log.Info("Sync CU Data Job is finishing.");
        }

        public String SyncMasterUser()
        {
            log.Debug("Start Sync Master User from {0}", Environment.GetEnvironmentVariable("ftpMasterUserFileName"));
            DataTable dt = new DataTable();
            DataTable dtForUserInfo = new DataTable();
            DataTable dtForImage = new DataTable();
            DataTable dtForCUNET = new DataTable();
            DataTable dtForOper = new DataTable();
            DataTable dtForContrct = new DataTable();
            string fullText;

            var startJob = new Stopwatch();
            startJob.Start();

            try
            {
                SftpClient sFtpclient = new FTPService().ftpConnect((int)Constants.AppConstant.FtpServerType.Gateway);

                if (sFtpclient.IsConnected)
                {
                    MemoryStream ms = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterUserFileName"), ms);
                    using (ms)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dt.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dt.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dt.Rows.Add(dr); //add other rows  
                            }

                        }
                    }
                    ms.Close();

                    MemoryStream msForUserInfo = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterUserInfoFileName"), msForUserInfo);
                    using (msForUserInfo)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(msForUserInfo.ToArray());
                        //fullText = readerForEmail.ReadToEnd().ToString();
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  

                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtForUserInfo.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow drForEmail = dtForUserInfo.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    drForEmail[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dtForUserInfo.Rows.Add(drForEmail); //add other rows  
                            }

                        }
                    };
                    msForUserInfo.Close();

                    MemoryStream msForOper = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterUserOperFileName"), msForOper);
                    using (msForOper)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(msForOper.ToArray());
                            //fullText = readerForImage.ReadToEnd().ToString();
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  

                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtForOper.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow drForOper = dtForOper.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    drForOper[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dtForOper.Rows.Add(drForOper); //add other rows  
                            }

                        }
                    };
                    msForOper.Close();

                    MemoryStream msForCUNET = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpCUNETFileName"), msForCUNET);
                    using (msForCUNET)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(msForCUNET.ToArray());
                        //fullText = readerForImage.ReadToEnd().ToString();
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  

                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtForCUNET.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow drForCUNET = dtForCUNET.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    drForCUNET[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dtForCUNET.Rows.Add(drForCUNET); //add other rows  
                            }

                        }
                    };
                    msForCUNET.Close();

                    //DG Contract
                    MemoryStream msForContract = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterUserContractFileName"), msForContract);
                    using (msForContract)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(msForContract.ToArray());
                        //fullText = readerForImage.ReadToEnd().ToString();
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  

                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtForContrct.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow drForContract = dtForContrct.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    drForContract[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dtForContrct.Rows.Add(drForContract); //add other rows  
                            }

                        }
                    };
                    msForContract.Close();


                    sFtpclient.Disconnect();
                    log.Debug("Successfully Disconnected to SFTP :", sFtpclient.ConnectionInfo.Host);
                }
                else
                {
                    return ("Ftp Connection Failed");
                }

                //filter Active User
                string expression;
                expression = string.Format("End_Date > '{0}'", DateTime.Now.ToString("MM/dd/yyyy"));
                DataRow[] foundRows;
                foundRows = dt.Select(expression);

                dt = foundRows.CopyToDataTable();

                //filter Active User                
                string expression2 = string.Format("End_Date > '{0}'", DateTime.Now.ToString("MM/dd/yyyy"));
                DataRow[] foundRows2;
                foundRows2 = dtForUserInfo.Select(expression2);

                dtForUserInfo = foundRows2.CopyToDataTable();


                //filter Active User                
                string expression3 = string.Format("End_Date > '{0}'", DateTime.Now.ToString("MM/dd/yyyy"));
                DataRow[] foundRows3;
                foundRows3 = dtForOper.Select(expression3);

                dtForOper = foundRows3.CopyToDataTable();

                //filter Active User                
                string expression4 = string.Format("End_Date > '{0}'", DateTime.Now.ToString("MM/dd/yyyy"));
                DataRow[] foundRows4;
                foundRows4 = dtForContrct.Select(expression4);

                dtForContrct = foundRows4.CopyToDataTable();

                dt.Columns.Add("Email");
                dt.Columns.Add("Firstnameth");
                dt.Columns.Add("Lastnameth");
                dt.Columns.Add("Firstnameen");
                dt.Columns.Add("Lastnameen");
                dt.Columns.Add("Citizenid");
                dt.Columns.Add("Passport");
                dt.Columns.Add("Birthdate");
                dt.Columns.Add("Imagepath");
                dt.Columns.Add("Username");
                dt.Columns.Add("Nation");
                dt.Columns.Add("TitleTh");
                dt.Columns.Add("StatusId");
                dt.Columns.Add("StatusName");
                dt.Columns.Add("CONTRACT_TYPE_ID");
                dt.Columns.Add("CONTRACT_TYPE_NAME");
                dt.Columns.Add("CONTRACT_END_DATE");
                foreach (DataRow dr in dt.Rows)
                {
                    var row = (from DataRow dre in dtForUserInfo.Rows
                                       where (string)dre["PERSONNEL_ID"] == dr["PERSONNEL_ID"].ToString()
                                       select dre).FirstOrDefault();

                    if (row != null)
                    {
                        dr["Email"] = row["EMAIL"].ToString().Trim();
                        dr["Firstnameth"] = row["NAME_TH"].ToString().Trim();
                        dr["Lastnameth"] = row["SURNAME_TH"].ToString().Trim();
                        dr["Firstnameen"] = row["NAME_EN"].ToString().Trim();
                        dr["Lastnameen"] = row["SURNAME_EN"].ToString().Trim();
                        dr["Citizenid"] = row["CITIZEN_ID"].ToString().Trim();
                        dr["Passport"] = row["PASSPORT_NUMBER"].ToString().Trim();
                        dr["Birthdate"] = row["BIRTH_DATE"].ToString().Trim();
                        dr["Nation"] = row["NATION"].ToString().Trim();
                        dr["TitleTh"] = row["TITLE_TH"].ToString().Trim();
                    }

                    row = (from DataRow dre in dtForImage.Rows
                           where (string)dre["PERSONNEL_ID"] == dr["PERSONNEL_ID"].ToString()
                           select dre).FirstOrDefault();

                    if (row != null)
                    {
                        dr["Imagepath"] = Constants.AppConstant.photopath + row["IMAGE_NAME"].ToString().Trim();
                    }

                    row = (from DataRow dre in dtForCUNET.Rows
                           where (string)dre["PERSONNEL_ID"] == dr["PERSONNEL_ID"].ToString()
                           select dre).FirstOrDefault();

                    if (row != null)
                    {
                        dr["Username"] = Constants.AppConstant.photopath + row["USERNAME"].ToString().Trim();
                    }

                    row = (from DataRow dre in dtForOper.Rows
                           where (string)dre["PERSONNEL_ID"] == dr["PERSONNEL_ID"].ToString()
                           select dre).FirstOrDefault();

                    if (row != null)
                    {
                        dr["StatusId"] = row["STATUS_ID"].ToString().Trim();
                        dr["StatusName"] = row["STATUS_NAME"].ToString().Trim();
                    }

                    row = (from DataRow dre in dtForContrct.Rows
                           where (string)dre["PERSONNEL_ID"] == dr["PERSONNEL_ID"].ToString()
                           select dre).FirstOrDefault();

                    if (row != null)
                    {
                        dr["CONTRACT_TYPE_ID"] = row["CONTRACT_TYPE_ID"].ToString().Trim();
                        dr["CONTRACT_TYPE_NAME"] = row["CONTRACT_TYPE_NAME"].ToString().Trim();
                        dr["CONTRACT_END_DATE"] = row["CONTRACT_END_DATE"].ToString().Trim();
                    }
                }

                log.Debug("Successfully Load CSV File to Datatable");

            }
            catch (Exception e)
            {
                log.Error(e, "FTP and Read File  Sync Master User Exception");
                return ("Ftp Connection Failed");
            }

            var masterUserService = new MasterUserService();

            var masterUserList = masterUserService.GetAll();

            int countAll = 0;
            int countSuccess = 0;
            int countFailed = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                countAll = dt.Rows.Count;
                var list = new List<MasterUser>();

                foreach (DataRow item in dt.Rows)
                {
                    try
                    {
                        var newItem = item.ItemArray.Select(o => o == null ? String.Empty : o.ToString()).ToList();
                        var masterUser = new MasterUser();
                        if (BuildModel(masterUser, newItem, masterUserList))
                        {
                            //check duplicated employee name
                            var duplicated = list.Where(x => x.EmployeeName == masterUser.EmployeeName).FirstOrDefault();

                            if (duplicated == null)
                            {
                                list.Add(masterUser);
                                countSuccess++;
                            }
                            else
                            {
                                //if new record's PersonnelGroupId = F then remove duplicated and add new
                                if (masterUser.PersonnelGroupId.Equals("F", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    list.Remove(duplicated);
                                    list.Add(masterUser);
                                    countSuccess++;
                                }
                                else
                                {
                                    //this record will not insert because PersonnelGroupId != F and duplicated
                                    countFailed++;
                                }
                            }
                        }
                        else
                        {
                            countFailed++;
                        }
                        //countSuccess++;
                    }
                    catch (Exception)
                    {
                        //countFailed++;
                    }
                }

                var markDeleteResult = masterUserService.MarkAsDeleteAll();

                masterUserService = new MasterUserService();
                var result = masterUserService.AddBulk(list);

                if (result)
                {
                    if (markDeleteResult > 0) masterUserService.DeleteAllMarkAsDeleteItem();
                    log.Debug("Successfully Save MasterUser to Database");

                    var newMasterUser = masterUserService.GetAll();

                    //Create New MasterFaculty or MasterInstitution
                    var masterFacultyList = new List<MasterInstitution>();

                    masterFacultyList = newMasterUser.Where(x => !String.IsNullOrEmpty(x.StructureLevel1Name) && !String.IsNullOrEmpty(x.StructureLevel1Name.Trim()))
                        .Select(x => new MasterInstitution { InstitutionCode = x.StructureLevel1Id, InstitutionNameTh = x.StructureLevel1Name })
                        .GroupBy(n => new { n.InstitutionCode, n.InstitutionNameTh })
                        .Select(g => g.FirstOrDefault()).ToList();

                    var masterFacultyService = new MasterInstitutionService();

                    var markDeleteMasterFacultyResult = masterFacultyService.MarkAsDeleteAll();
                    var resultMasterFaculty = masterFacultyService.AddBulk(masterFacultyList);

                    if (resultMasterFaculty)
                    {
                        if (markDeleteMasterFacultyResult > 0) masterFacultyService.DeleteAllMarkAsDeleteItem();
                        log.Debug("Successfully Save MasterFaculty to Database");
                    }
                    else
                    {
                        masterFacultyService.UnMarkAsDeleteAll();
                        log.Debug("RollBack Sync Master Faculty");
                    }

                    //Create New MasterDepartment
                    var masterDepartmentList = new List<MasterDepartment>();

                    masterDepartmentList = newMasterUser.Where(x => !String.IsNullOrEmpty(x.StructureLevel2Name) && !String.IsNullOrEmpty(x.StructureLevel2Name.Trim()))
                        .Select(x => new MasterDepartment { InstitutionId = Convert.ToInt32(x.StructureLevel1Id), DepartmentCode = x.StructureLevel2Id, DepartmentNameTh = x.StructureLevel2Name })
                        .GroupBy(n => new { n.InstitutionId, n.DepartmentCode, n.DepartmentNameTh })
                        .Select(g => g.FirstOrDefault()).ToList();

                    var masterDepartmentService = new MasterDepartmentService();

                    var markDeleteMasterDepartmentResult = masterDepartmentService.MarkAsDeleteAll();
                    var resultMasterDepartment = masterDepartmentService.AddBulk(masterDepartmentList);

                    if (resultMasterDepartment)
                    {
                        if (markDeleteMasterDepartmentResult > 0) masterDepartmentService.DeleteAllMarkAsDeleteItem();
                        log.Debug("Successfully Save MasterDepartment to Database");
                    }
                    else
                    {
                        masterDepartmentService.UnMarkAsDeleteAll();
                        log.Debug("RollBack Sync Master Department");
                    }

                    // Create New MasterDivision
                    var masterDivisionList  = new List<MasterDivision>();

                    masterDivisionList = newMasterUser.Where(x => !String.IsNullOrEmpty(x.StructureLevel3Name) && !String.IsNullOrEmpty(x.StructureLevel3Name.Trim()))
                        .Select(x => new MasterDivision { InstitutionCode = Convert.ToInt32(x.StructureLevel1Id), DepartmentCode = Convert.ToInt32(x.StructureLevel2Id), Divisionid = x.StructureLevel3Id, Name = x.StructureLevel2Name })
                        .GroupBy(n => new { n.InstitutionCode, n.DepartmentCode, n.Divisionid, n.Name })
                        .Select(g => g.FirstOrDefault()).ToList();
                    
                    var masterDivisionService = new MasterDivisionService();

                    var markDeleteMasterDivisionResult = masterDivisionService.MarkAsDeleteAll();
                    var resultMasterDivision = masterDivisionService.AddBulk(masterDivisionList);

                    if (resultMasterDivision)
                    {
                        
                        if (markDeleteMasterDivisionResult > 0) masterDivisionService.DeleteAllMarkAsDeleteItem();
                        log.Debug("Successfully Save MasterDivision to Database");
                    }
                    else
                    {
                        
                        masterDivisionService.UnMarkAsDeleteAll();
                        log.Debug("RollBack Sync Master Division");
                    }
                }
                else
                {
                    masterUserService.UnMarkAsDeleteAll();
                    log.Debug("RollBack Sync Master User");
                }
            }


            startJob.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = startJob.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            var returnString = string.Format("Total records is {0:N0} (success: {1:N0}, failed: {2:N0}) Job Duration : {3}", countAll, countSuccess, countFailed, elapsedTime);
            log.Debug("Successful Sync Master User: {0}", returnString);
            log.Debug("Total records is {0:N0} (success: {1:N0}, failed: {2:N0})", countAll, countSuccess, countFailed);
            log.Debug("Job Duration : {0}", elapsedTime);

            return returnString;
        }

        public bool BuildModel(MasterUser masterUser, List<string> newItem, List<MasterUser> masterUserList)
        {
            try
            {

                masterUser.PersonnelId = newItem[0];
                if (!string.IsNullOrEmpty(newItem[1])) masterUser.BeginDate = DateTimeOffset.ParseExact(newItem[1], "yyyyMMdd", null).DateTime;
                if (!string.IsNullOrEmpty(newItem[2])) masterUser.EndDate = DateTimeOffset.ParseExact(newItem[2], "yyyyMMdd", null).DateTime;
                masterUser.EmployeeName = newItem[3];

                masterUser.StructureLevel1Id = newItem[12];
                masterUser.StructureLevel1Name = newItem[13];
                masterUser.StructureLevel2Id = newItem[14];
                masterUser.StructureLevel2Name = newItem[15];
                masterUser.StructureLevel3Id = newItem[16];
                masterUser.StructureLevel3Name = newItem[17];
                masterUser.StructureLevel4Id = newItem[18];
                masterUser.StructureLevel4Name = newItem[19];
                masterUser.PersonnelGroupId = newItem[4];
                masterUser.PersonnelGroupName = newItem[5];
                masterUser.PersonnelSupGroupId = newItem[6];
                masterUser.PersonnelSupGroupName = newItem[7];
                masterUser.PositionId = newItem[20];
                masterUser.PositionName = newItem[21];
                masterUser.Email = newItem[28];
                masterUser.NameTh = newItem[29];
                masterUser.SurnameTh = newItem[30];
                masterUser.NameEn = newItem[31];
                masterUser.SurnameEn = newItem[32];
                masterUser.CitizenId = newItem[33];
                masterUser.PassportNumber = newItem[34];
                masterUser.Btrtl = newItem[10];
                if (!string.IsNullOrEmpty(newItem[35])) masterUser.BirthDate = DateTimeOffset.ParseExact(newItem[35], "yyyyMMdd", null).DateTime;
                masterUser.Username = newItem[37];
                masterUser.Nation = newItem[38];
                masterUser.TitleTh = newItem[39];
                masterUser.StatusId = newItem[40];
                masterUser.StatusName = newItem[41];
                masterUser.ContractTypeId = newItem[42];
                masterUser.ContractTypeName = newItem[43];
                if (!string.IsNullOrEmpty(newItem[44])) masterUser.ContractEndDate = DateTimeOffset.ParseExact(newItem[44], "yyyyMMdd", null).DateTime;
                //masterUser.Imagepath = newItem[36];
                //masterUser.Emailreserve = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Emailreserve).FirstOrDefault();
                //masterUser.Username = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Username).FirstOrDefault();
                //masterUser.Reporttopersonnelid = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Reporttopersonnelid).FirstOrDefault();
                //masterUser.Reporttemplate1 = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Reporttemplate1).FirstOrDefault();
                //masterUser.Reporttemplate2 = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Reporttemplate2).FirstOrDefault();
                //masterUser.Reporttemplate3 = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Reporttemplate3).FirstOrDefault();
                //masterUser.Reporttemplate4 = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Reporttemplate4).FirstOrDefault();
                //masterUser.Reporttemplate5 = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Reporttemplate5).FirstOrDefault();
                //masterUser.Iscommandline = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Iscommandline).FirstOrDefault();
                //masterUser.Groupid = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Groupid).FirstOrDefault();
                //masterUser.Imagepath = masterUserList.Where(x => x.Personnelid == newItem[0]).Select(x => x.Imagepath).FirstOrDefault();

                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }

        }
    }
}
