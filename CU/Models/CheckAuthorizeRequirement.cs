﻿using Microsoft.AspNetCore.Authorization;

namespace CU.Models
{
    public class CheckAuthorizeRequirement : IAuthorizationRequirement
    {
        public string AuthorityCode { get; }

        public CheckAuthorizeRequirement(string AuthorityCode)
        {
            this.AuthorityCode = AuthorityCode;
        }
    }
}