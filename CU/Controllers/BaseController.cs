﻿using NLog;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CU.Services;
using CU.Constants;

namespace CU.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ILogger log = LogManager.GetCurrentClassLogger();

        public dynamic GetSuccess()
        {
            return new { Status = "SUCCESS" };
        }

        public dynamic GetOk()
        {
            return new { Status = "OK", Data = "", Count = 0 };
        }

        public dynamic GetOk(dynamic data)
        {
            return new { Status = "OK", Data = data, Count = data.Count };
        }

        public dynamic GetOk(string data)
        {
            return new { Status = "OK", Data = data, Count = 1 };
        }

        public dynamic GetError()
        {
            return new { Status = "ERROR", Data = "", Count = 0 };
        }

        public dynamic GetError(dynamic data)
        {
            return new { Status = "ERROR", Data = data, Count = data.Count };
        }

        public IActionResult RedirectToDefaultPage(IActionResult actionResult)
        {
            if (AppContextHelper.User != null)
            {
                if (AppContextHelper.HasGroupAuthority(Constants.GroupAuthorityCode.Admin)) return RedirectToAction("Index", "Admin");
                return RedirectToAction("HomePage", "Home");
            }
            return actionResult;
        }

        public IActionResult AnonymousOnlyRedirect()
        {
            ShowFailureModal("ท่านไม่สามารถดำเนินการดังกล่าวได้");
            //if (AppContextHelper.HasGroupAuthority(GroupAuthorityCode.Admin)) return RedirectToAction("Index", "Admin");
           // else if (AppContextHelper.HasGroupAuthority(GroupAuthorityCode.HR)) return RedirectToAction("Index", "Hr");
            return RedirectToAction("Portal", "Home");
        }

        protected void ShowSuccessModal(string msg)
        {
            TempData["Success"] = msg;
        }

        protected void ShowFailureModal(string msg)
        {
            TempData["Failure"] = msg;
        }

        protected string UrlEncrypt(string url)
        {
            return Utilities.CryptoHelpers.UrlEncrypt(url);
        }

        protected string UrlDecrypt(string encrypturl)
        {
            return Utilities.CryptoHelpers.UrlDecrypt(encrypturl);
        }
    }

    public class JavaScriptResult : ContentResult
    {
        public JavaScriptResult(string script)
        {
            this.Content = script;
            this.ContentType = "application/javascript";
        }
    }
}
