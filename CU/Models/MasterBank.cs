﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterBank
    {
        public int Id { get; set; }
        public string Bankl { get; set; }
        public string Banka { get; set; }
        public string Brnch { get; set; }
        public string BankName { get; set; }
    }
}
