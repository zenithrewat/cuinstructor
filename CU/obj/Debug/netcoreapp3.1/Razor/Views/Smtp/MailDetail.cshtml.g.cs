#pragma checksum "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a4a4534d29752f1c073f952f6e2dbad20022ada6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Smtp_MailDetail), @"mvc.1.0.view", @"/Views/Smtp/MailDetail.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\_ViewImports.cshtml"
using CU;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\_ViewImports.cshtml"
using CU.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a4a4534d29752f1c073f952f6e2dbad20022ada6", @"/Views/Smtp/MailDetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f1fe06df287ca20645d34fa089cf891bbbdc6ed2", @"/Views/_ViewImports.cshtml")]
    public class Views_Smtp_MailDetail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CU.Models.MailTemplateDisplay>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Image/icon/cross-g.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("img-icon"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
 using (Html.BeginForm("Create", "Smtp", FormMethod.Post))
{

#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <div class=""modal-dialog modal-xl"">
        <div class=""modal-content"">
            <div class=""modal-header"" style=""padding:12px 15px;"">
                <h4 class=""modal-title"">ตั้งค่า Mail Template</h4>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "a4a4534d29752f1c073f952f6e2dbad20022ada64403", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n                    ");
#nullable restore
#line 15 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
               Write(Html.Hidden("TemplateId", CU.Utilities.CryptoHelpers.UrlEncrypt(Model.Id.ToString())));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                    <div class=""row"">
                        <div class=""col-md-6 col-sm-12 col-xs-12 clear-none"">
                            <div class=""form-group"">
                                <label>Key <span class=""text-red-b"">*</span></label>
                                ");
#nullable restore
#line 20 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
                           Write(Html.TextBoxFor(m => m.MailKey, new { @class = "form-control", required = "required", autocomplete = "off", @id = "" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </div>
                        </div>
                        <div class=""col-md-6 col-sm-12 col-xs-12 clear-none"">
                            <div class=""form-group"">
                                <label>Mail Subject <span class=""text-red-b"">*</span></label>
                                ");
#nullable restore
#line 26 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
                           Write(Html.TextBoxFor(m => m.MailSubject, new { @class = "form-control", required = "required", autocomplete = "off", @id = "" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </div>
                        </div>

                    </div>

                    <div class=""row"">
                        <div class=""col-md-6 col-sm-12 col-xs-12 clear-none"">
                            <div class=""form-group"">
                                <label>Mail From <span class=""text-red-b"">*</span></label>
                                ");
#nullable restore
#line 36 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
                           Write(Html.TextBoxFor(m => m.MailFrom, new { @class = "form-control", required = "required", autocomplete = "off", @id = "" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </div>
                        </div>
                        <div class=""col-md-6 col-sm-12 col-xs-12 clear-none"">
                            <div class=""form-group"">
                                <label>Mail To <span class=""text-red-b"">*</span></label>
                                ");
#nullable restore
#line 42 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
                           Write(Html.TextBoxFor(m => m.MailTo, new { @class = "form-control required-auth", autocomplete = "off", @id = "" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </div>
                        </div>
                    </div>
                    <div class=""row"">
                        <div class=""col-md-6 col-sm-12 col-xs-12 clear-none"">
                            <div class=""form-group"">
                                <label>Mail CC <span class=""text-red-b"">*</span></label>
                                ");
#nullable restore
#line 50 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
                           Write(Html.TextBoxFor(m => m.MailCC, new { @class = "form-control", autocomplete = "off", @id = "" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6 col-sm-12 col-xs-12 clear-none\">\r\n                            <div class=\"col-sm-12 checkbox-group\">\r\n                                ");
#nullable restore
#line 55 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
                           Write(Html.CheckBoxFor(m => m.IsEnable, new { @class = "form-check-input", @id = "IsEnable" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                <label for=""IsEnable"" class=""check-custom"">Enable</label>
                            </div>
                        </div>
                    </div>
                    <div class=""row"">
                        <div class=""col-md-6 col-sm-12 col-xs-12 clear-none"">
                            <div class=""form-group"">
                                <label>Mail Body <span class=""text-red-b"">*</span></label>
                                ");
#nullable restore
#line 64 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
                           Write(Html.TextAreaFor(m => m.MailBody, new { @class = "form-control", @rows = "3", @cols = "20", @style = "min-height:70px;", id = "MailBody" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class=""modal-footer"">
                <div class=""col-md-12 col-sm-12 col-xs-12 p-0"">
                    <div class=""form-group"" style=""margin-bottom:5px;"">
                        <div class=""row"">
                            <div class=""col-md-12 col-sm-12 col-xs-12 p-0 text-center"">
                                <button type=""submit"" class=""btn btn-main"" style=""margin-top:5px;"">
                                    บันทึก
                                </button>
                                <button type=""button"" class=""btn btn-gray"" style=""margin-top:5px;"" data-dismiss=""modal"" aria-label=""Close"">
                                    ยกเลิก
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>");
            WriteLiteral("\r\n    </div>\r\n");
#nullable restore
#line 89 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Smtp\MailDetail.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CU.Models.MailTemplateDisplay> Html { get; private set; }
    }
}
#pragma warning restore 1591
