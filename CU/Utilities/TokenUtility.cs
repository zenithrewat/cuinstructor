﻿using System;

namespace CU.Utilities
{
    public static class TokenUtility
    {
        private static readonly Random _random = new Random();
        private const string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        public static string GenToken(int length, string chars = characters)
        {
            int index = 0;
            string token = "";
            for (int i = 0; i < length; i++)
            {
                index = _random.Next(0, chars.Length);
                token += chars[index].ToString();
            }
            return token;
        }
    }
}