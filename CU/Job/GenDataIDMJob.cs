﻿using CUEnterprisePortal.Models;
using CUEnterprisePortal.Services;
using NLog;
using Quartz;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using CUEnterprisePortal.Utilities;
using System.Text;

namespace CUEnterprisePortal.Job
{

    public class GenDataIDMJob
    {
        protected ILogger log = NLog.LogManager.GetCurrentClassLogger();

        public void Execute()
        {
            log.Info("***** Ready to export csv for IDM *****");
            log.Info(GenDataCSV());
            log.Info("***** Finish to export csv. *****");
        }

        public string GenDataCSV()
        {

            int countAll = 0;
            int countSuccess = 0;
            int countFailed = 0;

            var startJob = new Stopwatch();
            startJob.Start();
            SftpClient sftpClient = null;
            try
            {
                List<RegisterCSVDisplay> data = new List<RegisterCSVDisplay>();
                //List<HeaderCSV> data = new List<HeaderCSV>();
                using (CUEnterprisePortalContext _dbContext = new CUEnterprisePortalContext())
                {

                    var input = _dbContext.DbRegistercsvs.Where(x => x.Inprocess == false).AsQueryable();
                    if(input != null)
                    {
                        List<DbRegistercsv> dbRegistercsv = new List<DbRegistercsv>();
                        dbRegistercsv = input.ToList();

                        foreach(DbRegistercsv obj in dbRegistercsv)
                        {
                            
                            string Division = _dbContext.DbMasterdivisions.Where(r => r.Divisionid == obj.Division).Select(r => r.Name).FirstOrDefault();
                            string Unit = _dbContext.DbMasterfaculties.Where(r => r.Facultyid == obj.Unit).Select(r => r.Name).FirstOrDefault();
                            obj.Unit = String.IsNullOrEmpty(Unit) ? "": Unit;
                            obj.Division = String.IsNullOrEmpty(Division) ? "" : Division;
                            data.Add(Mapper.ToRegisterDisplayCSV(obj));
                        }

                        //mark inprocess is true
                        foreach (DbRegistercsv obj in dbRegistercsv)
                        {
                            obj.Inprocess = true;
                            _dbContext.SaveChanges();
                        }

                        
                        //write csv file
                        FTPService ftpServ = new FTPService();
                        FTPDisplay ftpmodel = ftpServ.GetDefault((int)Constants.AppConstant.FtpServerType.IDM);

                        string filepath = ftpmodel.DrivePath;
                        string filename = "BLOCKCHAIN_IDM_REGISTER_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
                        string uploadFile = ftpmodel.PathUpload  + filename;

                        var conf = new CsvHelper.Configuration.CsvConfiguration(CultureInfo.InvariantCulture);
                        conf.Delimiter = ";";                        
                        using (var writer = new StreamWriter(filepath + filename, false, Encoding.UTF8))
                        using (var csv = new CsvWriter(writer, conf)) //(writer, CultureInfo.InvariantCulture))
                        {

                            csv.Context.RegisterClassMap<Mapper.HeaderCSVMap>();
                            csv.WriteHeader<HeaderCSV>();
                            csv.NextRecord();
                            csv.WriteRecords(data);
                        }


                        //remove data inprocess = true
                        List<DbRegistercsv> dbRemovecsv = null;
                        var csvRemove = _dbContext.DbRegistercsvs.Where(x => x.Inprocess == true).AsQueryable();
                        if (csvRemove != null)
                        {
                            dbRemovecsv = csvRemove.ToList();
                            _dbContext.RemoveRange(dbRemovecsv);
                            _dbContext.SaveChanges();
                        }

                        //transfer file to server by ftp
                        sftpClient = new FTPService().ftpConnect((int)Constants.AppConstant.FtpServerType.IDM);                        
                        if (sftpClient.IsConnected)
                        {
                            Stream fileStream = new FileStream(filepath + filename, FileMode.Open);
                            sftpClient.UploadFile(fileStream, uploadFile);
                            sftpClient.Disconnect();
                            sftpClient.Dispose();

                            //send mail to administrator
                            SmtpService smtp = new SmtpService();
                            MailTemplateDisplay template = smtp.GetTemplateByKey("MailIDMCSV");

                            smtp.SendMailLoadCSV(template);
                            fileStream.Close();
                        }

                        //remove file
                        if (File.Exists(filepath + filename))
                        {
                            try
                            {
                                File.Delete(filepath + filename);
                            }
                            catch (Exception e)
                            {
                                log.Error("The deletion failed: { 0}", e.Message);
                                return "Error delete flile!!!";
                            }
                        }

                    }
                    

                }
            }catch(Exception err)
            {
                sftpClient.Disconnect();
                sftpClient.Dispose();
                log.Error(err.InnerException, "GenDataCSV Exception!!!!");
                log.Error(err.Message, "GenDataCSV Exception!!!!");
                return ("Ftp Connection Failed");
            }

            startJob.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = startJob.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            var returnString = string.Format("Total records is {0:N0} (success: {1:N0}, failed: {2:N0}) Job Duration : {3}", countAll, countSuccess, countFailed, elapsedTime);
            log.Info("Successful Gen CSV file for IDM : {0}", returnString);
            log.Info("Total records is {0:N0} (success: {1:N0}, failed: {2:N0})", countAll, countSuccess, countFailed);
            log.Info("Job Duration : {0}", elapsedTime);

            return returnString;

            //return "Success generate data csv";
        }
    }
}
