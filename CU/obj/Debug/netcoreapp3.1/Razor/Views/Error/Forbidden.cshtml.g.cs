#pragma checksum "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Error\Forbidden.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f8151616ae04e728964c68ac63a47d6867534870"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Error_Forbidden), @"mvc.1.0.view", @"/Views/Error/Forbidden.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\_ViewImports.cshtml"
using CU;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\_ViewImports.cshtml"
using CU.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f8151616ae04e728964c68ac63a47d6867534870", @"/Views/Error/Forbidden.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f1fe06df287ca20645d34fa089cf891bbbdc6ed2", @"/Views/_ViewImports.cshtml")]
    public class Views_Error_Forbidden : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Error\Forbidden.cshtml"
   ViewData["Title"] = "403 การเข้าถึงถูกปฏิเสธ/ไม่ได้รับอนุญาต"; 

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""col-md-12 col-sm-12 col-xs-12 error_detail"">
    <div class=""row"">
        <div class=""col-md-6 col-sm-12 col-xs-12 form-frame"" style=""margin:0px auto;"">
            <div style=""text-align: center !important"">
                <div style=""font-size: 6rem;line-height: normal;font-weight: bold;color: #DB5F8E;"">403</div>
                <div style=""font-size: 1.8rem;line-height: normal;font-weight: bold;color: #DB5F8E;"">ไม่ได้รับอนุญาตในการเข้าถึงข้อมูลดังกล่าว</div>
                <a");
            BeginWriteAttribute("href", " href=\"", 575, "\"", 613, 1);
#nullable restore
#line 9 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Error\Forbidden.cshtml"
WriteAttributeValue("", 582, Url.Action("Login", "Account"), 582, 31, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"btn btn-theme\" style=\"margin-top:2.2rem;\">กลับสู่หน้าหลัก</a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
