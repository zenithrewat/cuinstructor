﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbStudyLevel
    {
        public int Id { get; set; }
        public string Slart { get; set; }
        public string Stext { get; set; }
        public bool? IsEnable { get; set; }
    }
}
