﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using CU.Models;
using CU.Services;
using NLog;

namespace CU.Job
{
    [DisallowConcurrentExecution]
    public class MasterJob : IJob
    {
        private ILogger log = NLog.LogManager.GetCurrentClassLogger();
        public Task Execute(IJobExecutionContext context)
        {
            var task = Task.Run(() =>
            {
                ExceuteJob();
            });
            return task;
        }

        public void ExceuteJob()
        {
            try
            {
                CronJobService cronJobService = new CronJobService();
                var currentUTC = DateTime.UtcNow;
                var joblist = cronJobService.GetJobsForExecuting(currentUTC);

                //check time to run each jobs
                foreach (var job in joblist)
                {
                    switch (job.JobName)
                    {
                        
                        //case "GenDataIDMJob":
                        //    Task.Run(() =>
                        //    {
                        //        GenDataIDMJob genDataIDMJob = new GenDataIDMJob();
                        //        genDataIDMJob.Execute();
                        //        cronJobService.ToggleStatus(job.Id);
                        //    });
                        //    break;
                        
                        case "SyncCUDataJob":
                            Task.Run(() =>
                            {
                                SyncCUDataJob syncCUDataJob = new SyncCUDataJob();
                                syncCUDataJob.Execute();
                                cronJobService.ToggleStatus(job.Id);
                            });
                            break;
                        case "CleanDataJob":
                            Task.Run(() =>
                            {
                                //CleanDataJob cleanDataJob = new CleanDataJob();
                                //cleanDataJob.Execute();
                                //cronJobService.ToggleStatus(job.Id);
                            });
                            break;
                    }
                }
                cronJobService.CalNextRunAll();
            }
            catch(Exception err)
            {
                log.Error(err);
            }
        }
    }
}
