﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CU.Utilities
{
    public class ChartUtility
    {
        private readonly Random _random = new Random();
        private readonly string[] fixColors = { "rgb(255,130,156)", "rgb(183,159,227)", "rgb(54,162,235)", "rgb(75,192,192)", "rgb(255,205,86)", "rgb(255,146,49)" };
        private readonly string[] fixTransColors = { "rgb(255,130,156,0.5)", "rgb(183,159,227,0.5)", "rgb(54,162,235,0.5)", "rgb(75,192,192,0.5)", "rgb(255,205,86,0.5)", "rgb(255,146,49,0.5)" };

        private readonly int chartIndicatior = int.MaxValue;
        private int cnt = 0;
        private List<int[]> dummyList = new List<int[]>();
        
        public string RandomTransparentColor()
        {
            return $"rgba({_random.Next(255)},{_random.Next(255)},{_random.Next(255)},0.5)";
        }

        public string[] RandomTransparentColor(int size)
        {
            string[] colors = new string[size];
            for(int i = 0; i < size; i++)
            {
                if (i < fixTransColors.Length) colors[i] = fixTransColors[i];
                else colors[i] = RandomTransparentColor();
            }
            return colors;
        }

        public string[] GetWhiteBorder(int size)
        {
            string[] output = new string[size];
            for (int i = 0; i < size; i++)
            {
                output[i] = "White";
            }
            return output;
        }

        public int[] GetBorderWidth(int size, int width)
        {
            int[] output = new int[size];
            for (int i = 0; i < size; i++)
            {
                output[i] = width;
            }
            return output;
        }

        public void GenDummyData()
        {
            int[] dummy = new int[2] { chartIndicatior, cnt - 1 };
            cnt++;
            dummyList.Add(dummy);
        }

        public int[] GetDummyData(int i)
        {
            return dummyList[i];
        }

        public string GetDummyString(int i)
        {
            if(dummyList.Count == 1)
            {
                cnt++;
                return "data:[" + string.Join(",", dummyList[i]) + $",{cnt - 1}]";
            }
            return "data:[" + string.Join(",", dummyList[i]) + "]";
        }

        public string GetDataString(double[] inputs)
        {
            return "data:[" + string.Join(",", inputs) + "]";
        }
    }
}