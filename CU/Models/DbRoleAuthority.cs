﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbRoleAuthority
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public bool IsDeleted { get; set; }
        public int Creator { get; set; }
        public string CreatorName { get; set; }
        public int RoleId { get; set; }
        public string AuthorityCode { get; set; }

        public virtual DbSecurityRole Role { get; set; }
    }
}
