﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CU.Constants;
using CU.Models;
using CU.Utilities;

namespace CU.Services
{
    public class MasterInstitutionService : BaseService
    {
        public MasterInstitutionService()
        {

        }

        public MasterInstitution GetById(int id)
        {
            MasterInstitution output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterInstitutions.Where(x => x.Id == id).FirstOrDefault();

                if (output == null)
                {
                    throw new Exception($"Master Faculty (id:{id}) not found");
                }
            }

            return output;
        }

        public List<MasterInstitution> GetAll()
        {
            List<MasterInstitution> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterInstitutions.ToList();
            }

            return output;
        }

        public List<KeyValuePair<string, string>> GetAllExistWitKeyword(string keyword)
        {
            var output = new List<KeyValuePair<string, string>>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var query = _dbcontext.MasterInstitutions.Where(x => x.IsDeleted == false);
                if (!string.IsNullOrEmpty(keyword)) query = query.Where(x => x.InstitutionNameTh.Contains(keyword));
                var targetlist = query.OrderBy(x => x.InstitutionNameTh).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(new KeyValuePair<string, string>(target.InstitutionCode, target.InstitutionNameTh));
                    }
                }
            }

            return output;
        }

        public List<KeyValuePair<string, string>> GetAllExistWitKeywordAndLimit(string keyword, int limit)
        {
            var output = new List<KeyValuePair<string, string>>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var query = _dbcontext.MasterInstitutions.Where(x => x.IsDeleted == false);
                if (!string.IsNullOrEmpty(keyword)) query = query.Where(x => x.InstitutionNameTh.Contains(keyword));
                var targetlist = query.OrderBy(x => x.InstitutionNameTh).Take(limit).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(new KeyValuePair<string, string>(target.InstitutionCode, target.InstitutionNameTh));
                    }
                }
            }

            return output;
        }

        public bool AddBulk(IEnumerable<MasterInstitution> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    model.Status = (byte)EnumHelpers.Status.Normal;
                    model.Isfixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.EditorName))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.EditorName = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterInstitutions.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master Faculty successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master Faculty");
            return false;
        }

        public bool Delete(int id)
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterInstitutions.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();

                if (target != null && target.Isfixed == false)
                {
                    target.Modified = DateTime.UtcNow;
                    target.Editor = ConfigHelpers.GetDefaultCreator();
                    target.EditorName = ConfigHelpers.GetDefaultCreatorName();

                    target.IsDeleted = true;

                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Delete Master Institution {target.InstitutionNameTh} (id:{target.Id})");
                        return true;
                    }
                }
            }
            return false;
        }

        public int MarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterInstitutions.ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.Isfixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.EditorName = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master Faculty as delete");
                    }
                }
                return count;
            }
        }

        public bool UnMarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterInstitutions.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.Isfixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.EditorName = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master Faculty");
                    return true;
                }
            }
            return false;
        }

        public bool DeleteAllMarkAsDeleteItem()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterInstitutions.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterInstitutions.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master Faculty which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master Faculty which marked as delete");
            return false;
        }

    }
}