﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterDistrict
    {
        public int Id { get; set; }
        public string Land1 { get; set; }
        public string Bland { get; set; }
        public string District { get; set; }
        public string DistrictText { get; set; }
    }
}
