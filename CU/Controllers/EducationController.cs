﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CU.Services;
using CU.Models;

namespace CU.Controllers
{
    public class EducationController : BaseController
    {
        public IActionResult Education()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetLevelEducate()
        {
            EducationService serv = new EducationService();
            List<EducationLevel> eduLevl = serv.GetLevelEducate();
            return Json(GetOk(eduLevl));
        }

        [HttpGet]
        public JsonResult GetDegreeInfo()
        {
            EducationService serv = new EducationService();
            List<EducationDegree> eduLevl = serv.GetDegreeInfo();
            return Json(GetOk(eduLevl));
        }

        [HttpGet]
        public JsonResult GetMajorInfo()
        {
            EducationService serv = new EducationService();
            List<EducationMajor> eduLevl = serv.GetMajorInfo();
            return Json(GetOk(eduLevl));
        }

        [HttpGet]
        public JsonResult GetInstitution(string code)
        {
            EducationService serv = new EducationService();
            List<Institution> institued = serv.GetInstitution(code);
            return Json(GetOk(institued));
        }
    }
}
