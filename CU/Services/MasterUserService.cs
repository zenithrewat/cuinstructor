﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CU.Constants;
using CU.Models;
using CU.Utilities;

namespace CU.Services
{
    public class MasterUserService : BaseService
    {
        public MasterUserService()
        {

        }

        public MasterUser GetById(int id)
        {
            MasterUser output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterUsers.Where(x => x.Id == id).FirstOrDefault();

                if (output == null)
                {
                    throw new Exception($"Master User (id:{id}) not found");
                }
            }

            return output;
        }

        public List<MasterUser> GetAll()
        {
            List<MasterUser> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterUsers.ToList();
            }

            return output;
        }

        public bool AddBulk(IEnumerable<MasterUser> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    //model.Status = (byte)EnumHelpers.Status.Normal;
                    model.IsFixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.Editorname))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.Editorname = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterUsers.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master User successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master User");
            return false;
        }

        //public bool Delete(int id)
        //{
        //    using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
        //    {
        //        var target = _dbcontext.MasterUsers.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();

        //        if (target != null && target.IsFixed == false)
        //        {
        //            target.Modified = DateTime.UtcNow;
        //            target.Editor = ConfigHelpers.GetDefaultCreator();
        //            target.EditorName = ConfigHelpers.GetDefaultCreatorName();

        //            target.IsDeleted = true;

        //            if (_dbcontext.SaveChanges() > 0)
        //            {
        //                log.Info($"Delete Master User {target.Username} (id:{target.Id})");
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        public int MarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterUsers.ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.IsFixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master User as delete");
                    }
                }
                return count;
            }
        }

        public bool UnMarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterUsers.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.IsFixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master User");
                    return true;
                }
            }
            return false;
        }

        public bool DeleteAllMarkAsDeleteItem()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterUsers.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterUsers.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master User which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master User which marked as delete");
            return false;
        }

        public MasterUser getByPersonalId(string PerId)
        {
            MasterUser output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterUsers.Where(x => x.PersonnelId == PerId).FirstOrDefault();

                if (output == null)
                {
                    //throw new Exception($"Master User (id:{PerId}) not found");
                    return null;
                }
            }

            return output;
        }

        public MasterUser getByUserName(string UserName)
        {
            MasterUser output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterUsers.Where(x => x.Username.ToUpper() == UserName.ToUpper()).FirstOrDefault();

                if (output == null)
                {
                    //throw new Exception($"Master User (id:{PerId}) not found");
                    return null;
                }
            }

            return output;
        }
    }
}