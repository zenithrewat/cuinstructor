﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class UploadDocument
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Deleted { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public byte Status { get; set; }
        public Guid Creator { get; set; }
        public string CreatorName { get; set; }
        public Guid Editor { get; set; }
        public string EditorName { get; set; }
        public string Description { get; set; }
        public byte[] Version { get; set; }
        public int ProjectId { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string UploadPath { get; set; }
        public string CategoryCode { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int RunningNumber { get; set; }
        public string OriginalFileName { get; set; }
    }
}
