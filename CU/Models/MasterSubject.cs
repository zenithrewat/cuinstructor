﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterSubject
    {
        public int Id { get; set; }
        public int? Courseid { get; set; }
        public int? Programid { get; set; }
        public string Courseno { get; set; }
        public string CoursenameTh { get; set; }
        public string CoursenameEn { get; set; }
        public int? Creator { get; set; }
        public string Creatorname { get; set; }
        public int? Editor { get; set; }
        public string Editorname { get; set; }
        public bool? IsFixed { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime Modified { get; set; }
        public string StudyProgramSystem { get; set; }
    }
}
