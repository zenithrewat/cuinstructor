﻿using CUEnterprisePortal.Constants;
using CUEnterprisePortal.Models;
using CUEnterprisePortal.Utilities;
using FFMpegCore;
using FFMpegCore.Arguments;
using LazZiya.ImageResize;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace CUEnterprisePortal.Services
{
    public class FileService : BaseService
    {
        public int UploadFile(Microsoft.AspNetCore.Http.IFormFile file)
        {
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                //Checking file is available to save.  
                if (file != null)
                {
                    var inputFileName = Path.GetFileName(file.FileName);

                    DbFile uploadFile = new DbFile
                    {
                        Filename = inputFileName,
                        Path = DateTime.UtcNow.Year.ToString(),
                        Uploader = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id,
                        Uploadername = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname,
                        Uploaded = DateTime.UtcNow
                    };

                    _dbcontext.DbFiles.Add(uploadFile);
                    _dbcontext.SaveChanges();

                    var serverSavePath = Path.Combine(PathHelpers.ContentRootPath, AppConstant.BasedFilePath) + uploadFile.Path;

                    if (!Directory.Exists(serverSavePath)) Directory.CreateDirectory(serverSavePath);
                    //Save file to server folder  
                    using (Stream fileStream = new FileStream(serverSavePath + "\\" + uploadFile.Id, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                    //assigning file uploaded status to ViewBag for showing message to user.  

                    return uploadFile.Id;
                }
                else
                {
                    return 0;
                }

            }
        }

        public string UploadFile(Microsoft.AspNetCore.Http.IFormFileCollection files)
        {
            string fileIDList = "";
            for (int i = 0; i < files.Count; i++)
            {
                var file = files[i];
                using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
                {
                    //Checking file is available to save.  
                    if (file != null)
                    {
                        var inputFileName = Path.GetFileName(file.FileName);

                        DbFile uploadFile = new DbFile
                        {
                            Filename = inputFileName,
                            Path = DateTime.UtcNow.Year.ToString(),
                            Uploader = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id,
                            Uploadername = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname,
                            Uploaded = DateTime.UtcNow
                        };

                        _dbcontext.DbFiles.Add(uploadFile);
                        _dbcontext.SaveChanges();

                        var serverSavePath = Path.Combine(Utilities.PathHelpers.ContentRootPath, AppConstant.BasedFilePath) + uploadFile.Path;

                        if (!Directory.Exists(serverSavePath)) Directory.CreateDirectory(serverSavePath);
                        //Save file to server folder  
                        using (Stream fileStream = new FileStream(serverSavePath + "\\" + uploadFile.Id, FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                        //assigning file uploaded status to ViewBag for showing message to user.  

                        if (fileIDList == "") fileIDList = uploadFile.Id.ToString();
                        else fileIDList = fileIDList + "," + uploadFile.Id;
                    }
                    else
                    {
                        throw new Exception("Upload file is null or empty.");
                    }
                }
            }
            return fileIDList;
        }

        public string GetFile(string ID, out string contentType, out string fileName)
        {
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var id = Convert.ToInt32(ID);
                var dbresult = _dbcontext.DbFiles.Where(f => f.Id == id).FirstOrDefault();

                if (dbresult != null)
                {
                    contentType = HeyRed.Mime.MimeTypesMap.GetMimeType(dbresult.Filename);
                    fileName = dbresult.Filename;
                    return Path.Combine(PathHelpers.ContentRootPath, AppConstant.BasedFilePath) + dbresult.Path + "\\" + dbresult.Id;
                }
                else
                {
                    throw new Exception("Target file is missing.");
                }
            }
        }

        public bool IsImage(string contentType)
        {
            if (contentType.ToLower().StartsWith("image/"))
            {
                return true;
            }
            return false;
        }

        public string WebmToMp4Based64(Microsoft.AspNetCore.Http.IFormFile file)
        {
            string output = "";

            byte[] webmbody;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms); // read sent .h264 data
                webmbody = ms.ToArray();
            }

            var outputStream = new MemoryStream();
            {
                // FFMpegCore
                FFMpegCore.FFMpegArguments
                                .FromPipeInput(new FFMpegCore.Pipes.StreamPipeSource(new MemoryStream(webmbody)))
                                .OutputToPipe(new FFMpegCore.Pipes.StreamPipeSink(outputStream), options => options
                             .WithVideoCodec("h264") // added this argument
                             .ForceFormat("mp4")
                             .WithArgument(new FragArgument())) // or VideoType.MpegTs
                              .ProcessSynchronously();

                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
                {
                    var serverSavePath = Path.Combine(PathHelpers.ContentRootPath, AppConstant.BasedFilePath) + AppConstant.TempPath;
                    if (!Directory.Exists(serverSavePath)) Directory.CreateDirectory(serverSavePath);
                    string filename;
                    do
                    {
                        filename = TokenUtility.GenToken(6);
                    } while (File.Exists(serverSavePath + filename + ".mp4"));
                    // view converted to file
                    File.WriteAllBytes(serverSavePath + filename + ".mp4", outputStream.ToArray());
                }

                output = Convert.ToBase64String(outputStream.ToArray());
            }
            return output;
        }

        public string WebmToJpegBased64(Microsoft.AspNetCore.Http.IFormFile file)
        {
            string output = "";

            var serverSavePath = Path.Combine(PathHelpers.ContentRootPath, AppConstant.BasedFilePath) + AppConstant.TempPath;
            if (!Directory.Exists(serverSavePath)) Directory.CreateDirectory(serverSavePath);
            string filename;
            string fullpath;

            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                
                do
                {
                    filename = TokenUtility.GenToken(6);
                    fullpath = serverSavePath + filename + ".webm";
                } while (File.Exists(fullpath));
                // view converted to file
                File.WriteAllBytes(fullpath, ms.ToArray());
            }

            var outputStream = new MemoryStream();
            {
                var mediaFileAnalysis = FFProbe.Analyse(fullpath);
                var bitmap = FFMpeg.Snapshot(mediaFileAnalysis, new Size(1080, 720), TimeSpan.FromSeconds(2));

                EncoderParameters encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
                {
                    // view converted to file
                    bitmap.Save(serverSavePath + filename + ".jpg", GetEncoder(ImageFormat.Jpeg), encoderParameters);
                }
                bitmap.Save(outputStream, GetEncoder(ImageFormat.Jpeg), encoderParameters);
                output = Convert.ToBase64String(outputStream.ToArray());
            }
            File.Delete(fullpath);
            return output;
        }

        public string FileToJpegBased64(Microsoft.AspNetCore.Http.IFormFile file)
        {
            string output = "";

            var ms = new MemoryStream();
            file.CopyTo(ms);
            ms = ResizeImageOnStream(ms);
            output = StreamToJpegBased64(ms);
            ms.Close();

            return output;
        }

        public string FileToJpegBased64(int ID)
        {
            string output = "";
            using (CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext())
            {
                var dbresult = _dbcontext.DbFiles.Where(f => f.Id == ID).FirstOrDefault();

                if (dbresult != null)
                {
                    using var image = Image.FromFile(Path.Combine(PathHelpers.ContentRootPath, AppConstant.BasedFilePath) + dbresult.Path + "\\" + dbresult.Id);
                    using var ms = new MemoryStream();
                    image.ScaleAndCrop(1080, 720).Save(ms, ImageFormat.Jpeg);
                    output = StreamToJpegBased64(ms);
                }
                else
                {
                    throw new Exception("Target file is missing.");
                }
            }
            return output;
        }

        public MemoryStream ResizeImageOnStream(MemoryStream stream)
        {
            using (var image = Image.FromStream(stream))
            {
                using MemoryStream output = new MemoryStream();
                image.ScaleAndCrop(1080, 720).Save(output, ImageFormat.Jpeg);
                stream = output;
            }

            return stream;
        }

        public string StreamToJpegBased64(MemoryStream stream)
        {
            var fileBytes = stream.ToArray();
            return Convert.ToBase64String(fileBytes);
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }
    }

    public class FragArgument : FFMpegCore.Arguments.IArgument
    {
        string IArgument.Text => "-movflags frag_keyframe+empty_moov";
    }
}
