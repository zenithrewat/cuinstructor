﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class SecurityRole
    {
        public SecurityRole()
        {
            SecurityUserRoles = new HashSet<SecurityUserRole>();
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime? Deleted { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsFixed { get; set; }
        public byte Status { get; set; }
        public int Creator { get; set; }
        public string CreatorName { get; set; }
        public int Editor { get; set; }
        public string EditorName { get; set; }
        public string Description { get; set; }
        public string Rolename { get; set; }

        public virtual ICollection<SecurityUserRole> SecurityUserRoles { get; set; }
    }
}
