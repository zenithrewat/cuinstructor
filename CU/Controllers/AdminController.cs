﻿using CU.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CU.Job;

namespace CU.Controllers
{
    public class AdminController : BaseController
    {
        [Authorize(Policy = PolicyName.AdminPage)]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SyncUser()
        {
            SyncCUDataJob syncCUDataJob = new SyncCUDataJob();
            syncCUDataJob.Execute();

            //SyncDataRegister syncCUDataJob = new SyncDataRegister();
            //syncCUDataJob.Execute();
            return View("Index");
        }
    }
}
