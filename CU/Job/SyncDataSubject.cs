﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using CU.Models;
using CU.Services;
using NLog;
using Renci.SshNet;

namespace CU.Job
{
    public class SyncDataSubject
    {
        protected ILogger log = NLog.LogManager.GetCurrentClassLogger();

        public void Execute()
        {
            log.Info("Sync CU Data Job is executing.");
            log.Info(SyncMasterSubject());
            log.Info("Sync CU Data Job is finishing.");
        }

        public string SyncMasterSubject()
        {
            log.Debug("Start Sync Master User from {0}", Environment.GetEnvironmentVariable("ftpMasterUserFileName"));
            DataTable dt = new DataTable();
            DataTable dtForSubject = new DataTable();
            DataTable dtForImage = new DataTable();
            DataTable dtForCUNET = new DataTable();
            DataTable dtForOper = new DataTable();
            DataTable dtForContrct = new DataTable();
            string fullText;

            var startJob = new Stopwatch();
            startJob.Start();

            try
            {
                SftpClient sFtpclient = new FTPService().ftpConnect((int)Constants.AppConstant.FtpServerType.Gateway);

                if (sFtpclient.IsConnected)
                {
                    //DG0403
                    MemoryStream ms = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterCourseTermFileName"), ms);
                    using (ms)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dt.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dt.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dt.Rows.Add(dr); //add other rows  
                            }

                        }
                    }
                    ms.Close();

                    //DG0206
                    MemoryStream msSubject = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterSubjectTermFileName"), msSubject);
                    using (msSubject)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(msSubject.ToArray());
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtForSubject.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow drSubject = dtForSubject.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    drSubject[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dtForSubject.Rows.Add(drSubject); //add other rows  
                            }

                        }
                    }
                    msSubject.Close();

                    sFtpclient.Disconnect();
                    log.Debug("Successfully Disconnected to SFTP :", sFtpclient.ConnectionInfo.Host);
                }
                else
                {
                    return ("Ftp Connection Failed");
                }

                //copy datatable
                foreach (DataRow dr in dt.Rows)
                {

                }
            }
            catch (Exception e)
            {
                log.Error(e, "FTP and Read File  Sync Master User Exception");
                return ("Ftp Connection Failed");
            }


            var masterSubjectService = new MasterSubjectService();

            var masterSubjectList = masterSubjectService.GetAll();

            int countAll = 0;
            int countSuccess = 0;
            int countFailed = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                countAll = dt.Rows.Count;
                var list = new List<MasterSubject>();

                foreach (DataRow item in dt.Rows)
                {
                    try
                    {
                        var newItem = item.ItemArray.Select(o => o == null ? String.Empty : o.ToString()).ToList();
                        var masterSubject = new MasterSubject();
                        if (BuildModel(masterSubject, newItem, masterSubjectList))
                        {
                            //check duplicated employee name
                            var duplicated = list.Where(x => x.Courseno == masterSubject.Courseno).FirstOrDefault();

                            if (duplicated == null)
                            {
                                list.Add(masterSubject);
                                countSuccess++;
                            }
                            else
                            {
                                //if new record's PersonnelGroupId = F then remove duplicated and add new
                                //if (masterUser.PersonnelGroupId.Equals("F", StringComparison.InvariantCultureIgnoreCase))
                                //{
                                //    list.Remove(duplicated);
                                //    list.Add(masterUser);
                                //    countSuccess++;
                                //}
                                //else
                                //{
                                //    //this record will not insert because PersonnelGroupId != F and duplicated
                                //    countFailed++;
                                //}
                            }
                        }
                        else
                        {
                            countFailed++;
                        }
                        //countSuccess++;
                    }
                    catch (Exception)
                    {
                        //countFailed++;
                    }
                }

                var markDeleteResult = masterSubjectService.MarkAsDeleteAll();

                masterSubjectService = new MasterSubjectService();
                var result = masterSubjectService.AddBulk(list);

                if (result)
                {
                    if (markDeleteResult > 0) masterSubjectService.DeleteAllMarkAsDeleteItem();
                    log.Debug("Successfully Save MasterSubject to Database");
                    
                }
                else
                {
                    masterSubjectService.UnMarkAsDeleteAll();
                    log.Debug("RollBack Sync Master Subject");
                }

            }

            startJob.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = startJob.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            var returnString = string.Format("Total records is {0:N0} (success: {1:N0}, failed: {2:N0}) Job Duration : {3}", countAll, countSuccess, countFailed, elapsedTime);
            log.Debug("Successful Sync Master Job Assignment Detial Sub Type 1-1: {0}", returnString);
            log.Debug("Job Duration : {0}", elapsedTime);

            return returnString;

        }

        public bool BuildModel(MasterSubject masterSubject, List<string> newItem, List<MasterSubject> masterSubjList)
        {
            try
            {
                masterSubject.Courseid = 0;
                masterSubject.Courseno = "";
                masterSubject.CoursenameTh = "";
                masterSubject.CoursenameEn = "";
                masterSubject.Programid = 0;
                //masterUser.PersonnelId = newItem[0];
                //if (!string.IsNullOrEmpty(newItem[1])) masterUser.BeginDate = DateTimeOffset.ParseExact(newItem[1], "yyyyMMdd", null).DateTime;
                //if (!string.IsNullOrEmpty(newItem[2])) masterUser.EndDate = DateTimeOffset.ParseExact(newItem[2], "yyyyMMdd", null).DateTime;
                //masterUser.EmployeeName = newItem[3];

                //masterUser.StructureLevel1Id = newItem[12];
                //masterUser.StructureLevel1Name = newItem[13];
                //masterUser.StructureLevel2Id = newItem[14];
                //masterUser.StructureLevel2Name = newItem[15];
                //masterUser.StructureLevel3Id = newItem[16];
                //masterUser.StructureLevel3Name = newItem[17];
                //masterUser.StructureLevel4Id = newItem[18];
                //masterUser.StructureLevel4Name = newItem[19];
                //masterUser.PersonnelGroupId = newItem[4];
                //masterUser.PersonnelGroupName = newItem[5];
                //masterUser.PersonnelSupGroupId = newItem[6];
                //masterUser.PersonnelSupGroupName = newItem[7];
                //masterUser.PositionId = newItem[20];
                //masterUser.PositionName = newItem[21];
                //masterUser.Email = newItem[28];
                //masterUser.NameTh = newItem[29];
                //masterUser.SurnameTh = newItem[30];
                //masterUser.NameEn = newItem[31];
                //masterUser.SurnameEn = newItem[32];
                //masterUser.CitizenId = newItem[33];
                //masterUser.PassportNumber = newItem[34];
                //masterUser.Btrtl = newItem[10];
                //if (!string.IsNullOrEmpty(newItem[35])) masterUser.BirthDate = DateTimeOffset.ParseExact(newItem[35], "yyyyMMdd", null).DateTime;
                //masterUser.Username = newItem[37];
                //masterUser.Nation = newItem[38];
                //masterUser.TitleTh = newItem[39];
                //masterUser.StatusId = newItem[40];
                //masterUser.StatusName = newItem[41];
                //masterUser.ContractTypeId = newItem[42];
                //masterUser.ContractTypeName = newItem[43];
                //masterUser.ContractEndDate = DateTimeOffset.ParseExact(newItem[44], "yyyyMMdd", null).DateTime;

                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }

        }


    }
}
