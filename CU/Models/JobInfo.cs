﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CU.Models
{
    public class JobInfo
    {
        public string JobName { get; set; }
        public string TriggerName { get; set; }
        public string GroupName { get; set; }
        public DataParameter[] DataParameters { get; set; }
        public string CronExpression { get; set; }
    }

    
}
