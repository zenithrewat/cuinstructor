﻿namespace CU.Models
{
    public class DataParameter
    {
        public string Key { get; set; }
        public dynamic Value { get; set; }
    }
}
