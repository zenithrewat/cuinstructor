﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbAddress
    {
        public int Id { get; set; }
        public string BuildingNo { get; set; }
        public string RoomNo { get; set; }
        public string Floor { get; set; }
        public string Village { get; set; }
        public string HouseNo { get; set; }
        public string Moo { get; set; }
        public string Alley { get; set; }
        public string Road { get; set; }
        public string SubDistrict { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public int InstructorId { get; set; }
    }
}
