﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbActivityLogging
    {
        public long Id { get; set; }
        public string Category { get; set; }
        public string Action { get; set; }
        public string Originator { get; set; }
        public string Level { get; set; }
        public string Thread { get; set; }
        public string Source { get; set; }
        public string Url { get; set; }
        public string Hostname { get; set; }
        public string IpAddress { get; set; }
        public string UserAgent { get; set; }
        public string Username { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public byte[] Version { get; set; }
        public DateTime Created { get; set; }
    }
}
