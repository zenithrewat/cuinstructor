﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbResearch
    {
        public int Id { get; set; }
        public string TypeOfWork { get; set; }
        public string ResearchType { get; set; }
        public int YearPublish { get; set; }
        public int MonthPublish { get; set; }
        public string AuthorName { get; set; }
        public string Subject { get; set; }
        public string Journal { get; set; }
        public string JournalYear { get; set; }
        public string JournalNo { get; set; }
        public string JournalPage { get; set; }
        public string JournalDb { get; set; }
    }
}
