﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbGroupDoc
    {
        public int Id { get; set; }
        public string Ntype { get; set; }
        public string Text { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
