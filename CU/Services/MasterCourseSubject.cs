﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CU.Models;
using CU.Utilities;

namespace CU.Services
{
    public class MasterCourseSubject : BaseService
    {
        public MasterCourseSubject()
        {

        }


        public List<MasterCourse> GetCourseAll()
        {
            List<MasterCourse> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterCourses.ToList();
            }

            return output;
        }

        public bool AddCourseBulk(IEnumerable<MasterCourse> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    //model.Status = (byte)EnumHelpers.Status.Normal;
                    model.IsFixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.Editorname))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.Editorname = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterCourses.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master User successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master User");
            return false;
        }

        //public bool Delete(int id)
        //{
        //    using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
        //    {
        //        var target = _dbcontext.MasterUsers.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();

        //        if (target != null && target.IsFixed == false)
        //        {
        //            target.Modified = DateTime.UtcNow;
        //            target.Editor = ConfigHelpers.GetDefaultCreator();
        //            target.EditorName = ConfigHelpers.GetDefaultCreatorName();

        //            target.IsDeleted = true;

        //            if (_dbcontext.SaveChanges() > 0)
        //            {
        //                log.Info($"Delete Master User {target.Username} (id:{target.Id})");
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        public int MarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterCourses.ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.IsFixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master User as delete");
                    }
                }
                return count;
            }
        }

        public bool UnMarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterCourses.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.IsFixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master User");
                    return true;
                }
            }
            return false;
        }

        public bool DeleteAllMarkAsDeleteItem()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterCourses.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterCourses.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master Course which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master Course which marked as delete");
            return false;
        }
    }
}
