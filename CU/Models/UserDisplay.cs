﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CU.Models
{
    public class UserDisplay
    {
        public UserDisplay()
        {
            Id = 0;
        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "ไม่สามารถดำเนินการได้เนื่องจากอีเมลไม่ถูกต้อง")]
        public string Email { get; set; }
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "ไม่สามารถดำเนินการได้เนื่องจากอีเมลสำรองไม่ถูกต้อง")]
        public string EmailSecondary { get; set; }
        public List<Role> Roles { get; set; }

        public string roleToString
        {
            get
            {
                return Roles != null ? string.Join(",", Roles.Select(r => r.Name)) : string.Empty;
            }
            set
            {
                Roles = new List<Role>();
                if (value != null)
                {
                    foreach (var rolename in value.Split(','))
                    {
                        Roles.Add(new Role { Name = rolename });
                    }
                } 
            }
        }
    }
}