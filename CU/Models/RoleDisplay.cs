﻿using System;
using CU.Constants;

namespace CU.Models
{
    public class RoleDisplay
    {
        public RoleDisplay()
        {
            Id = "0";
            isFixed = false;
            Ldap = AuthorityOption.Deny;
            Smtp = AuthorityOption.Deny;
            Log = AuthorityOption.Deny;
            User = AuthorityOption.Deny;
            Role = AuthorityOption.Deny;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public int UserCount { get; set; }
        public int AuthCount { get; set; }
        public DateTime Created { get; set; }
        public string CreatorName { get; set; }
        public bool isFixed { get; set; }
        public string HrDigitalID
        {
            get
            {
                return getRadioValue(_HrDigitalID);
            }
            set
            {
                //_HrDigitalID = setValueFromRadio(value, PageAuthorityCode.HrDigitalID);
            }
        }
        
        public string Ldap
        {
            get
            {
                return getRadioValue(_Ldap);
            }
            set
            {
                _Ldap = setValueFromRadio(value, PageAuthorityCode.Ldap);
            }
        }
        public string Smtp
        {
            get
            {
                return getRadioValue(_Smtp);
            }
            set
            {
                //_Smtp = setValueFromRadio(value, PageAuthorityCode.Smtp);
            }
        }
        public string Log
        {
            get
            {
                return getRadioValue(_Log);
            }
            set
            {
                _Log = setValueFromRadio(value, PageAuthorityCode.Log);
            }
        }
        public string User
        {
            get
            {
                return getRadioValue(_User);
            }
            set
            {
                _User = setValueFromRadio(value, PageAuthorityCode.User);
            }
        }
        public string Role
        {
            get
            {
                return getRadioValue(_Role);
            }
            set
            {
                _Role = setValueFromRadio(value, PageAuthorityCode.Role);
            }
        }
        
        public string Job
        {
            get
            {
                return getRadioValue(_Job);
            }
            set
            {
                //_Job = setValueFromRadio(value, PageAuthorityCode.Job);
            }
        }
        public string ImportCsv
        {
            get
            {
                return getRadioValue(_ImportCsv);
            }
            set
            {
                //_ImportCsv = setValueFromRadio(value, PageAuthorityCode.ImportCsv);
            }
        }

        public string _HrDigitalID { get; set; }
        public string _HrLesspaper { get; set; }
        public string _Ldap { get; set; }
        public string _Smtp { get; set; }
        public string _Log { get; set; }
        public string _User { get; set; }
        public string _Role { get; set; }
        public string _SmsGateway { get; set; }
        public string _Job { get; set; }
        public string _ImportCsv { get; set; }

        public string getRadioValue(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                if (input.EndsWith(AuthorityStatusCode.FullControl)) return AuthorityOption.Allow;
                return AuthorityOption.ReadOnly;
            }
            else return AuthorityOption.Deny;
        }
        public string setValueFromRadio(string input, string pagecode)
        {
            if (input == AuthorityOption.Allow)
                return pagecode + AuthorityStatusCode.FullControl;
            else if (input == AuthorityOption.ReadOnly)
                return pagecode + AuthorityStatusCode.ReadOnly;
            else
                return null;
        }
    }
}