﻿using CU.Constants;
using CU.Models;
using CU.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using CU.Utilities;


namespace CU.Services
{
    public class LdapService : BaseService
    {
        public LdapService()
        {

        }

        public IEnumerable<LdapDisplay> GetAll()
        {
            var jsonList = new List<LdapDisplay>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var list = _dbcontext.MasterLdaps.Where(x => x.IsDeleted == false).ToList();

                if (list != null)
                {
                    list.ForEach(e => jsonList.Add(Mapper.ToLdapDisplay(e)));
                }
            }

            return jsonList;
        }

        public LdapDisplay GetById(int id)
        {
            var json = new LdapDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterLdaps.Where(x => x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToLdapDisplay(target);
                }
            }

            return json;
        }

        public LdapDisplay GetDefault()
        {
            var json = new LdapDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterLdaps.Where(x => x.IsDeleted == false && x.IsDefault == true).OrderByDescending(x => x.Created).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToLdapDisplay(target);
                }
            }
            return json;
        }

        public bool Save(LdapDisplay json)
        {
            var model = new MasterLdap();
            json.username = json.username ?? "";
            json.password = json.password ?? "";
            model = Mapper.ToMasterLdap(json);

            //Insert new
            if (model.Id <= 0)
            {
                return Insert(model, json);
            }

            //Update
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterLdaps.Where(x => x.Id == model.Id).FirstOrDefault();

                if (target == null)
                {
                    throw new ArgumentOutOfRangeException("ldap", "Item not found.");
                }

                target.Modified = DateTime.UtcNow;
                target.Editor = AppContextHelper.User.Id;
                target.EditorName = AppContextHelper.User.Fullname;
                target.Hostname = model.Hostname;
                target.Port = model.Port;
                target.BasePath = model.BasePath;
                target.ProtocolVersion = model.ProtocolVersion;
                target.DirectoryPath = model.DirectoryPath;
                target.EnableSsl = model.EnableSsl;
                target.IsAuthenticated = model.IsAuthenticated;
                if (model.IsAuthenticated)
                {
                    target.Username = model.Username;
                    target.Password = CryptoHelpers.Encrypt(json.password);
                }

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Update Ldap setting (id:{model.Id})");
                    return true;
                }
                else return false;
            }
        }

        public bool Insert(MasterLdap model, LdapDisplay json)
        {
            model.Status = (byte)AppConstant.Status.Normal;
            model.IsEnabled = true;
            model.IsDeleted = false;
            model.IsDefault = true;
            model.Created = DateTime.UtcNow;
            model.Modified = DateTime.UtcNow;
            model.Deleted = new DateTime();

            if (string.IsNullOrEmpty(model.CreatorName))
            {
                model.Creator = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
                model.CreatorName = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            }

            if (string.IsNullOrEmpty(model.EditorName))
            {
                model.Editor = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
                model.EditorName = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            }

            model.Password = CryptoHelpers.Encrypt(json.password);

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterLdaps.Add(model);
                _dbcontext.SaveChanges();
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Create new Ldap setting (id:{model.Id})");
                    return true;
                }
                else return false;
            }
        }

        public bool Delete(int id)
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterLdaps.Where(x => x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    target.IsDeleted = true;
                    target.Deleted = DateTime.UtcNow;
                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Delete Ldap setting (id:{target.Id})");
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
