﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CU.Models
{
    public class CUUserModel
    {
        public string Firstnameth { get; set; }
        public string Lastnameth { get; set; }
        public string Firstnameen { get; set; }
        public string Lastnameen { get; set; }
        public string Division { get; set; }
        public string Unit { get; set; }
        public string citizenid { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Passport { get; set; }
        public DateTime Birthdate { get; set; }
        public string Image { get; set; }
    }
}
