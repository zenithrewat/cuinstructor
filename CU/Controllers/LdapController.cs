﻿using Microsoft.AspNetCore.Mvc;
using CU.Services;
using CU.Models;
using Microsoft.AspNetCore.Authorization;
using CU.Constants;

namespace CU.Controllers
{
    [Authorize(Policy = PolicyName.LdapPage)]
    public class LdapController : BaseController
    {
        [Route("Admin/[controller]/{action}")]
        public IActionResult Index()
        {
            //Authorize(AuthorityCode.LdapFullControl);
            LdapDisplay model = new LdapDisplay();
            LdapService ldapSetting = new LdapService();
            model = ldapSetting.GetDefault();
            ViewBag.showPopUp = "false";
            return View(model);
        }

        [HttpPost]
        public IActionResult Update(LdapDisplay input)
        {
            //Authorize(AuthorityCode.LdapFullControl);
            LdapDisplay model = new LdapDisplay();
            LdapService ldapSetting = new LdapService();
            if (ModelState.IsValid)
            {
                if (ldapSetting.Save(input)) return RedirectToAction("Index");
            }
            model = ldapSetting.GetDefault();
            ViewBag.showPopUp = "true";
            return View(model);
        }
    }
}
