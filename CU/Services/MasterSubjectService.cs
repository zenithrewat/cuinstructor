﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CU.Models;
using CU.Utilities;

namespace CU.Services
{
    public class MasterSubjectService : BaseService
    {
        public MasterSubjectService()
        { }

        public List<MasterSubject> GetAll()
        {
            List<MasterSubject> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterSubjects.ToList();
            }

            return output;
        }

        public List<MasterDg0206> GetAllDG0206()
        {
            List<MasterDg0206> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterDg0206s.ToList();
            }

            return output;
        }

        public List<MasterDg0207> GetAllDG0207()
        {
            List<MasterDg0207> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterDg0207s.ToList();
            }

            return output;
        }
        public int MarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterSubjects.ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.IsFixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master User as delete");
                    }
                }
                return count;
            }
        }

        public int MarkAsDeleteAllDG206()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDg0206s.ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.IsFixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master User as delete");
                    }
                }
                return count;
            }
        }

        public int MarkAsDeleteAllDG207()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDg0207s.ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.IsFixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master User as delete");
                    }
                }
                return count;
            }
        }

        public bool AddBulk(IEnumerable<MasterSubject> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    //model.Status = (byte)EnumHelpers.Status.Normal;
                    model.IsFixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.Editorname))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.Editorname = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterSubjects.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master User successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master User");
            return false;
        }

        public bool AddBulkDGResister(IEnumerable<MasterDg0206> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    //model.Status = (byte)EnumHelpers.Status.Normal;
                    model.IsFixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.Editorname))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.Editorname = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterDg0206s.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master User successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master User");
            return false;
        }

        public bool AddBulkDG0207(IEnumerable<MasterDg0207> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    //model.Status = (byte)EnumHelpers.Status.Normal;
                    model.IsFixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.Editorname))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.Editorname = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterDg0207s.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master User successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master User");
            return false;
        }

        
            public bool DeleteAllMarkAsDeleteItemDG0207()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDg0207s.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterDg0207s.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master Subject which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master Subject which marked as delete");
            return false;
        }

        public bool DeleteAllMarkAsDeleteItemDG206()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDg0206s.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterDg0206s.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master Subject which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master Subject which marked as delete");
            return false;
        }

        public bool DeleteAllMarkAsDeleteItem()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterSubjects.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterSubjects.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master Subject which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master Subject which marked as delete");
            return false;
        }

        public bool UnMarkAsDeleteAllDg206()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDg0206s.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.IsFixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master Faculty");
                    return true;
                }
            }
            return false;
        }

        public bool UnMarkAsDeleteAllDg0207()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDg0207s.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.IsFixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master Faculty");
                    return true;
                }
            }
            return false;
        }

        public bool UnMarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterSubjects.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.IsFixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.Editorname = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master Faculty");
                    return true;
                }
            }
            return false;
        }
    }
}
