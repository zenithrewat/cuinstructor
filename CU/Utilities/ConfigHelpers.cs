﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CU.Utilities
{
    public class ConfigHelpers
    {
        public static int GetDefaultCreator()
        {
            return 0;
        }

        public static Guid GetDefaultCreatorUpload()
        {
            return new Guid("00000000000000000000000000000000");
        }
        public static string GetDefaultCreatorName()
        {
            return "System";
        }
    }
}