﻿namespace CU.Constants
{
    public static class GroupAuthorityCode
    {
        public const int Length = 4;

        public const string HR = "HR"; // => if have code Start with HR => allow to access HR
        public const string HrDigitalID = GroupAuthorityCode.HR + "ID";
        public const string HrLesspaper = GroupAuthorityCode.HR + "LP";

        public const string Report = "REPO"; // => if have code Start with REPO => allow to access Report page

        public const string Dashboard = "DASH"; // => if have code Start with DASH => allow to access Dashboard page

        public const string Admin = "ADMI"; // => if have code Start with Admin => allow to access Admin main page
    }
    public static class PageAuthorityCode
    {
        public const string HrDigitalID = GroupAuthorityCode.HrDigitalID + "000";

        public const string HrLesspaper = GroupAuthorityCode.HrLesspaper + "000";

        public const string Ldap = GroupAuthorityCode.Admin + "000";
        public const string Smtp = GroupAuthorityCode.Admin + "001";

        public const string User = GroupAuthorityCode.Admin + "003";
        public const string Role = GroupAuthorityCode.Admin + "004";
        public const string Log = GroupAuthorityCode.Admin + "005";
        public const string FTP = GroupAuthorityCode.Admin + "006";
        public const string SmsGateway = GroupAuthorityCode.Admin + "007";
        public const string Job = GroupAuthorityCode.Admin + "008";
        public const string ImportCsv = GroupAuthorityCode.Admin + "009";
    }
    public static class ExtraAuthorityCode
    {
        private const string Extra = "EXTR";

        public const string ExportReportAsPdf = Extra + "000";
        public const string ExportReportAsDoc = Extra + "001";

        public const string RevertToDraft = Extra + "002";
    }
    public static class AuthorityStatusCode
    {
        public const string ReadOnly = "0";
        public const string FullControl = "1";
    }

    public static class AuthorityCode
    {
        public const int Length = 8;
        public const string HrDigitalIDReadOnly = PageAuthorityCode.HrDigitalID + AuthorityStatusCode.ReadOnly;
        public const string HrDigitalIDFullControl = PageAuthorityCode.HrDigitalID + AuthorityStatusCode.FullControl;
        public const string HrLesspaperReadOnly = PageAuthorityCode.HrLesspaper + AuthorityStatusCode.ReadOnly;
        public const string HrLesspaperFullControl = PageAuthorityCode.HrLesspaper + AuthorityStatusCode.FullControl;

        public const string LdapFullControl = PageAuthorityCode.Ldap + AuthorityStatusCode.FullControl;
        public const string SmtpFullControl = PageAuthorityCode.Smtp + AuthorityStatusCode.FullControl;
        public const string UserFullControl = PageAuthorityCode.User + AuthorityStatusCode.FullControl;
        public const string RoleFullControl = PageAuthorityCode.Role + AuthorityStatusCode.FullControl;
        public const string LogFullControl = PageAuthorityCode.Log + AuthorityStatusCode.FullControl;
        public const string FTPFullControl = PageAuthorityCode.FTP + AuthorityStatusCode.FullControl;
        public const string SmsGatewayFullControl = PageAuthorityCode.SmsGateway + AuthorityStatusCode.FullControl;
        public const string JobFullControl = PageAuthorityCode.Job + AuthorityStatusCode.FullControl;
        public const string ImportCsvFullControl = PageAuthorityCode.ImportCsv + AuthorityStatusCode.FullControl;
    }

    public static class AuthorityOption
    {
        public const string Deny = "deny";
        public const string ReadOnly = "read";
        public const string Allow = "all";
    }

    public static class PolicyName
    {
        public const string GeneralUser = "GeneralUser";

        public const string HRPage = "HRPage";
        public const string DigitalIDViewRequest = "DigitalIDViewRequest";
        public const string DigitalIDApproveRequest = "DigitalIDApproveRequest";
        public const string LesspaperViewRequest = "LesspaperViewRequest";
        public const string LesspaperApproveRequest = "LesspaperApproveRequest";

        public const string AdminPage = "AdminPage";
        public const string LdapPage = "LdapPage";
        public const string SmtpPage = "SmtpPage";
        public const string UserPage = "UserPage";
        public const string RolePage = "RolePage";
        public const string LogPage = "LogPage";
        public const string FTPPage = "FTPPage";
        public const string SmsGatewayPage = "SmsGatewayPage";
        public const string JobPage = "JobPage";
        public const string ImportCsvPage = "ImportCsvPage";
    }
}