﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace CU.Utilities
{
    public class CryptoHelpers
    {
        private const string encrypt_key = "zenithcomappteam";
        private const string encrypt_iv = "teamappzenithcom";
        private const string url_key = "qkg1X";
        private const string url_iv = "PX23a";

        public static string Encrypt(string input, string key = encrypt_key, string iv = encrypt_iv)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            AesCryptoServiceProvider aesCrypto = new AesCryptoServiceProvider
            {
                Key = new SHA256Managed().ComputeHash(Encoding.UTF8.GetBytes(key)),
                IV = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(iv)),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cTransform = aesCrypto.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            aesCrypto.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string input, string key = encrypt_key, string iv = encrypt_iv)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            AesCryptoServiceProvider aesCrypto = new AesCryptoServiceProvider
            {
                Key = new SHA256Managed().ComputeHash(Encoding.UTF8.GetBytes(key)),
                IV = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(iv)),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cTransform = aesCrypto.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            aesCrypto.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static string GetHash(string input)
        {
            return GetSha256Hash(input);
        }
        public static string GetSha256Hash(string input)
        {
            byte[] data = new SHA256Managed().ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        public static bool VerifySha256Hash(string input, string hash)
        {
            string hashOfInput = GetSha256Hash(input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string UrlEncrypt(string plainText)
        {
            plainText = Encrypt(plainText, key: url_key, iv: url_iv);
            return Base64UrlEncoder.Encode(StringUtility.Reverse(plainText));
        }

        public static string UrlDecrypt(string text)
        {
            return Decrypt(StringUtility.Reverse(Base64UrlEncoder.Decode(text)), key: url_key, iv: url_iv);
        }

        /*===========================IDM DataEncryptor==============================*/
        static readonly string IDMPasswordHash = "CUFrontP@@Sw0rd";
        static readonly string IDMSaltKey = "CUFrontS@LT";
        static readonly string IDMVIKey = "@1B2c3D4e5F6g7H8";

        public static string IDMEncrypt(string plainText)
        {
            if (string.IsNullOrEmpty(plainText))
                return null;

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(IDMPasswordHash, Encoding.ASCII.GetBytes(IDMSaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(IDMVIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static string IDMDecrypt(string encryptedText)
        {
            if (string.IsNullOrEmpty(encryptedText))
                return null;
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(IDMPasswordHash, Encoding.ASCII.GetBytes(IDMSaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(IDMVIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
    }
}