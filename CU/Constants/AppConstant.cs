﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CU.Constants
{
    public static class AppConstant
    {
        public enum Status : byte
        {
            Normal = 0,
        }
        public const string Local = "TH";
        public const string SystemAdmin = "ผู้ดูแลระบบ";
        public const string HrStaff = "เจ้าหน้าที่HR";
        public const string General = "ผู้ใช้งานทั่วไป";

        public const string PasswordNotMacth = "คุณใส่รหัสผ่านไม่ตรงกัน";
        public const string PassNotMacth = "คุณใส่รหัสผ่านไม่ตรงกัน";
        public const int YearCount = 100;
        public static readonly string[] JobLevel = { "ปริญญาตรี", "ปริญญาโท", "ปริญญาเอก", "ป.บัณฑิต","ป.บัณฑิตชั้นสูง" };
        public static readonly string[] MonthSelect = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
        public enum FtpServerType : int
        {
            Gateway = 0,
            IDM = 1
        }

        public enum DigitalIDRegisterStatus : int
        {
            WaitForHr = 0,
            Reject = 1,
            WaitForFaceScan = 2,
            WaitForIDM = 3,
            WaitForOtp = 4,
            WaitForSetPassword = 5,
            Complete = 6
        }

        public static readonly string[] DigitalIDRegisterStatusName = { "รอตรวจสอบ", "ไม่อนุมัติ", "รอยืนยันใบหน้า", "รอสร้างบัญชี", "รอยืนยัน OTP", "รอตั้งรหัสผ่าน", "เสร็จสมบูรณ์" };

        public enum DigitalIDRegisterType : int
        {
            RegisterPage = 0,
            Automatic = 1,
            FirstTimeLogin = 2,
        }

        public const int uploadImgLimit = 3; //MB

        public const int pageSize = 15;
        public const int logPageSize = 50;

        public const string BasedFilePath = "Files\\";
        public const string TempPath = "Temp\\";

        public const int RefNoLength = 6;
        public const int OtpLength = 6;
        public const int OtpTimeSpan = 3; //minute
        public const int OtpLock = 5; //times then lock for 1 hour

        public const int LockDuration = 60; //minute

        public const int PasswordMinLength = 8;

        public const string photopath = "PHOTO/";

        public enum UserType : int
        {
            Outsource = 0,
            Internal = 1
        }

        public enum DigitalIDChangeMobileStatus : int
        {
            WaitForFaceScan = 0,
            WaitForOtp = 1,
            WaitForSetMobile = 2,
            Complete = 3
        }

        public static readonly string[] DigitalIDChangeMobileStatusName = { "รอยืนยันใบหน้า", "รอยืนยัน OTP", "รอกำหนดเบอร์ใหม่", "เสร็จสมบูรณ์" };

        public enum DigitalIDResetPasswordStatus : int
        {
            WaitForFaceScan = 0,
            WaitForOtp = 1,
            WaitForSetPassword = 2,
            Complete = 3
        }

        public static readonly string[] DigitalIDResetPasswordStatusName = { "รอยืนยันใบหน้า", "รอยืนยัน OTP", "รอตั้งรหัสผ่าน", "เสร็จสมบูรณ์" };


        public enum RequestPinStatus : int
        {
            Cancel = 0,
            FaceScan = 1,
            CheckData = 2,
            WaitForOtp = 3,
            SendMail = 4,
        }

        public static readonly string[] RequestPinStatusName = { "ยกเลิก", "รอยืนยันใบหน้า", "รอยืนยันข้อมูล", "รอยืนยัน OTP", "เสร็จสมบูรณ์" };
    }
}