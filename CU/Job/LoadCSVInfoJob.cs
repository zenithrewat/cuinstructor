﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using CUEnterprisePortal.Models;
using CUEnterprisePortal.Services;
using NLog;
using Renci.SshNet;
using System.IO;
using CsvHelper;
using CUEnterprisePortal.Utilities;

namespace CUEnterprisePortal.Job
{
    public class LoadCSVInfoJob
    {
        private ILogger log = NLog.LogManager.GetCurrentClassLogger();
        private static CUEnterprisePortalContext _dbcontext = new CUEnterprisePortalContext();
        private static DateTime loadDate = DateTime.UtcNow;
        //private static string filename = "IMPORT_IDM_CU.csv";
        private static string fileDirs = String.Empty;
        public void Execute()
        {
            log.Info("Load CSV from Drive Path");
                // log.Info(ReadCSV());
            ReadCSV();
            log.Info("Load CSV Job is finishing.");
            log.Info("Start Check Correct Data CSV");
            log.Info(ProcessCheckCSVData(loadDate));
            log.Info("Finish Check Correct Data CSV");
            log.Info("Start Delete file csv");
            RemoveCSVFile();
            log.Info("Finish Delete file csv");
        }

        public void ReadCSV()
        {
            
            try
            {
                //set up ftpclient
                log.Info("*********Connect sftp************");
                FTPService ftpServ = new FTPService();
                FTPDisplay ftpmodel = ftpServ.GetDefault((int)Constants.AppConstant.FtpServerType.IDM);
                SftpClient sftp = ftpConnect(ftpmodel);
                fileDirs = ftpmodel.DrivePath;
                //filepath = ftpmodel.DrivePath + filename;
                if (sftp.IsConnected)
                {
                    log.Info("*******Start Read CSV File************");

                    string[] fileEntries = System.IO.Directory.GetFiles(ftpmodel.DrivePath, "*.csv");
                    foreach (string fileObj in fileEntries)
                    {
                        string filepath = ftpmodel.DrivePath + fileObj;

                        using (var reader = new StreamReader(filepath))  //ftpmodel.DrivePath)) 
                        using (var csv = new CsvReader(reader, System.Globalization.CultureInfo.CurrentCulture))
                        {

                            csv.Context.RegisterClassMap<Mapper.UserCSVMap>();
                            csv.Read();
                            csv.ReadHeader();
                            while (csv.Read())
                            {
                                try
                                {

                                    var record = new UserCSV();
                                    record = csv.GetRecord<UserCSV>();

                                    var model = new DbLoadcsv();
                                    model = Mapper.ToLoadCSV(record);
                                    model.CreatedDate = loadDate;//DateTime.UtcNow;
                                    model.IsOtp = false;
                                    if (model.Id <= 0)
                                    {
                                        //insert
                                        _dbcontext.DbLoadcsvs.Add(model);

                                    }
                                    else
                                    {
                                        //update
                                        model.MobileNo = record.MobileNo;
                                        model.UserName = record.UserName.Trim();
                                    }

                                    _dbcontext.SaveChanges();
                                }
                                catch (CsvHelper.FieldValidationException ex)
                                {
                                    //    log.Error();
                                    //Console.WriteLine(ex);
                                    //save to log error table
                                    var input = new UserCSVError
                                    {
                                        MobileNo = ex.Context.Parser.Record[0],
                                        UserName = ex.Context.Parser.Record[1],
                                        IsClosed = false,
                                        SyncDate = loadDate //DateTime.UtcNow //วันที่ของไฟล์
                                    };
                                    DbLoadUserError userError = Mapper.ToLoadUserError(input);
                                    if (userError.Id <= 0)
                                    {
                                        _dbcontext.DbLoadUserErrors.Add(userError);
                                        _dbcontext.SaveChanges();
                                    }
                                }

                            }
                        }
                    }
                    
                }
                else
                {
                    log.Error("Can not connect ftp server");
                   // return null;
                }
            }
            catch(Exception err)
            {
                log.Error(err, "Error in ReadCSV method in LoadCSVInfo class");
                //return null;
            }
            
        }

        public SftpClient ftpConnect(FTPDisplay ftpmodel)
        {

            var host = ftpmodel.serverip;
            var username = ftpmodel.username;
            var password = CryptoHelpers.Decrypt(ftpmodel.password);
            var port = ftpmodel.port;

            var client = new SftpClient(host, port, username, password);
            try
            {
                client.Connect();
            }
            catch (Exception e)
            {
                log.Error("ftp conntect error : ", e.Message);
            }
            return client;
        }

        public string ProcessCheckCSVData(DateTime loadDt)
        {
            try
            {
                CSVUserService csvUserServ = new CSVUserService();
                int totalError = csvUserServ.GetErrorLoadCSV(loadDt);
                if(totalError > 0)
                {
                    //send mail to administrator
                    SmtpService smtp = new SmtpService();
                    MailTemplateDisplay template =  smtp.GetTemplateByKey("MailLoadCSV");

                    smtp.SendMailLoadCSV(template);
                }
            }
            catch(Exception err)
            {
                return "Error ProcessCheckCSVData " + err.Message;
            }

            return "Finish check load data";
        }

        private void RemoveCSVFile()
        {

            string[] filePaths = Directory.GetFiles(fileDirs);//@"c:\MyDir\");
            foreach (string filePath in filePaths)
            {
                if (File.Exists(filePath))
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception e)
                    {
                        log.Error("The deletion failed: { 0}", e.Message);
                        //return "Error delete flile!!!";
                    }
                }
                else
                {
                    log.Info("File not found ", filePath);
                }
                
            }

            //return "Success delete file";

        }
    }
}
