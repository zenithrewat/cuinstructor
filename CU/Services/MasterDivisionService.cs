﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CU.Constants;
using CU.Models;
using CU.Utilities;

namespace CU.Services
{
    public class MasterDivisionService : BaseService
    {
        public MasterDivisionService()
        {

        }

        public MasterDivision GetById(int id)
        {
            MasterDivision output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterDivisions.Where(x => x.Id == id).FirstOrDefault();

                if (output == null)
                {
                    throw new Exception($"Master Department (id:{id}) not found");
                }
            }

            return output;
        }

        public List<MasterDivision> GetAll()
        {
            List<MasterDivision> output;
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                output = _dbcontext.MasterDivisions.ToList();
            }

            return output;
        }

        public bool AddBulk(IEnumerable<MasterDivision> list)
        {
            foreach (var model in list)
            {
                if (model.Id <= 0)
                {
                    model.Status = (byte)EnumHelpers.Status.Normal;
                    model.IsFixed = false;
                    model.IsEnabled = true;
                    model.IsDeleted = false;
                    model.Created = DateTime.Now;
                    model.Modified = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Creatorname))
                    {
                        model.Creator = ConfigHelpers.GetDefaultCreator();
                        model.Creatorname = ConfigHelpers.GetDefaultCreatorName();
                    }

                    if (string.IsNullOrEmpty(model.EditorName))
                    {
                        model.Editor = ConfigHelpers.GetDefaultCreator();
                        model.EditorName = ConfigHelpers.GetDefaultCreatorName();
                    }
                }
            }

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterDivisions.AddRange(list);
                if (_dbcontext.SaveChanges() == list.Count())
                {
                    log.Info($"Bulk add Master Department successfully");
                    return true;
                }
            }

            log.Info($"Fail to bulk add Master Department");
            return false;
        }

        public bool Delete(int id)
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterDivisions.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();

                if (target != null && target.IsFixed == false)
                {
                    target.Modified = DateTime.UtcNow;
                    target.Editor = ConfigHelpers.GetDefaultCreator();
                    target.EditorName = ConfigHelpers.GetDefaultCreatorName();

                    target.IsDeleted = true;

                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Delete Master Department {target.Name} (id:{target.Id})");
                        return true;
                    }
                }
            }
            return false;
        }

        public int MarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDivisions.Where(x => x.IsDeleted == false).ToList();
                var count = 0;
                if (targetlist.Count > 0)
                {
                    foreach (var target in targetlist)
                    {
                        if (target != null && target.IsFixed == false)
                        {
                            target.Modified = DateTime.UtcNow;
                            target.Editor = ConfigHelpers.GetDefaultCreator();
                            target.EditorName = ConfigHelpers.GetDefaultCreatorName();

                            target.IsDeleted = true;
                            count++;
                        }
                    }
                    if (_dbcontext.SaveChanges() == count)
                    {
                        log.Info($"Mark all Master Department as delete");
                    }
                }
                return count;
            }
        }

        public bool UnMarkAsDeleteAll()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDivisions.Where(x => x.IsDeleted == true).ToList();

                foreach (var target in targetlist)
                {
                    if (target != null && target.IsFixed == false)
                    {
                        target.Modified = DateTime.UtcNow;
                        target.Editor = ConfigHelpers.GetDefaultCreator();
                        target.EditorName = ConfigHelpers.GetDefaultCreatorName();

                        target.IsDeleted = false;
                    }
                }
                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Unmark all deleted Master Department");
                    return true;
                }
            }
            return false;
        }

        public bool DeleteAllMarkAsDeleteItem()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.MasterDivisions.Where(x => x.IsDeleted == true).ToList();

                _dbcontext.MasterDivisions.RemoveRange(targetlist);

                if (_dbcontext.SaveChanges() > 0)
                {
                    log.Info($"Delete Master Department which marked as delete successfully");
                    return true;
                }
            }

            log.Info($"Fail to delete Master Department which marked as delete");
            return false;
        }

    }
}