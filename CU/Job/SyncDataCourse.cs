﻿using CU.Models;
using CU.Services;
using NLog;
using Quartz;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CU.Job
{

    public class SyncDataCourse
    {
        protected ILogger log = NLog.LogManager.GetCurrentClassLogger();

        public void Execute()
        {
            log.Info("Sync CU Data Course & Subject is executing.");
            log.Info(SyncCourseAndSubject());
            log.Info("Sync CU Data Course & Subject is finishing.");
        }

        public String SyncCourseAndSubject()
        {
            log.Debug("Start Sync Master Course & Subject from {0}", Environment.GetEnvironmentVariable("ftpMasterCourseFileName"));
            DataTable dt = new DataTable();
            DataTable dtForUserInfo = new DataTable();
            string fullText;

            var startJob = new Stopwatch();
            startJob.Start();

            try
            {
                SftpClient sFtpclient = new FTPService().ftpConnect((int)Constants.AppConstant.FtpServerType.Gateway);

                if (sFtpclient.IsConnected)
                {

                    MemoryStream msForCourseInfo = new MemoryStream();
                    sFtpclient.DownloadFile(Environment.GetEnvironmentVariable("ftpMasterCourseFileName"), msForCourseInfo);
                    using (msForCourseInfo)
                    {
                        fullText = System.Text.Encoding.UTF8.GetString(msForCourseInfo.ToArray());
                        //fullText = readerForEmail.ReadToEnd().ToString();
                        string[] rows = fullText.Split('\n');
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = System.Text.RegularExpressions.Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                            //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  

                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtForUserInfo.Columns.Add(rowValues[j].ToString().Replace("\"", string.Empty)); //add headers  
                                }
                            }
                            else
                            {
                                DataRow drForEmail = dtForUserInfo.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    drForEmail[k] = rowValues[k].ToString().Replace("\"", string.Empty);
                                }
                                dtForUserInfo.Rows.Add(drForEmail); //add other rows  
                            }

                        }
                    };
                    msForCourseInfo.Close();

                }
                else
                {
                    return ("Ftp Connection Failed");
                }

                //filter Active User
                string expression;
                expression = string.Format("End_Date > '{0}'", DateTime.Now.ToString("MM/dd/yyyy"));
                DataRow[] foundRows;
                foundRows = dt.Select(expression);

                dt = foundRows.CopyToDataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    var row = (from DataRow dre in dtForUserInfo.Rows
                               where (string)dre["programid"] == dr["programid"].ToString()
                               select dre).FirstOrDefault();

                    if (row != null)
                    {

                    }
                }

                log.Debug("Successfully Load CSV File to Datatable");
            }
            catch (Exception e)
            {
                log.Error(e, "FTP and Read File  Sync Master Course & Subject Exception");
                return ("Ftp Connection Failed");
            }

            var masterCourseService = new MasterCourseSubject();

            var masterUserList = masterCourseService.GetCourseAll();

            int countAll = 0;
            int countSuccess = 0;
            int countFailed = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                countAll = dt.Rows.Count;
                var list = new List<MasterCourse>();

                foreach (DataRow item in dt.Rows)
                {
                    try
                    {
                        var newItem = item.ItemArray.Select(o => o == null ? String.Empty : o.ToString()).ToList();
                        var masterCourse = new MasterCourse();
                        if (BuildModel(masterCourse, newItem, masterUserList))
                        {
                            //check duplicated employee name
                            var duplicated = list.Where(x => x.Programid == masterCourse.Programid).FirstOrDefault();

                            if (duplicated == null)
                            {
                                list.Add(masterCourse);
                                countSuccess++;
                            }
                            else
                            {
                                ////if new record's PersonnelGroupId = F then remove duplicated and add new
                                //if (masterCourse.Programid.Equals("F", StringComparison.InvariantCultureIgnoreCase))
                                //{
                                //    list.Remove(duplicated);
                                //    list.Add(masterCourse);
                                //    countSuccess++;
                                //}
                                //else
                                //{
                                //    //this record will not insert because PersonnelGroupId != F and duplicated
                                //    countFailed++;
                                //}
                            }
                        }
                        else
                        {
                            countFailed++;
                        }
                        //countSuccess++;
                    }
                    catch (Exception)
                    {
                        //countFailed++;
                    }
                }

                var markDeleteResult = masterCourseService.MarkAsDeleteAll();

                masterCourseService = new MasterCourseSubject();
                var result = masterCourseService.AddCourseBulk(list);

                if (result)
                {
                    if (markDeleteResult > 0) masterCourseService.DeleteAllMarkAsDeleteItem();
                    log.Debug("Successfully Save MasterUser to Database");

                    var newMasterUser = masterCourseService.GetCourseAll();

                    //Create New MasterFaculty or MasterInstitution
                    //var masterFacultyList = new List<MasterInstitution>();

                    //masterFacultyList = newMasterUser.Where(x => !String.IsNullOrEmpty(x.StructureLevel1Name) && !String.IsNullOrEmpty(x.StructureLevel1Name.Trim()))
                    //    .Select(x => new MasterInstitution { InstitutionCode = x.StructureLevel1Id, InstitutionNameTh = x.StructureLevel1Name })
                    //    .GroupBy(n => new { n.InstitutionCode, n.InstitutionNameTh })
                    //    .Select(g => g.FirstOrDefault()).ToList();

                    //var masterFacultyService = new MasterInstitutionService();

                    //var markDeleteMasterFacultyResult = masterFacultyService.MarkAsDeleteAll();
                    //var resultMasterFaculty = masterFacultyService.AddBulk(masterFacultyList);

                    //if (resultMasterFaculty)
                    //{
                    //    if (markDeleteMasterFacultyResult > 0) masterFacultyService.DeleteAllMarkAsDeleteItem();
                    //    log.Debug("Successfully Save MasterFaculty to Database");
                    //}
                    //else
                    //{
                    //    masterFacultyService.UnMarkAsDeleteAll();
                    //    log.Debug("RollBack Sync Master Faculty");
                    //}

                    ////Create New MasterDepartment
                    //var masterDepartmentList = new List<MasterDepartment>();

                    //masterDepartmentList = newMasterUser.Where(x => !String.IsNullOrEmpty(x.StructureLevel2Name) && !String.IsNullOrEmpty(x.StructureLevel2Name.Trim()))
                    //    .Select(x => new MasterDepartment { InstitutionId = Convert.ToInt32(x.StructureLevel1Id), DepartmentCode = x.StructureLevel2Id, DepartmentNameTh = x.StructureLevel2Name })
                    //    .GroupBy(n => new { n.InstitutionId, n.DepartmentCode, n.DepartmentNameTh })
                    //    .Select(g => g.FirstOrDefault()).ToList();

                    //var masterDepartmentService = new MasterDepartmentService();

                    //var markDeleteMasterDepartmentResult = masterDepartmentService.MarkAsDeleteAll();
                    //var resultMasterDepartment = masterDepartmentService.AddBulk(masterDepartmentList);

                    //if (resultMasterDepartment)
                    //{
                    //    if (markDeleteMasterDepartmentResult > 0) masterDepartmentService.DeleteAllMarkAsDeleteItem();
                    //    log.Debug("Successfully Save MasterDepartment to Database");
                    //}
                    //else
                    //{
                    //    masterDepartmentService.UnMarkAsDeleteAll();
                    //    log.Debug("RollBack Sync Master Department");
                    //}

                    //// Create New MasterDivision
                    //var masterDivisionList = new List<MasterDivision>();

                    //masterDivisionList = newMasterUser.Where(x => !String.IsNullOrEmpty(x.StructureLevel3Name) && !String.IsNullOrEmpty(x.StructureLevel3Name.Trim()))
                    //    .Select(x => new MasterDivision { InstitutionCode = Convert.ToInt32(x.StructureLevel1Id), DepartmentCode = Convert.ToInt32(x.StructureLevel2Id), Divisionid = x.StructureLevel3Id, Name = x.StructureLevel2Name })
                    //    .GroupBy(n => new { n.InstitutionCode, n.DepartmentCode, n.Divisionid, n.Name })
                    //    .Select(g => g.FirstOrDefault()).ToList();

                    //var masterDivisionService = new MasterDivisionService();

                    //var markDeleteMasterDivisionResult = masterDivisionService.MarkAsDeleteAll();
                    //var resultMasterDivision = masterDivisionService.AddBulk(masterDivisionList);

                    //if (resultMasterDivision)
                    //{

                    //    if (markDeleteMasterDivisionResult > 0) masterDivisionService.DeleteAllMarkAsDeleteItem();
                    //    log.Debug("Successfully Save MasterDivision to Database");
                    //}
                    //else
                    //{

                    //    masterDivisionService.UnMarkAsDeleteAll();
                    //    log.Debug("RollBack Sync Master Division");
                    //}
                }
                else
                {
                    masterCourseService.UnMarkAsDeleteAll();
                    log.Debug("RollBack Sync Master User");
                }
            }

            startJob.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = startJob.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            var returnString = string.Format("Total records is {0:N0} (success: {1:N0}, failed: {2:N0}) Job Duration : {3}", countAll, countSuccess, countFailed, elapsedTime);
            log.Debug("Successful Sync Master User: {0}", returnString);
            log.Debug("Total records is {0:N0} (success: {1:N0}, failed: {2:N0})", countAll, countSuccess, countFailed);
            log.Debug("Job Duration : {0}", elapsedTime);

            return returnString;

        }

        public bool BuildModel(MasterCourse masterCourse, List<string> newItem, List<MasterCourse> masterUserList)
        {
            try
            {

                masterCourse.Programid = Convert.ToInt32(newItem[0]);

                masterCourse.Muano = newItem[12];
                masterCourse.ProgramnameTh = newItem[13];
                masterCourse.ProgramnameEn = newItem[14];
                masterCourse.TitledegreeTh = newItem[15];
                masterCourse.TitledegreeEn = newItem[16];
                masterCourse.Facultycode = newItem[17];
                masterCourse.Departmentcode = newItem[18];
                masterCourse.Levelcode = newItem[19];
                //masterCourse.PersonnelGroupId = newItem[4];
                //masterCourse.PersonnelGroupName = newItem[5];
                //masterUser.PersonnelSupGroupId = newItem[6];
                //masterUser.PersonnelSupGroupName = newItem[7];
                //masterUser.PositionId = newItem[20];
                //masterUser.PositionName = newItem[21];
                //masterUser.Email = newItem[28];
                //masterUser.NameTh = newItem[29];
                //masterUser.SurnameTh = newItem[30];
                //masterUser.NameEn = newItem[31];
                //masterUser.SurnameEn = newItem[32];
                //masterUser.CitizenId = newItem[33];
                //masterUser.PassportNumber = newItem[34];
                //masterUser.Btrtl = newItem[10];
                //if (!string.IsNullOrEmpty(newItem[35])) masterUser.BirthDate = DateTimeOffset.ParseExact(newItem[35], "yyyyMMdd", null).DateTime;
                //masterUser.Username = newItem[37];
                //masterUser.Nation = newItem[38];
                //masterUser.TitleTh = newItem[39];
                //masterUser.StatusId = newItem[40];
                //masterUser.StatusName = newItem[41];
                //masterUser.ContractTypeId = newItem[42];
                //masterUser.ContractTypeName = newItem[43];
                //masterUser.ContractEndDate = DateTimeOffset.ParseExact(newItem[44], "yyyyMMdd", null).DateTime;



                return true;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return false;
            }

        }
    }
}
