﻿using CU.Models;
using CU.Utilities;
using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CU.Services
{
    public class LdapAuthenticationService : BaseService
    {
        private string _path;
        public string Fullname { get; set; }
        public string Email { get; set; }

        public LdapAuthenticationService(string path)
        {
            _path = path;
        }

        public bool Authenticated(string domain, AuthenUser authenUser)
        {
            var ldapService = new LdapService();
            var ldapServer = ldapService.GetDefault();

            if (ldapServer != null)
            {

                string authUsername = ldapServer.username;
                string authPassword = CryptoHelpers.Decrypt(ldapServer.password);
                string authPath = ldapServer.directory_path;

                LdapDirectoryIdentifier ldapDirectoryIdentifier = new LdapDirectoryIdentifier(ldapServer.host_name, ldapServer.port);
                NetworkCredential networkCredential = new NetworkCredential(string.Format("uid={0},{1}", authUsername, authPath), authPassword);
                LdapConnection ldapConnection = new LdapConnection(ldapDirectoryIdentifier, networkCredential)
                {
                    AuthType = AuthType.Basic
                };

                try
                {
                    log.Info("login ldap begin");
                    string filter = string.Format("(uid={0})", authenUser.username);
                    string realUserName = "";
                    var request = new SearchRequest(ldapServer.base_path, filter, System.DirectoryServices.Protocols.SearchScope.Subtree, null);

                    log.Debug($"login ldap sending request with username : {authenUser.username}");
                    var response = (SearchResponse)ldapConnection.SendRequest(request);

                    foreach (SearchResultEntry entry in response.Entries)
                    {
                        realUserName = entry.DistinguishedName;
                        DirectoryAttribute cnItem = entry.Attributes["cn"];
                        DirectoryAttribute emailItem = entry.Attributes["mail"];

                        Fullname = cnItem[0].ToString();
                        Email = emailItem[0].ToString();
                    }

                    if (string.IsNullOrEmpty(realUserName))
                    {
                        ldapConnection.Dispose();
                        return false;
                    }
                    else
                    {
                        log.Info("login with real name {0}", realUserName);
                        NetworkCredential snc = new NetworkCredential(realUserName, authenUser.password);
                        ldapConnection.Bind(snc);
                        ldapConnection.Dispose();
                        return true;
                    }
                }
                catch (LdapException e)
                {
                    log.Warn(e, "login ldap warning");
                    ldapConnection.Dispose();
                    return false;
                }
                catch (Exception e)
                {
                    log.Error(e, "login ldap exception");
                    ldapConnection.Dispose();
                    return false;
                }
            }
            log.Error("The default ldap server is not found");
            return false;
        }

        public string GetLdapPath(LdapDisplay model)
        {
            if (model != null)
            {
                var protocol = "LDAP";
                if (model.enable_ssl) protocol = "LDAPS";

                string adPath = string.Format("{0}://{1}:{2}/{3}", protocol, model.host_name, model.port, model.base_path);
                log.Info("adPath is {0}", adPath);

                return adPath;
            }

            return string.Empty;
        }
    }
}
