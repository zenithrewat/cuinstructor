﻿using CU.Constants;
using CU.Models;
using CU.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CU.Job;

namespace CU.Services
{
    public class InitService : BaseService
    {
        public void Init()
        {
            log.Debug("Root path " + PathHelpers.ContentRootPath);
            CheckInitRoles();
            if (Environment.GetEnvironmentVariable("JOBS_ENABLE") == "1")
            {
                CheckInitJobs();
                Job.QuartzServer.Start();
            }
        }

        private void CheckInitRoles()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                if (_dbcontext.DbSecurityRoles.ToList().Count == 0)
                {
                    var SystemAdmin = new DbSecurityRole
                    {
                        Created = DateTime.UtcNow,
                        Creator = ConfigHelpers.GetDefaultCreator(),
                        CreatorName = ConfigHelpers.GetDefaultCreatorName(),

                        Modified = DateTime.UtcNow,
                        Editor = ConfigHelpers.GetDefaultCreator(),
                        EditorName = ConfigHelpers.GetDefaultCreatorName(),

                        IsEnabled = true,
                        IsDeleted = false,
                        IsFixed = true,
                        Status = (byte)AppConstant.Status.Normal,

                        Rolename = AppConstant.SystemAdmin,
                    };
                    _dbcontext.DbSecurityRoles.Add(SystemAdmin);

                    var HrStaff = new DbSecurityRole
                    {
                        Created = DateTime.UtcNow,
                        Creator = ConfigHelpers.GetDefaultCreator(),
                        CreatorName = ConfigHelpers.GetDefaultCreatorName(),

                        Modified = DateTime.UtcNow,
                        Editor = ConfigHelpers.GetDefaultCreator(),
                        EditorName = ConfigHelpers.GetDefaultCreatorName(),

                        IsEnabled = true,
                        IsDeleted = false,
                        IsFixed = true,
                        Status = (byte)AppConstant.Status.Normal,

                        Rolename = AppConstant.HrStaff,
                    };
                    _dbcontext.DbSecurityRoles.Add(HrStaff);

                    var General = new DbSecurityRole
                    {
                        Created = DateTime.UtcNow,
                        Creator = ConfigHelpers.GetDefaultCreator(),
                        CreatorName = ConfigHelpers.GetDefaultCreatorName(),

                        Modified = DateTime.UtcNow,
                        Editor = ConfigHelpers.GetDefaultCreator(),
                        EditorName = ConfigHelpers.GetDefaultCreatorName(),

                        IsEnabled = true,
                        IsDeleted = false,
                        IsFixed = true,
                        Status = (byte)AppConstant.Status.Normal,

                        Rolename = AppConstant.General,
                    };
                    _dbcontext.DbSecurityRoles.Add(General);

                    _dbcontext.SaveChanges();

                    _dbcontext.DbRoleAuthorities.AddRange(new List<DbRoleAuthority> { new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.HrDigitalIDFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.HrLesspaperFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.LdapFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.SmtpFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.UserFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.RoleFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.LogFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.FTPFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.SmsGatewayFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.JobFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = SystemAdmin.Id, AuthorityCode = AuthorityCode.ImportCsvFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = HrStaff.Id, AuthorityCode = AuthorityCode.HrDigitalIDFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() },
                    new DbRoleAuthority { RoleId = HrStaff.Id, AuthorityCode = AuthorityCode.HrLesspaperFullControl, Created = DateTime.UtcNow, Creator = ConfigHelpers.GetDefaultCreator(), CreatorName = ConfigHelpers.GetDefaultCreatorName() }});

                    _dbcontext.SaveChanges();
                }
            }
        }

        private void CheckInitJobs()
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                if (_dbcontext.DbJobcronSchedules.ToList().Count == 0)
                {
                    var SyncCUDataJob = new DbJobcronSchedule
                    {
                        JobName = "SyncCUDataJob",
                        Minutes = "30",
                        Hours = "1",
                        DayofMonth = "1/1",
                        Month = "*",
                        DayofWeek = "?",
                        IsExcuting = false,
                        JobEnable = true
                    };
                    _dbcontext.DbJobcronSchedules.Add(SyncCUDataJob);

                    var LoadCSVInfoJob = new DbJobcronSchedule
                    {
                        JobName = "LoadCSVInfoJob",
                        Minutes = "0",
                        Hours = "2",
                        DayofMonth = "1/1",
                        Month = "*",
                        DayofWeek = "?",
                        IsExcuting = false,
                        JobEnable = true
                    };
                    _dbcontext.DbJobcronSchedules.Add(LoadCSVInfoJob);

                    var SMSDeliveryJob = new DbJobcronSchedule
                    {
                        JobName = "SMSDeliveryJob",
                        Minutes = "30",
                        Hours = "2",
                        DayofMonth = "1/1",
                        Month = "*",
                        DayofWeek = "?",
                        IsExcuting = false,
                        JobEnable = true
                    };
                    _dbcontext.DbJobcronSchedules.Add(SMSDeliveryJob);

                    var GenDataIDMJob = new DbJobcronSchedule
                    {
                        JobName = "GenDataIDMJob",
                        Minutes = "0",
                        Hours = "0",
                        DayofMonth = "1/1",
                        Month = "*",
                        DayofWeek = "?",
                        IsExcuting = false,
                        JobEnable = true
                    };
                    _dbcontext.DbJobcronSchedules.Add(GenDataIDMJob);

                    var CleanDataJob = new DbJobcronSchedule
                    {
                        JobName = "CleanDataJob",
                        Minutes = "0",
                        Hours = "0/2",
                        DayofMonth = "1/1",
                        Month = "*",
                        DayofWeek = "?",
                        IsExcuting = false,
                        JobEnable = true
                    };
                    _dbcontext.DbJobcronSchedules.Add(CleanDataJob);

                    _dbcontext.SaveChanges();
                }
            }
        }
    }
}
