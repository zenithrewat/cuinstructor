﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbEducation
    {
        public int Id { get; set; }
        public string EducationLevel { get; set; }
        public string Degree { get; set; }
        public string Major { get; set; }
        public string Acadamy { get; set; }
        public DateTime? Graduate { get; set; }
        public string Country { get; set; }
        public int InstructorId { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Edited { get; set; }
        public DateTime? Deleted { get; set; }
        public int? Editor { get; set; }
        public string EditorName { get; set; }
        public string CreatorName { get; set; }
        public int? Creator { get; set; }
    }
}
