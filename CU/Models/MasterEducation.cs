﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterEducation
    {
        public int Id { get; set; }
        public string Langu { get; set; }
        public string Faart { get; set; }
        public string Ftext { get; set; }
    }
}
