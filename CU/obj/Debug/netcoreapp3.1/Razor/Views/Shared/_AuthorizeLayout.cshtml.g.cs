#pragma checksum "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__AuthorizeLayout), @"mvc.1.0.view", @"/Views/Shared/_AuthorizeLayout.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\_ViewImports.cshtml"
using CU;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\_ViewImports.cshtml"
using CU.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
using CU.Constants;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
using CU.Services;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9bab9d61d28f60c58ffab23aab4e2e057d2ae70d", @"/Views/Shared/_AuthorizeLayout.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f1fe06df287ca20645d34fa089cf891bbbdc6ed2", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__AuthorizeLayout : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/bootstrap/dist/css/bootstrap.min.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/home.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/global.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery/dist/jquery.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/bootstrap/dist/js/bootstrap.bundle.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", "~/js/site.js", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("back-bg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ScriptTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d6771", async() => {
                WriteLiteral("\r\n    <meta charset=\"utf-8\" />\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\r\n    <title>");
#nullable restore
#line 8 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
      Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
                WriteLiteral(" - ระบบสารสนเทศเพื่อรองรับอาจารย์พิเศษและอาจารย์ชาวต่างชาติ</title>\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d7465", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d8643", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d9821", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
                WriteLiteral("    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d11034", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d12134", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d13234", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ScriptTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper.Src = (string)__tagHelperAttribute_6.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
#nullable restore
#line 17 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper.AppendVersion = true;

#line default
#line hidden
#nullable disable
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-append-version", __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper.AppendVersion, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9bab9d61d28f60c58ffab23aab4e2e057d2ae70d15905", async() => {
                WriteLiteral("\r\n    <header>\r\n        <div class=\"navbar\">\r\n            <div class=\"left\">\r\n                <a");
                BeginWriteAttribute("href", " href=\"", 998, "\"", 1036, 1);
#nullable restore
#line 24 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
WriteAttributeValue("", 1005, Url.Action("HomePage", "Home"), 1005, 31, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"logo\"><img src=\"../../Image/chula-logo-white.png\" border=\"0\" /></a>\r\n                <a");
                BeginWriteAttribute("href", " href=\"", 1132, "\"", 1170, 1);
#nullable restore
#line 25 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
WriteAttributeValue("", 1139, Url.Action("HomePage", "Home"), 1139, 31, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"home\">ระบบสารสนเทศเพื่อรองรับอาจารย์พิเศษและอาจารย์ชาวต่างชาติ</a>\r\n            </div>\r\n\r\n            <div class=\"right dropdown\">\r\n");
#nullable restore
#line 29 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
                 if (AppContextHelper.User != null)
                {

#line default
#line hidden
#nullable disable
                WriteLiteral(@"                    <div class=""dropbtn flex-row"">
                        <div class=""row"">
                            <div class=""w-f mt-2-5 "">
                                <div class=""logo white-text mr-2""><img src=""../../Image/icon/person.png"" border=""0"" /></div>
                            </div>
                            <div class=""w-f mt-2"">
                                <div class=""name white-text"">
                                    <div class=""text-user-name white-text"">
                                        ");
#nullable restore
#line 39 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
                                   Write(AppContextHelper.User.Fullname);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                                    </div>
                                    <div class=""role white-text"">เจ้าหน้าที่สาขา</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=""dropdown-content"">
                        <a href=""#""><img class=""img-icon"" src=""../../Image/icon/gear-p.png"" border=""0"" />ตั้งค่าระบบ</a>
                        <a");
                BeginWriteAttribute("href", " href=\"", 2428, "\"", 2467, 1);
#nullable restore
#line 48 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
WriteAttributeValue("", 2435, Url.Action("Logout", "Account"), 2435, 32, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("><img class=\"img-icon\" src=\"../../Image/icon/logout.png\" border=\"0\" />Log out</a>\r\n                    </div>\r\n");
#nullable restore
#line 50 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
                }

#line default
#line hidden
#nullable disable
                WriteLiteral("            </div>\r\n        </div>\r\n    </header>\r\n");
#nullable restore
#line 54 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
     if (AppContextHelper.HasGroupAuthority(GroupAuthorityCode.Admin) || AppContextHelper.HasGroupAuthority(GroupAuthorityCode.HR))
    {

#line default
#line hidden
#nullable disable
                WriteLiteral("        <ul class=\"nav admin-nav\">\r\n            <li id=\"Home\" class=\"nav-item\">\r\n                <a");
                BeginWriteAttribute("href", " href=\"", 2888, "\"", 2926, 1);
#nullable restore
#line 58 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
WriteAttributeValue("", 2895, Url.Action("HomePage", "Home"), 2895, 31, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"nav-link\">Portal</a>\r\n            </li>\r\n");
#nullable restore
#line 60 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
             if (AppContextHelper.HasGroupAuthority(GroupAuthorityCode.Admin))
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                <li id=\"Admin\" class=\"nav-item\">\r\n                    <a");
                BeginWriteAttribute("href", " href=\"", 3143, "\"", 3179, 1);
#nullable restore
#line 63 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
WriteAttributeValue("", 3150, Url.Action("Index", "Admin"), 3150, 29, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"nav-link\">ผู้ดูแลระบบ</a>\r\n                </li>\r\n");
#nullable restore
#line 65 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 66 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
             if (AppContextHelper.HasGroupAuthority(GroupAuthorityCode.Admin))
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                <li id=\"Admin\" class=\"nav-item\">\r\n                    <a");
                BeginWriteAttribute("href", " href=\"", 3420, "\"", 3456, 1);
#nullable restore
#line 69 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
WriteAttributeValue("", 3427, Url.Action("Index", "Admin"), 3427, 29, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"nav-link\">ตั้งค่าข้อมูล Master</a>\r\n                </li>\r\n");
#nullable restore
#line 71 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
            }

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n");
#nullable restore
#line 73 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
             if (AppContextHelper.HasGroupAuthority(GroupAuthorityCode.HR))
            {

#line default
#line hidden
#nullable disable
                WriteLiteral("                <li id=\"Hr\" class=\"nav-item\">\r\n                    <a");
                BeginWriteAttribute("href", " href=\"", 3702, "\"", 3741, 1);
#nullable restore
#line 76 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
WriteAttributeValue("", 3709, Url.Action("Index", "Workflow"), 3709, 32, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"nav-link\">ตั้งค่าส่วนงาน</a>\r\n                </li>\r\n");
#nullable restore
#line 78 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
            }

#line default
#line hidden
#nullable disable
                WriteLiteral("        </ul>\r\n");
#nullable restore
#line 80 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
    }

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    <div class=\"container body-page full-width\">\r\n        <main role=\"main\" class=\"pb-3\">\r\n            ");
#nullable restore
#line 84 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
       Write(await Html.PartialAsync("_NotificationPanel"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n            ");
#nullable restore
#line 85 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
       Write(RenderBody());

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
            <div class=""loading"">
                <p class=""loading-text"">กรุณารอสักครู่</p>
            </div>
        </main>
    </div>

    <footer class=""footer ml-4"">
        <div class=""copyright""><p>Copyright &copy; 2021 สำนักบริหารทรัพยากรมนุษย์ <span class=""new-lines"">จุฬาลงกรณ์มหาวิทยาลัย</span></p></div>
    </footer>
    ");
#nullable restore
#line 95 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
Write(await RenderSectionAsync("Scripts", required: false));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n");
#nullable restore
#line 97 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
     if (AppContextHelper.HasGroupAuthority(GroupAuthorityCode.Admin) || AppContextHelper.HasGroupAuthority(GroupAuthorityCode.HR))
    {

#line default
#line hidden
#nullable disable
                WriteLiteral("        <script>\r\n        $(function () {\r\n            $(\'#");
#nullable restore
#line 101 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
           Write(ViewContext.RouteData.Values["Controller"]);

#line default
#line hidden
#nullable disable
                WriteLiteral("\').addClass(\'active\');\r\n        });\r\n        </script>\r\n");
#nullable restore
#line 104 "C:\project\Chula\CU_HR\CU_InstructorExperts\CU\Views\Shared\_AuthorizeLayout.cshtml"
    }

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
</html>

<script>
    $(function () {
        $(document).on('submit', function (event) {
            $("".loading"").fadeIn();
        });

        $(""a[href]"").not('[target=""_blank""]').on(""click"", function () {
            $("".loading"").fadeIn();
        });
    });
</script>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
