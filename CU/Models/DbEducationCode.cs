﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbEducationCode
    {
        public int Id { get; set; }
        public string Slart { get; set; }
        public string Faart { get; set; }
    }
}
