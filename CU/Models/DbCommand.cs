﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbCommand
    {
        public int Id { get; set; }
        public string Subty { get; set; }
        public string Cmdc { get; set; }
        public string Cmd { get; set; }
    }
}
