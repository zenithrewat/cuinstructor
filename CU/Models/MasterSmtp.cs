﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterSmtp
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Deleted { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public byte Status { get; set; }
        public int Creator { get; set; }
        public string CreatorName { get; set; }
        public int Editor { get; set; }
        public string EditorName { get; set; }
        public string Description { get; set; }
        public string Hostname { get; set; }
        public int Port { get; set; }
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
        public bool EnableSsl { get; set; }
        public string SecurityProtocol { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsDefault { get; set; }
    }
}
