﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbSecurityUserRole
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public DateTime Created { get; set; }
        public int? Creator { get; set; }
        public string CreatorName { get; set; }
        public DateTime Deleted { get; set; }
        public int Editor { get; set; }
        public string EditorName { get; set; }
        public bool IsDeleted { get; set; }

        public virtual DbSecurityRole Role { get; set; }
        public virtual DbSecurityUser User { get; set; }
    }
}
