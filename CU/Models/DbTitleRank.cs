﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbTitleRank
    {
        public int Id { get; set; }
        public string Art { get; set; }
        public string Titel { get; set; }
        public string Ttout { get; set; }
        public string Ztout { get; set; }
    }
}
