﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CU.Constants;
using CU.Models;
using CU.Utilities;
using Microsoft.EntityFrameworkCore;

namespace CU.Services
{
    public class UserService : BaseService
    {
        public UserService()
        {

        }

        public UserDisplay GetById(int id)
        {
            var output = new UserDisplay();
            if (id == 0)
            {
                output.Id = 0;
                output.Username = Environment.GetEnvironmentVariable("ADMINUSER");
                output.Fullname = Environment.GetEnvironmentVariable("ADMINUSER");
                output.Email = "";
            }
            else
            {
                using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
                {
                    var target = _dbcontext.DbSecurityUsers.Include(x => x.DbSecurityUserRoles).ThenInclude(x => x.Role).Where(x => x.Id == id).FirstOrDefault();

                    if (target != null)
                    {
                        output = Mapper.ToUserDisplay(target);
                    }
                    else
                    {
                        throw new Exception($"User (id:{id}) not found");
                    }
                }
            }

            return output;
        }

        public List<UserDisplay> GetListById(List<int> idlist)
        {
            var output = new List<UserDisplay>
            {
                new UserDisplay()
                {
                    Id = 0,
                    Username = Environment.GetEnvironmentVariable("ADMINUSER"),
                    Fullname = Environment.GetEnvironmentVariable("ADMINUSER"),
                    Email = ""
                }
            };

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var targetlist = _dbcontext.DbSecurityUsers.Include(x => x.DbSecurityUserRoles).ThenInclude(x => x.Role).Where(x => idlist.Contains(x.Id)).ToList();

                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(Mapper.ToUserDisplay(target));
                    }
                }
            }

            return output;
        }

        public UserDisplay GetByUsername(string username)
        {
            var output = new UserDisplay();
            if (username == Environment.GetEnvironmentVariable("ADMINUSER"))
            {
                output.Id = 0;
                output.Username = username;
                output.Fullname = username;
                output.Email = "";
            }
            else
            {
                using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
                {
                    var target = _dbcontext.DbSecurityUsers.Include(x => x.DbSecurityUserRoles).ThenInclude(x => x.Role).Where(x => x.Username == username).FirstOrDefault();

                    if (target != null)
                    {
                        output = Mapper.ToUserDisplay(target);
                    }
                    else
                    {
                        throw new Exception($"User (username:{username}) not found");
                    }
                }
            }

            return output;
        }

        public List<UserDisplay> GetExistForPage(int pageIndex, string keyword, out int totalItem, out int actualIndex)
        {
            var output = new List<UserDisplay>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                IQueryable<DbSecurityUser> query = _dbcontext.DbSecurityUsers.Include(x => x.DbSecurityUserRoles).ThenInclude(x => x.Role).Where(x => x.IsDeleted == false);

                if (!String.IsNullOrEmpty(keyword))
                {
                    query = query.Where(r => r.Fullname.Contains(keyword) || r.Email.Contains(keyword));
                }

                totalItem = query.Count();
                if (pageIndex > 0)
                {
                    var lastPageIndex = totalItem % AppConstant.pageSize != 0 ? (int)Math.Floor((double)totalItem / AppConstant.pageSize) : (int)Math.Floor((double)totalItem / AppConstant.pageSize) - 1;
                    if (pageIndex > lastPageIndex) pageIndex = lastPageIndex;
                }
                else pageIndex = 0;
                actualIndex = pageIndex;

                var targetlist = query.OrderBy(r => r.Created).Skip(AppConstant.pageSize * pageIndex).Take(AppConstant.pageSize).OrderByDescending(r => r.Modified).ToList();
                if (targetlist != null)
                {
                    foreach (var target in targetlist)
                    {
                        output.Add(Mapper.ToUserDisplay(target));
                    }
                }
            }

            return output;
        }

        public bool Save(UserDisplay UserDisplay, out string Msg, out bool isNew)
        {
            Msg = "";
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                if (_dbcontext.DbSecurityUsers.Where(r => r.Id != UserDisplay.Id && r.Username == UserDisplay.Username && r.IsDeleted == false).FirstOrDefault() == null)
                {
                    var User = _dbcontext.DbSecurityUsers.Include(x => x.DbSecurityUserRoles).ThenInclude(x => x.Role).Where(u => u.Id == UserDisplay.Id && u.IsDeleted == false).FirstOrDefault();
                    if (User == null)
                    {
                        // Create
                        isNew = true;
                        return CreateUser(_dbcontext, UserDisplay, User, out Msg);
                    }
                    else
                    {
                        // Update
                        isNew = false;
                        return UpdateUser(_dbcontext, UserDisplay, User, out Msg);
                    }

                }
                else
                {
                    isNew = false;
                    Msg = "ไม่สามารถใช้ username นี้ได้";
                }
            }

            return false;
        }

        public bool CreateUser(CUSpecialInstructorContext _dbcontext, UserDisplay UserDisplay, DbSecurityUser User, out string Msg)
        {
            User = new DbSecurityUser
            {
                Created = DateTime.UtcNow,
                Creator = AppContextHelper.User.Id,
                CreatorName = AppContextHelper.User.Fullname,

                Modified = DateTime.UtcNow,
                Editor = AppContextHelper.User.Id,
                EditorName = AppContextHelper.User.Fullname,

                IsEnabled = true,
                IsDeleted = false,
                Status = (byte)AppConstant.Status.Normal,

                Username = UserDisplay.Username,
                Fullname = UserDisplay.Fullname,
                Email = UserDisplay.Email,
                //Emailsecondary = UserDisplay.EmailSecondary,
            };

            _dbcontext.DbSecurityUsers.Add(User);
            if (_dbcontext.SaveChanges() > 0)
            {
                var selectedRoles = UserDisplay.Roles.Select(r => r.Name).ToList();
                if (selectedRoles.Count > 0)
                {
                    var Roles = _dbcontext.DbSecurityRoles.Where(r => selectedRoles.Contains(r.Rolename) && r.IsDeleted == false).ToList();
                    Roles.ForEach(r => User.DbSecurityUserRoles.Add(
                        new DbSecurityUserRole
                        {
                            Created = DateTime.UtcNow,
                            Creator = AppContextHelper.User.Id,
                            CreatorName = AppContextHelper.User.Fullname,

                            Editor = AppContextHelper.User.Id,
                            EditorName = AppContextHelper.User.Fullname,

                            IsDeleted = false,

                            UserId = User.Id,
                            RoleId = r.Id
                        }
                    ));
                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Create new User (id:{User.Id})");
                        Msg = "ทำการเพิ่มผู้ใช้งานใหม่สำเร็จ";
                        return true;
                    }
                }
            }
            Msg = "ทำการเพิ่มผู้ใช้งานใหม่ไม่สำเร็จ";
            return false;
        }

        public bool UpdateUser(CUSpecialInstructorContext _dbcontext, UserDisplay UserDisplay, DbSecurityUser User, out string Msg)
        {
            User.Modified = DateTime.UtcNow;
            User.Editor = AppContextHelper.User.Id;
            User.EditorName = AppContextHelper.User.Fullname;

            User.Username = UserDisplay.Username;
            User.Fullname = UserDisplay.Fullname;
            User.Email = UserDisplay.Email;
            //User.Emailsecondary = UserDisplay.EmailSecondary;

            if (_dbcontext.SaveChanges() > 0)
            {
                var existRoles = User.DbSecurityUserRoles.Where(r => r.IsDeleted == false).ToList();
                Role tempRole;
                int cntChange = 0;
                foreach (var userrole in existRoles)
                {
                    tempRole = UserDisplay.Roles.Where(r => r.Name == userrole.Role.Rolename).FirstOrDefault();
                    if (tempRole == null)
                    {
                        // Delete role
                        userrole.Deleted = DateTime.UtcNow;
                        userrole.Editor = AppContextHelper.User.Id;
                        userrole.EditorName = AppContextHelper.User.Fullname;

                        userrole.IsDeleted = true;

                        cntChange++;
                    }
                    UserDisplay.Roles.Remove(tempRole);
                }
                //Add role
                var selectedRoles = UserDisplay.Roles.Select(r => r.Name).ToList();
                if (selectedRoles.Count > 0)
                {
                    var Roles = _dbcontext.DbSecurityRoles.Where(r => selectedRoles.Contains(r.Rolename) && r.IsDeleted == false).ToList();
                    Roles.ForEach(r => _dbcontext.DbSecurityUserRoles.Add(
                        new DbSecurityUserRole
                        {
                            Created = DateTime.UtcNow,
                            Creator = AppContextHelper.User.Id,
                            CreatorName = AppContextHelper.User.Fullname,

                            Editor = AppContextHelper.User.Id,
                            EditorName = AppContextHelper.User.Fullname,

                            IsDeleted = false,

                            UserId = User.Id,
                            RoleId = r.Id
                        }
                    ));
                    cntChange += Roles.Count();
                }
                if (cntChange > 0 && _dbcontext.SaveChanges() != cntChange)
                {
                    Msg = "ทำการแก้ไขผู้ใช้งานไม่สำเร็จ";
                    return false;
                }
                log.Info($"Update User (id:{User.Id})");
                Msg = "ทำการแก้ไขผู้ใช้งานสำเร็จ";
                return true;
            }
            Msg = "ทำการแก้ไขผู้ใช้งานไม่สำเร็จ";
            return false;
        }

        public bool Delete(int id)
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.DbSecurityUsers.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();

                if (target != null)
                {
                    target.Deleted = DateTime.UtcNow;
                    target.Editor = AppContextHelper.User.Id;
                    target.EditorName = AppContextHelper.User.Fullname;

                    target.IsDeleted = true;

                    if (_dbcontext.SaveChanges() > 0)
                    {
                        log.Info($"Delete User {target.Username} (id:{target.Id})");
                        return true;
                    }
                }
            }
            return false;
        }
    }
}