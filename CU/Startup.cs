using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StackExchange.Redis;
using CU.Constants;
using System;


namespace CU
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Utilities.PathHelpers.ContentRootPath = env.ContentRootPath;
            Services.InitService initService = new Services.InitService();
            initService.Init();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            if (Environment.GetEnvironmentVariable("REDIS_ENABLE") == "1")
            {
                // Configure Redis Based Distributed Session
                var redisConfigurationOptions = ConfigurationOptions.Parse(Environment.GetEnvironmentVariable("REDIS_SERVERIP"));

                services.AddStackExchangeRedisCache(redisCacheConfig =>
                {
                    redisCacheConfig.ConfigurationOptions = redisConfigurationOptions;
                    redisCacheConfig.InstanceName = "Session_";
                });
            }

            services.AddMvc().AddSessionStateTempDataProvider();
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
                options.IdleTimeout = TimeSpan.FromMinutes(10);
            });

            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
           .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
           {
               options.Cookie.HttpOnly = true;

               options.LoginPath = new PathString("/Account/Login");
               options.AccessDeniedPath = new PathString("/Error/Forbidden");
               options.SlidingExpiration = true;
           });
            services.AddSingleton<IAuthorizationHandler, Utilities.CheckAuthorizationHandler>();
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyName.GeneralUser, policy =>
                   policy.Requirements.Add(new Models.CheckAuthorizeRequirement("")));

                options.AddPolicy(PolicyName.AdminPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(GroupAuthorityCode.Admin)));
                options.AddPolicy(PolicyName.LdapPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.LdapFullControl)));
                options.AddPolicy(PolicyName.SmtpPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.SmtpFullControl)));
                options.AddPolicy(PolicyName.UserPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.UserFullControl)));
                options.AddPolicy(PolicyName.RolePage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.RoleFullControl)));
                options.AddPolicy(PolicyName.LogPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.LogFullControl)));
                options.AddPolicy(PolicyName.FTPPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.FTPFullControl)));
                options.AddPolicy(PolicyName.SmsGatewayPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.SmsGatewayFullControl)));
                options.AddPolicy(PolicyName.JobPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.JobFullControl)));
                options.AddPolicy(PolicyName.ImportCsvPage, policy =>
                    policy.Requirements.Add(new Models.CheckAuthorizeRequirement(AuthorityCode.ImportCsvFullControl)));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            NLog.LogManager.Configuration.Variables["nlogConnectionstring"] = System.Environment.GetEnvironmentVariable("CONNECTION_STRING");

            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseStatusCodePagesWithReExecute("/Error/Index/{0}");

            // Exception handling logging below
            app.UseExceptionHandler("/Error");

            
            app.UseStaticFiles();

            app.UseSession();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            Services.AppContextHelper.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "shorturl",
                    pattern: "{refno}",
                    defaults: new { controller = "ShortUrl", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}