﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace CU.Utilities
{
    public static class DateTimeUtility
    {
        public static string ToFullThaiTime(DateTime input)
        {
            return input.ToLocalTime().ToString("d MMM yyyy H:mm:ss น", CultureInfo.CreateSpecificCulture("th-TH"));
        }

        public static string ToFullThaiDate(DateTime input)
        {
            return input.ToLocalTime().ToString("d MMMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));
        }

        public static string ToShortThaiDate(DateTime input)
        {
            return input.ToLocalTime().ToString("d MMM yyyy", CultureInfo.CreateSpecificCulture("th-TH"));
        }

        public static string ToDatePickerTime(DateTime input)
        {
            return input.ToString("dd/MM/yyyy");
        }

        public static int GetCurrentThaiGovPeriod()
        {
            var now = DateTime.Now;
            if (now.Month >= 10) return (now.Year + 544);
            return (now.Year + 543);
        }

        public static int GetCurrentThaiGovPeriod2Digits()
        {
            var now = DateTime.Now;
            if (now.Month >= 10) return (now.Year + 544)%100;
            return (now.Year + 543)%100;
        }

        public static string GetDuration(int inputMinutes)
        {
            string output = "";
            var hour = inputMinutes / 60;
            if (hour >= 1) output += $"{hour} hours";
            var minute = inputMinutes % 60;
            if (minute >= 1) output = (string.IsNullOrEmpty(output) ? "" : output + " ") + $"{minute} minutes";
            return output;
        }

        public static string GetDuration(double inputMinutes)
        {
            string output = "";
            var hour = (int)Math.Ceiling(inputMinutes) / 60;
            if (hour >= 1) output += $"{hour} hours";
            var minute = (int)Math.Ceiling(inputMinutes) % 60;
            if (minute >= 1) output = (string.IsNullOrEmpty(output) ? "" : output + " ") + $"{minute} minutes";
            return output;
        }
    }
}