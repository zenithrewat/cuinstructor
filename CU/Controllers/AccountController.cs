﻿using CU.Models;
using CU.Services;
using Microsoft.AspNetCore.Mvc;

namespace CU.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Account
        public IActionResult Index()
        {
            return RedirectToAction("Login", "Account");
        }

        public IActionResult Login()
        {
            return RedirectToDefaultPage(View());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(AuthenUser user, string ReturnUrl = "")
        {
            if (ModelState.IsValid && AppContextHelper.Login(user))
            {
                log.Info($"{AppContextHelper.User.Username} logged in (Session : {AppContextHelper._httpContext.Session.Id})");
                if (Url.IsLocalUrl(ReturnUrl))
                    return Redirect(ReturnUrl);
                else
                    return RedirectToDefaultPage(View());
            }
            if (user.username != null && user.password != null) ModelState.AddModelError("ErrorMsg", "Invalid Username or Password");
            log.Info($"Fail to log in as { user.username } (Session : {HttpContext.Session.Id})");
            return View();
        }

        public IActionResult Logout()
        {
            if (AppContextHelper.User != null) log.Info($"{AppContextHelper.User.Username} logged out (Session : {HttpContext.Session.Id})");
            AppContextHelper.Logout();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            return Json(GetSuccess());
        }
    }
}