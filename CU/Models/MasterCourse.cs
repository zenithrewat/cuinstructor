﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterCourse
    {
        public int Id { get; set; }
        public int? Programid { get; set; }
        public string Muano { get; set; }
        public string ProgramnameTh { get; set; }
        public string ProgramnameEn { get; set; }
        public string TitledegreeTh { get; set; }
        public string TitledegreeEn { get; set; }
        public string Facultycode { get; set; }
        public string Departmentcode { get; set; }
        public string Levelcode { get; set; }
        public int? Creator { get; set; }
        public string Creatorname { get; set; }
        public int? Editor { get; set; }
        public string Editorname { get; set; }
        public bool? IsFixed { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime Modified { get; set; }
        public string Degree { get; set; }
        public string Major { get; set; }
        public string Programsystem { get; set; }
        public string Calendar { get; set; }
    }
}
