﻿using CU.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using CU.Services;

namespace CU.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            
            return RedirectToDefaultPage(View());
        }

        public IActionResult HomePage()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(AuthenUser user, string ReturnUrl = "")
        {
            if (ModelState.IsValid && AppContextHelper.Login(user))
            {
                log.Info($"{AppContextHelper.User.Username} logged in (Session : {AppContextHelper._httpContext.Session.Id})");
                if (Url.IsLocalUrl(ReturnUrl))
                    return Redirect(ReturnUrl);
                else
                    return RedirectToDefaultPage(View());
            }
            if (user.username != null && user.password != null) ModelState.AddModelError("ErrorMsg", "Invalid Username or Password");
            log.Info($"Fail to log in as { user.username } (Session : {HttpContext.Session.Id})");
            return View();
        }

        public IActionResult Logout()
        {
            if (AppContextHelper.User != null) log.Info($"{AppContextHelper.User.Username} logged out (Session : {HttpContext.Session.Id})");
            AppContextHelper.Logout();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            return Json(GetSuccess());
        }
    }
}
