﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbInstructor
    {
        public int Id { get; set; }
        public string PerId { get; set; }
        public string DosStatusCode { get; set; }
        public string TitleThCode { get; set; }
        public string NameTh { get; set; }
        public string SurNameTh { get; set; }
        public string TitleEngCode { get; set; }
        public string NameEng { get; set; }
        public string SurNameEng { get; set; }
        public string TitleCode1 { get; set; }
        public string MilitaryTitleCode { get; set; }
        public string TitleCode2 { get; set; }
        public string TitleOther { get; set; }
        public string TitleCode3 { get; set; }
        public string Sex { get; set; }
        public string BirthDate { get; set; }
        public string NationCode { get; set; }
        public string IdCard { get; set; }
        public string IdCardStartDate { get; set; }
        public string IdCardEndDate { get; set; }
        public string Passport { get; set; }
        public string PassportStartDate { get; set; }
        public string PassportEndDate { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string TaxId { get; set; }
        public string BankCode { get; set; }
        public string Branch { get; set; }
        public string AccountNo { get; set; }
        public string DocStatusCode { get; set; }
    }
}
