﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterTitleName
    {
        public int Id { get; set; }
        public string Anred { get; set; }
        public string Atext { get; set; }
        public string Anrlt { get; set; }
        public string TitleEng { get; set; }
    }
}
