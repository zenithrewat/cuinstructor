﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CU.Models
{
    public class MailTemplateDisplay
    {
        public int Id { get; set; }
        public string MailKey { get; set; }
        public string MailTo { get; set; }
        public string MailFrom { get; set; }
        public string MailCC { get; set; }
        public string MailBody { get; set; }
        public string MailSubject { get; set; }
        public bool IsEnable { get; set; }
        public string TemplateId { get; set; }
    }
}
