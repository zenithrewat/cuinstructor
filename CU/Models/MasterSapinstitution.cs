﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterSapinstitution
    {
        public int Id { get; set; }
        public string Slart { get; set; }
        public string Schcd { get; set; }
        public string Insti { get; set; }
    }
}
