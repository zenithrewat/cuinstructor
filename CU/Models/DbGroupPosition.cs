﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbGroupPosition
    {
        public int Id { get; set; }
        public string Infty { get; set; }
        public string Subty { get; set; }
        public string Stext { get; set; }
    }
}
