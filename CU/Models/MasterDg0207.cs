﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class MasterDg0207
    {
        public int Id { get; set; }
        public string CourseNo { get; set; }
        public string Degree { get; set; }
        public string Major { get; set; }
        public string ProgramSystem { get; set; }
        public string Calendar { get; set; }
        public int? Creator { get; set; }
        public string Creatorname { get; set; }
        public int? Editor { get; set; }
        public string Editorname { get; set; }
        public bool? IsFixed { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
