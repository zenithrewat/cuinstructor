﻿using System;
using System.ComponentModel.DataAnnotations;
using CU.Constants;

namespace CU.Models
{
    public class LdapDisplay
    {
        public int id { get; set; }
        public string host_name { get; set; }
        public int port { get; set; }
        public string base_path { get; set; }
        public string directory_path { get; set; }
        public string domain_name { get; set; }
        public bool enable_ssl { get; set; }
        public string protocol_version { get; set; }
        public bool is_authenticated { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        [Compare("password", ErrorMessage = AppConstant.PassNotMacth)]
        public string confirmpassword { get; set; }
        public bool is_default { get; set; }
        public int creator { get; set; }
        public string creator_name { get; set; }
        public int editor { get; set; }
        public string editor_name { get; set; }
        public DateTime created_date { get; set; }
    }
}