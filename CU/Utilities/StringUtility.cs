﻿using System;

namespace CU.Utilities
{
    public static class StringUtility
    {
        public static string ToCurrency(string input)
        {
            return string.Format("{0:#,##0.##}", input);
        }

        public static string ToCurrency(int input)
        {
            return string.Format("{0:#,##0.##}", input);
        }

        public static string ToCurrency(float input)
        {
            return string.Format("{0:#,##0.##}", input);
        }

        public static string ToCurrency(double input)
        {
            return string.Format("{0:#,##0.##}", input);
        }

        public static string ToCurrency(decimal input)
        {
            return string.Format("{0:#,##0.##}", input);
        }

        public static string ToCurrencyFix2Digit(string input)
        {
            return string.Format("{0:#,##0.00}", input);
        }

        public static string ToCurrencyFix2Digit(int input)
        {
            return string.Format("{0:#,##0.00}", input);
        }

        public static string ToCurrencyFix2Digit(float input)
        {
            return string.Format("{0:#,##0.00}", input);
        }

        public static string ToCurrencyFix2Digit(double input)
        {
            return string.Format("{0:#,##0.00}", input);
        }

        public static string ToCurrencyFix2Digit(decimal input)
        {
            return string.Format("{0:#,##0.00}", input);
        }

        public static string Reverse(string input)
        {
            char[] chars = input.ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }

        public static string CensorMobileNo(string input)
        {
            if (input.Length < 4) return input;
            else return input[0..^4] + "xxxx";
        }
    }
}