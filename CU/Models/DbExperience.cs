﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CU.Models
{
    public partial class DbExperience
    {
        public int Id { get; set; }
        public string ClassLevel { get; set; }
        public string Institution { get; set; }
        public int? StartYear { get; set; }
        public int? EndYear { get; set; }
        public int? TotalPeriod { get; set; }
        public int InstructorId { get; set; }
        public int? Creator { get; set; }
        public string CreatorName { get; set; }
        public int? Editor { get; set; }
        public string EditorName { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Deleted { get; set; }
        public DateTime? Modified { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsEdit { get; set; }
    }
}
