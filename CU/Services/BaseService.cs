﻿using NLog;

namespace CU.Services
{
    public class BaseService
    {
        protected ILogger log = NLog.LogManager.GetCurrentClassLogger();
    }
}