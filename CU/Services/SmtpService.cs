﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using CU.Constants;
using CU.Models;
using CU.Utilities;
using System.Net.Mime;
using Microsoft.AspNetCore.Http.Features;

namespace CU.Services
{
    public class SmtpService : BaseService
    {
        public SmtpService()
        {

        }

        public IEnumerable<SmtpDisplay> GetAll()
        {
            var jsonList = new List<SmtpDisplay>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var list = _dbcontext.MasterSmtps.Where(x => x.IsDeleted == false).ToList();

                if (list != null)
                {
                    list.ForEach(e => jsonList.Add(Mapper.ToSmtpDisplay(e)));
                }
            }

            return jsonList;
        }

        public SmtpDisplay GetById(int id)
        {
            var json = new SmtpDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSmtps.Where(x => x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToSmtpDisplay(target);
                }
            }

            return json;
        }

        public MailTemplateDisplay GetMailTemplateById(int id)
        {
            var json = new MailTemplateDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterNotifications.Where(x => x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToMailTemplate(target);
                }
            }

            return json;
        }

        public MailTemplateDisplay GetTemplateByKey(string key)
        {
            var json = new MailTemplateDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterNotifications.Where(x => x.MailKey == key).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToMailTemplate(target);
                }
            }

            return json;
        }

        public SmtpDisplay GetDefault()
        {
            var json = new SmtpDisplay();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSmtps.Where(x => x.IsDeleted == false && x.IsDefault == true).OrderByDescending(x => x.Created).FirstOrDefault();

                if (target != null)
                {
                    json = Mapper.ToSmtpDisplay(target);
                }
            }
            return json;
        }

        public List<MailTemplateDisplay> GetMailTemplate()
        {
            var json = new List<MailTemplateDisplay>();
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var output = _dbcontext.MasterNotifications.OrderByDescending(x => x.Id).AsQueryable(); //Where(x => x.IsEnable == true)

                if (output != null)
                {
                    List<MasterNotification> target = new List<MasterNotification>();
                    target = output.ToList();
                    foreach(MasterNotification obj in target)
                    {
                        json.Add(Mapper.ToMailTemplate(obj));
                    }
                    
                }
            }
            return json;
        }

        public bool Save(SmtpDisplay json)
        {
            var model = new MasterSmtp();
            json.username = (json.username ?? "");
            json.password = (json.password ?? "");
            model = Mapper.ToMasterSmtp(json);

            //Insert new
            if (model.Id <= 0)
            {
                return Insert(model, json);
            }

            //Update
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSmtps.Where(x => x.Id == model.Id).FirstOrDefault();

                if (target == null)
                {
                    throw new ArgumentOutOfRangeException("Smtp", "Item not found.");
                }

                target.Modified = DateTime.UtcNow;
                target.Editor = AppContextHelper.User.Id;
                target.EditorName = AppContextHelper.User.Fullname;
                target.Hostname = model.Hostname;
                target.Port = model.Port;
                target.SenderEmail = model.SenderEmail;
                target.SenderName = model.SenderName;
                target.EnableSsl = model.EnableSsl;
                target.IsAuthenticated = model.IsAuthenticated;
                if (model.IsAuthenticated)
                {
                    target.Username = model.Username;
                    target.Password = CryptoHelpers.Encrypt(json.password);
                }     

                return _dbcontext.SaveChanges() > 0;
            }
        }

        public bool SaveMailTemplate(MailTemplateDisplay json)
        {
            var model = new MasterNotification();
            model = Mapper.ToDbMailTemplate(json);

            //Insert new
            if (model.Id <= 0)
            {
                return InsertMailTemplate(model, json);
            }

            //Update
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterNotifications.Where(x => x.Id == model.Id).FirstOrDefault();

                if (target == null)
                {
                    throw new ArgumentOutOfRangeException("Mailtemplate", "Item not found.");
                }

                
                target.MailKey = model.MailKey;
                target.MailSubject = model.MailSubject;
                target.MailTo = model.MailTo;
                target.MailFrom = model.MailFrom;
                target.MailCc = model.MailCc;
                target.MailBody = model.MailBody;
                target.IsEnable = model.IsEnable;

                return (_dbcontext.SaveChanges() > 0);
                
                
            }
        }

        public bool Insert(MasterSmtp model, SmtpDisplay json)
        {
            model.Status = (byte)AppConstant.Status.Normal;
            model.IsEnabled = true;
            model.IsDeleted = false;
            model.Created = DateTime.UtcNow;
            model.Modified = DateTime.UtcNow;
            model.Deleted = new DateTime();

            if (string.IsNullOrEmpty(model.CreatorName))
            {
                model.Creator = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
                model.CreatorName = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            }

            if (string.IsNullOrEmpty(model.EditorName))
            {
                model.Editor = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
                model.EditorName = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            }

            model.Password = CryptoHelpers.Encrypt(json.password);

            if (GetDefault().id == 0) model.IsDefault = true;

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterSmtps.Add(model);
                _dbcontext.SaveChanges();
            }

            return model.Id > 0;
        }

        public bool InsertMailTemplate(MasterNotification model, MailTemplateDisplay json)
        {
            model.IsEnable = true;
            model.MailKey = json.MailKey;
            model.MailTo = json.MailTo;
            model.MailFrom = json.MailFrom;
            model.MailCc = json.MailCC;
            model.MailBody = json.MailBody;
            model.MailSubject = json.MailSubject;

            //if (string.IsNullOrEmpty(model.Creatorname))
            //{
            //    model.Creator = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
            //    model.Creatorname = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            //}

            //if (string.IsNullOrEmpty(model.Editorname))
            //{
            //    model.Editor = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreator() : AppContextHelper.User.Id;
            //    model.Editorname = AppContextHelper.User == null ? ConfigHelpers.GetDefaultCreatorName() : AppContextHelper.User.Fullname;
            //}

            //model.Password = CryptoHelpers.Encrypt(json.password);

            //if (GetDefault().id == 0) model.Isdefault = true;

            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                _dbcontext.MasterNotifications.Add(model);
                _dbcontext.SaveChanges();
            }

            return model.Id > 0;
        }

        public bool Delete(int id)
        {
            using (CUSpecialInstructorContext _dbcontext = new CUSpecialInstructorContext())
            {
                var target = _dbcontext.MasterSmtps.Where(x => x.Id == id).FirstOrDefault();

                if (target != null)
                {
                    target.IsDeleted = true;
                    target.Deleted = DateTime.UtcNow;
                    return _dbcontext.SaveChanges() > 0;
                }
            }
            return false;
        }

        public string Subject { set; get; }

        public string Message { set; get; }

        public string From { set; get; }

        public string[] To { set; get; }

        public string[] Cc { set; get; }

        public string Attached { set; get; }

        public async Task<bool> Send()
        {
            // get server
            var smtpServer = GetDefault();
            var appMode = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var emailTest = Environment.GetEnvironmentVariable("EMAILTEST");
            var emailPrefix = Environment.GetEnvironmentVariable("EMAILPREFIX");
            var tlsVersion = Environment.GetEnvironmentVariable("TLSVERSION");

            if (smtpServer != null)
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(smtpServer.sender_email, string.IsNullOrEmpty(From) ? smtpServer.sender_name : From);

                    if (appMode == "Development")//test
                    {
                        mail.To.Add(emailTest);
                    }
                    else
                    {
                        foreach (var address in To)
                        {
                            mail.To.Add(address);
                        }

                        if (Cc != null)
                        {
                            foreach (var address in Cc)
                            {
                                mail.CC.Add(address);
                            }
                        }
                    }

                    mail.Subject = string.Format("{0}{1}", emailPrefix, Subject).Trim();
                    mail.Body = Message;
                    mail.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient();

                    smtp.Host = smtpServer.host_name;
                    smtp.EnableSsl = smtpServer.enable_ssl;
                    smtp.UseDefaultCredentials = true;
                    smtp.Port = smtpServer.port;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    if (smtpServer.is_authenticated)
                    {
                        NetworkCredential networkCredential = new NetworkCredential(smtpServer.username, CryptoHelpers.Decrypt(smtpServer.password));
                        smtp.Credentials = networkCredential;
                    }

                    if (!string.IsNullOrEmpty(Attached))
                    {
                        var attachment = File.ReadAllBytes(Attached);
                        log.Debug("send email {0} with attached file {1}", Subject, attachment);

                        if (attachment != null)
                        {
                            using (System.IO.Stream stream = new System.IO.MemoryStream(attachment))
                            {
                                var extension = Path.GetExtension(Attached);

                                mail.Attachments.Add(new Attachment(stream, string.Format("cubudget-attached.{0}", extension)));
                                log.Debug("attached file added");

                                smtp.SendCompleted += (s, e) => { smtp.Dispose(); mail.Dispose(); };
                                await smtp.SendMailAsync(mail);

                                log.Debug("email sent");
                                return true;
                            }
                        }
                    }
                    else
                    {
                        log.Debug("send email {0} without attached file", Subject);

                        smtp.SendCompleted += (s, e) => { smtp.Dispose(); mail.Dispose(); };
                        await smtp.SendMailAsync(mail);

                        log.Debug("email sent");
                        return true;
                    }
                }
            }
            else
            {
                throw new Exception("no smtp server setting");
            }
            return false;
        }

        public async Task Send(LinkedResource[] linkedResources)
        {
            // get server
            var smtpServer = GetDefault();
            var appMode = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var emailTest = Environment.GetEnvironmentVariable("EMAILTEST");
            var emailPrefix = Environment.GetEnvironmentVariable("EMAILPREFIX");
            var tlsVersion = Environment.GetEnvironmentVariable("TLSVERSION");

            if (smtpServer != null)
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(smtpServer.sender_email, string.IsNullOrEmpty(From) ? smtpServer.sender_name : From);

                    if (appMode == "Development")//test
                    {
                        mail.To.Add(emailTest);
                    }
                    else
                    {
                        foreach (var address in To)
                        {
                            mail.To.Add(address);
                        }

                        if (Cc != null)
                        {
                            foreach (var address in Cc)
                            {
                                mail.CC.Add(address);
                            }
                        }
                    }

                    mail.Subject = string.Format("{0}{1}", emailPrefix, Subject).Trim();
                    AlternateView av = AlternateView.CreateAlternateViewFromString(Message, null, MediaTypeNames.Text.Html);
                    foreach(var linkedResource in linkedResources)
                    {
                        av.LinkedResources.Add(linkedResource);
                    }
                    mail.AlternateViews.Add(av);
                    mail.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient();

                    smtp.Host = smtpServer.host_name;
                    smtp.EnableSsl = smtpServer.enable_ssl;
                    smtp.UseDefaultCredentials = true;
                    smtp.Port = smtpServer.port;
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    if (smtpServer.is_authenticated)
                    {
                        NetworkCredential networkCredential = new NetworkCredential(smtpServer.username, CryptoHelpers.Decrypt(smtpServer.password));
                        smtp.Credentials = networkCredential;
                    }

                    if (!string.IsNullOrEmpty(Attached))
                    {
                        var attachment = System.IO.File.ReadAllBytes(Attached);
                        log.Debug("send email {0} with attached file {1}", Subject, attachment);

                        if (attachment != null)
                        {
                            using (Stream stream = new MemoryStream(attachment))
                            {
                                var extension = Path.GetExtension(Attached);

                                mail.Attachments.Add(new Attachment(stream, string.Format("cubudget-attached.{0}", extension)));
                                log.Debug("attached file added");

                                smtp.SendCompleted += (s, e) => { smtp.Dispose(); mail.Dispose(); };
                                await smtp.SendMailAsync(mail);

                                log.Debug("email sent");
                            }
                        }
                    }
                    else
                    {
                        log.Debug("send email {0} without attached file", Subject);

                        smtp.SendCompleted += (s, e) => { smtp.Dispose(); mail.Dispose(); };
                        await smtp.SendMailAsync(mail);

                        log.Debug("email sent");
                    }
                }
            }
            else
            {
                throw new Exception("no smtp server setting");
            }
        }

        public string TestAsync(string to_email)
        {
            try
            {
                var LinkServer = AppContextHelper._httpContext.Features.Get<IHttpConnectionFeature>().LocalIpAddress;

                Subject = "SMTP Server Setting Test for CU Enterprise Portal";
                Message = "This is test message from server " + LinkServer;
                To = new string[] { to_email };

                if (Send().Result)
                {
                    return "";
                }

                return $"Error during sent mail to {to_email}";
            }
            catch (Exception ex)
            {
                log.Error(ex, "testing smtp exception");
                return ex.ToString();
            }
        }

        public void SendMailLoadCSV(MailTemplateDisplay mailTemplate)
        {
            try
            {
                Subject = mailTemplate.MailSubject;
                Message = mailTemplate.MailBody;
                From = mailTemplate.MailFrom;
                To = mailTemplate.MailTo.Split(';');
                if (! string.IsNullOrEmpty(mailTemplate.MailCC)) Cc = mailTemplate.MailCC.Split(';');

                if (!Send().Result)
                {
                    throw new Exception($"Error during sent mail to {mailTemplate.MailTo}");
                }

            }
            catch (Exception ex)
            {
                log.Error(ex, "smtp exception while send mail load csv");
            }
        }
    }
}