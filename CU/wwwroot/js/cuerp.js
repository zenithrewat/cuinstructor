﻿$(document).ready(function () {

    //TitleTH
    $(function () {
        $.ajax({
            url: "/CreateForm/GetTitle",
            data: { 'type': 'TH' }
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {
                    //if (i == 0) {

                        text += "<option  value='" + obj.data[i].anred + "'>" + obj.data[i].anrlt + "</option>";
                    //} else {
                    //    text += "<option value='" + obj.data[i].anred + "'>" + obj.data[i].anrlt + "</option>";
                    //}

                }
                $('#selTitleTH').html(text);

                if ($('#titleTH').val() !== '-1' && $('#titleTH').val() !== '') { //

                    $('select#selTitleTH').val($('#titleTH').val());
                }
            }
        });
    });

    //TitleEng
    $(function () {
        $.ajax({
            url: "/CreateForm/GetTitle",
            data: { 'type': 'ENG' }
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {
                    //if (i == 0) {

                    text += "<option  value='" + obj.data[i].anred + "'>" + obj.data[i].titleEng + "</option>";
                    //} else {
                    //    text += "<option value='" + obj.data[i].anred + "'>" + obj.data[i].anrlt + "</option>";
                    //}

                }
                $('#selTitleEng').html(text);

                if ($('#titleEng').val() !== '-1' && $('#titleEng').val() !== '') { //

                    $('select#selTitleEng').val($('#titleEng').val());
                }
            }
        });
    });


    //selJobTitle1, selJobTitle2
    $(function () {
        $.ajax({
            url: "/CreateForm/GetJobTitle",
            data: { 'type': 'V' }
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].title + "'>" + obj.data[i].ttout + "</option>";

                }
                $('#selJobTitle').html(text);

                if ($('#txtJobTitle').val() !== '-1' && $('#txtJobTitle').val() !== '') { //

                    $('select#selJobTitle').val($('#txtJobTitle').val());
                }

                $('#selJobTitle2').html(text);
                if ($('#txtJobTitle2').val() !== '-1' && $('#txtJobTitle2').val() !== '') { //

                    $('select#selJobTitle2').val($('#txtJobTitle2').val());
                }
            }
        });
    });

    //militarytitle
    $(function () {
        $.ajax({
            url: "/CreateForm/GetJobTitle",
            data: { 'type': 'T' }
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].title + "'>" + obj.data[i].ttout + "</option>";

                }
                $('#selMilitaryTitle').html(text);

                if ($('#txtMilitaryTitle').val() !== '-1' && $('#txtMilitaryTitle').val() !== '') { //

                    $('select#selMilitaryTitle').val($('#txtJobTitle').val());
                }
            }
        });
    });

    //othertitle
    $(function () {
        $.ajax({
            url: "/CreateForm/GetJobTitle",
            data: { 'type': 'S' }
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].title + "'>" + obj.data[i].ttout + "</option>";

                }
                $('#selTitleOther').html(text);

                if ($('#txtTitleOther').val() !== '-1' && $('#txtTitleOther').val() !== '') { //

                    $('select#selTitleOther').val($('#txtTitleOther').val());
                }
            }
        });
    });

    //txtTitle3
    $(function () {
        $.ajax({
            url: "/CreateForm/GetJobTitle",
            data: { 'type': 'Z' }
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].title + "'>" + obj.data[i].ttout + "</option>";

                }
                $('#selTitle3').html(text);

                if ($('#txtTitle3').val() !== '-1' && $('#txtTitle3').val() !== '') { //

                    $('select#selTitle3').val($('#txtTitle3').val());
                }
            }
        });
    });


    //select country
    $(function () {
        $.ajax({
            url: "/CreateForm/GetCountry"            
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var select = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].natio50 + "'>" + obj.data[i].natio + "</option>";
                    select += "<option  value='" + obj.data[i].land1 + "'>" + obj.data[i].landx + "</option>";
                }
                $('#selCountry').html(text);

                if ($('#txtCountry').val() !== '-1' && $('#txtCountry').val() !== '') { //

                    $('select#selCountry').val($('#txtCountry').val());
                }

                //address country
                $('#selCountryAddress').html(select);

                if ($('#txtCountryAddress').val() !== '-1' && $('#txtCountryAddress').val() !== '') { //

                    $('select#selCountryAddress').val($('#txtCountryAddress').val());
                }
                
            }

        });
    });

    //select bank
    $(function () {
        $.ajax({
            url: "/CreateForm/GetBank",
            data: { 'type': 'Z' }
        }).done(function (obj) {
            console.log(obj);
            var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";
            var code = "";
            if (obj.status === 'OK') {
                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].bankl + "'>" + obj.data[i].bankName + "</option>";

                }
                $('#selBank').html(text);

                if ($('#txtBank').val() !== '-1' && $('#txtBank').val() !== '') { //

                    $('select#selBank').val($('#txtCountry').val());
                }
            }
        });
    });

    //get bank branch
    $('#selBank').on('change', function () {

        console.log($(this).val());
        var key = $(this).val();

        $.ajax({
            type: "GET",
            url: "/CreateForm/GetBankBranch",
            data: { 'bankcode': key }
        }).done(function (obj) {
            console.log(obj);
            if (obj.status ==='OK') {
                var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";

                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].bankl + "'>" + obj.data[i].branch + "</option>";

                }
                $('#selBranch').html(text);

                if ($('#txtBranch').val() !== '-1' && $('#txtBranch').val() !== '') { //

                    $('select#selBranch').val($('#txtBranch').val());
                }
            }
            

        });
    });


    //get city address
    $('#selCountryAddress').on('change', function () {
        var contrycode = $(this).val();

        //clear value
        var txt = '';
        $('#selCity').html(txt);
        $('#selDistrict').html(txt);
        $('#selSubDistrict').html(txt);
        $('#txtCity').val('');
        $('#txtDistrict').val('');
        $('#txtSubDistrict').val('');
        $('#txtPostCode').val('');

        $.ajax({
            type: "GET",
            url: "/CreateForm/GetCity",
            data: { 'contrycode': contrycode }
        }).done(function (obj) {
            console.log(obj);
            if (obj.status ==='OK') {
                var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";

                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].bland + "'>" + obj.data[i].bezei + "</option>";

                }
                $('#selCity').html(text);

                if ($('#txtCity').val() !== '-1' && $('#txtCity').val() !== '') { //

                    $('select#selCity').val($('#txtCity').val());
                }
            }
            

        });

    });

    //get district
    $('#selCity').on('change', function () {
        var citycode = $(this).val();
        $('#txtCity').val(citycode);

        //clear value
        var txt = '';
        $('#selDistrict').html(txt);
        $('#selSubDistrict').html(txt);
        $('#txtDistrict').val('');
        $('#txtSubDistrict').val('');
        $('#txtPostCode').val('');

        $.ajax({
            type: "GET",
            url: "/CreateForm/GetDistict",
            data: { 'citycode': citycode }
        }).done(function (obj) {
            console.log(obj);
            if (obj.status === 'OK') {
                var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";

                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].districtId + "'>" + obj.data[i].districtName + "</option>";

                }
                $('#selDistrict').html(text);

                if ($('#txtDistrict').val() !== '-1' && $('#txtDistrict').val() !== '') { //

                    $('select#selDistrict').val($('#txtDistrict').val());
                }
            }
            

        });

    });

    //get subdistrict
    $('#selDistrict').on('change', function () {
        var districtcode = $(this).val();
        $('#txtDistrict').val(districtcode);
        var citycode = $('#txtCity').val();

        //clear value
        var txt = '';                
        $('#txtSubDistrict').val('');
        $('#txtPostCode').val('');

        $.ajax({
            type: "GET",
            url: "/CreateForm/GetSubDistict",
            data: { 'citycode': citycode,'districtcode': districtcode}
        }).done(function (obj) {
            console.log(obj);

            if (obj.status === 'OK') {
                var text = "<option selected='selected' value='-1'>กรุณาเลือก</option>";

                for (var i = 0; i < obj.count; i++) {

                    text += "<option  value='" + obj.data[i].subdistr + "'>" + obj.data[i].subdistrName + "</option>";

                }
                $('#selSubDistrict').html(text);

                if ($('#txtSubDistrict').val() !== '-1' && $('#txtSubDistrict').val() !== '') { //

                    $('select#selSubDistrict').val($('#txtSubDistrict').val());
                }
            }
            

        });

    });


    //get postcode
    $('#selSubDistrict').on('change', function () {
        
        var subdistrictcode = $(this).val();
        var districtcode = $('#txtDistrict').val();
        var citycode = $('#txtCity').val();
        $.ajax({
            type: "GET",
            url: "/CreateForm/GetPostCode",
            data: { 'citycode': citycode, 'districtcode': districtcode, 'subdistrictcode': subdistrictcode}
        }).done(function (obj) {
            console.log(obj);
            if (obj.status === 'OK') {
                $('#txtPostCode').val(obj.data);
            }

        });

    });
});